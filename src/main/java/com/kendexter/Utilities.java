package com.kendexter;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

@SuppressWarnings({ "rawtypes", "unchecked"})
public class Utilities {

	private static Logger logger = Logger.getLogger(Utilities.class);

	private Utilities() {
	}

	public static boolean compareBeans(Object bean1, Object bean2) throws Exception {
		Class clazz = bean1.getClass();
		if (bean1.getClass() != bean2.getClass())
			return false;
		Method methods[] = clazz.getDeclaredMethods();
		for (Method method : methods) {
			if (method.getParameterTypes().length > 0 || (!method.getName().startsWith("get") && !method.getName().startsWith("is"))) {
				continue;	// Ignore everything thing except simple get and is
			}
			if (Collection.class.isAssignableFrom(method.getReturnType())) {
				//  The collection must be of a type that keeps a set sequence (LinkedList, ArrayList) or this won't work
				Collection collection1 = (Collection) method.invoke(bean1, new Object[] {});
				Collection collection2 = (Collection) method.invoke(bean2, new Object[] {});
				if (collection1.size() != collection2.size())
					return false;
				Iterator iterator1 = collection1.iterator();
				Iterator iterator2 = collection2.iterator();
				while (iterator1.hasNext()) {
					Object object1 = iterator1.next();
					Object object2 = iterator2.next();
					if (!compareBeans(object1, object2))
						return false;
				}
			} else {
				Object object1 = method.invoke(bean1, new Object[] {});
				Object object2 = method.invoke(bean2, new Object[] {});
				if ((object1 == null && object2 != null) || (object1 != null && object2 == null))
					return false;
				if (object1 == null && object2 == null)
					continue;
				if (object1 instanceof Double) {
					if ((double) object1 != (double) object2) {
						return false;
					}
				} else if (object1 instanceof Long) {
					if ((long) object1 != (long) object2) {
						return false;
					}
				} else if (object1 instanceof Integer) {
					if ((int) object1 != (int) object2) {
						return false;
					}
				} else if (object1 instanceof String) {
					if (!((String) object1).equals((String) object2)) {
						return false;
					}
				} else if (object1 instanceof Timestamp) {
					if (!((Timestamp) object1).equals((Timestamp) object2)) {
						return false;
					}
				} else if (object1 instanceof Date) {
					if (!((Date) object1).equals((Date) object2)) {
						return false;
					}
				} else if (object1 instanceof Boolean) {
					if (!((boolean) object1) == ((boolean) object2)) {
						return false;
					}
				} else {
					logger.error(bean1.getClass().getSimpleName() + "." + method.getName());
					logger.error("Utilities.compareBeans no logic to compare " + object1.getClass().getSimpleName() + " types");
				}
			}
		}
		return true;
	}

	public static Object shallowClone(Object fromBean, Object toBean) {
		/* Get a Set of set methods */
		HashMap<String, Method> toNames = new HashMap<String, Method>();
		Class toClass = toBean.getClass();
		Method toMethods[] = toClass.getMethods();
		for (int i = 0; i < toMethods.length; i++) {
			Method toMethod = toMethods[i];
			if (!toMethod.getName().startsWith("set") || toMethod.getParameterTypes().length != 1)
				continue;
			toNames.put(toMethod.getName(), toMethod);
		}
		Class fromClass = fromBean.getClass();
		Method fromMethods[] = fromClass.getMethods();
		for (int i = 0; i < fromMethods.length; i++) {
			Method fromMethod = fromMethods[i];
			String fromName = fromMethod.getName();
			/* Check for proper "get" or "is" method name */
			if ((!fromName.startsWith("get") && !fromName.startsWith("is")) || fromMethod.getParameterTypes().length != 0)
				continue;
			copyTo(fromName, toNames, fromBean, fromMethod, toBean);
		}
		return toBean;
	}

	public static Object deepClone(Object fromBean, Object toBean) {
		/* Get a Set of set methods */
		HashMap<String, Method> toNames = new HashMap<String, Method>();
		Class toClass = toBean.getClass();
		Method toMethods[] = toClass.getMethods();
		for (int i = 0; i < toMethods.length; i++) {
			Method toMethod = toMethods[i];
			if (!toMethod.getName().startsWith("set") || toMethod.getParameterTypes().length != 1)
				continue;
			toNames.put(toMethod.getName(), toMethod);
		}
		Class fromClass = fromBean.getClass();
		Method fromMethods[] = fromClass.getMethods();
		for (int i = 0; i < fromMethods.length; i++) {
			Method fromMethod = fromMethods[i];
			String fromName = fromMethod.getName();
			/* Check for proper "get" or "is" method name */
			if ((!fromName.startsWith("get") && !fromName.startsWith("is")) || fromMethod.getParameterTypes().length != 0)
				continue;

			if (Collection.class.isAssignableFrom(fromMethod.getReturnType())) {
				try {
					Collection collectionBean = (Collection) fromMethod.getReturnType().newInstance();
					Collection fromCollection = (Collection) fromMethod.invoke(fromBean, new Object[] {});
					for (Object fromObject : fromCollection) {
						Object toObject = fromObject.getClass().newInstance();
						deepClone(fromObject, toObject);
						collectionBean.add(toObject);
					}
					setCollection(fromName, toNames, fromBean, fromMethod, toBean, collectionBean);
					continue;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			copyTo(fromName, toNames, fromBean, fromMethod, toBean);
		}
		return toBean;
	}

	private static void copyTo(String fromName, HashMap<String, Method> toNames, Object fromBean, Method fromMethod, Object toBean) {
		String oName = "";
		/*
		 *  Check that the to bean has the corresponding method 
		 */
		if (fromName.startsWith("get") && toNames.get("set" + fromName.substring(3)) != null)
			oName = "set" + fromName.substring(3);
		else if (fromName.startsWith("is") && toNames.get("set" + fromName.substring(2)) != null)
			oName = "set" + fromName.substring(2);
		else {
			loggerNotFound(fromBean, fromMethod);
			return;
		}
		Method toMethod = toNames.get(oName);
		try {
			Object fromObject = fromMethod.invoke(fromBean, new Object[] {});
			toMethod.invoke(toBean, new Object[] { fromObject });
		} catch (IllegalArgumentException e) {

			logger.warn("Type mismatch on CopyBean from "
					+ fromBean.getClass().getSimpleName()
					+ " to "
					+ toBean.getClass().getSimpleName()
					+ " on method "
					+ toMethod.getName());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	private static void setCollection(String fromName, HashMap<String, Method> toNames, Object fromBean, Method fromMethod, Object toBean, Object collection) {
		String oName = "";
		/*
		 *  Check that the to bean has the corresponding method 
		 */
		if (fromName.startsWith("get") && toNames.get("set" + fromName.substring(3)) != null)
			oName = "set" + fromName.substring(3);
		else {
			loggerNotFound(fromBean, fromMethod);
			return;
		}

		Method toMethod = toNames.get(oName);

		try {
			toMethod.invoke(toBean, collection);
		} catch (IllegalArgumentException e) {
			logger.warn("Type mismatch on CopyBean from "
					+ fromBean.getClass().getSimpleName()
					+ " to "
					+ toBean.getClass().getSimpleName()
					+ " on method "
					+ toMethod.getName());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	private static void loggerNotFound(Object fromBean, Method fromMethod) {
		if (fromMethod.getName().equals("getClass"))
			return;
		String classMethod = fromBean.getClass().getSimpleName() + "." + fromMethod.getName();
		logger.warn("No getter method found for " + classMethod);

	}
}
