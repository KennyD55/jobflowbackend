package com.kendexter.jobflow.services.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

//TODO Need to handle approval et al....
//TODO Need to add special functions to change account number and set to active / inactive / delete.
@Path("customers")
public class CustomerServices {
	private static Logger logger = Logger.getLogger(CustomerServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getCustomer")
	@GET
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomer(@QueryParam("customerName") String customerName) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Customer customer = CustomersDAO.getCustomer(connection, customerName);
			return Response.ok().entity(customer).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getCustomerByAccount")
	@GET
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerByAccount(@QueryParam("accountNumber") String accountNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Customer customer = CustomersDAO.getCustomerByAccount(connection, accountNumber);
			return Response.ok().entity(customer).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertCustomer")
	@POST
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertCustomer(CustomerForm customerForm, @Context HttpServletRequest request) {
		Connection connection = null;
		User user = (User) request.getAttribute("user");
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			CustomersDAO.upsertCustomer(connection, customerForm, user);
			connection.commit();
			return Response.ok().entity(customerForm).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyCustomerName")
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyCustomerName(@QueryParam("id") int id, @QueryParam("customerName") String customerName) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = conn.prepareStatement("SELECT COUNT(*) FROM CUSTOMERS WHERE CUSTOMER_NAME = ? AND ID != ?");
			select.setString(1, customerName);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null : "Customer name " + customerName + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("customerNameList")
	@GET
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerNameList() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			User user = AppContext.getUser(context, requestContext);
			Collection<String> customerNames = CustomersDAO.getCustomerNameList(connection, user);
			return Response.ok().entity(customerNames).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("customerList")
	@GET
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerList() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			User user = AppContext.getUser(context, requestContext);
			Collection<Customer> customers = CustomersDAO.getCustomerList(connection, user);
			return Response.ok().entity(customers).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deleteContact")
	@POST
	@RolesAllowed({ AppContext.COD, AppContext.MAINTAIN_ALL_CUSTOMERS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteContact(@QueryParam("id") int id, @QueryParam("name") String name) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Contact> remainingContacts = CustomersDAO.deleteContact(connection, id, name);
			connection.commit();
			return Response.ok().entity(remainingContacts).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

}
