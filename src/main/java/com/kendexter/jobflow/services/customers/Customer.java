package com.kendexter.jobflow.services.customers;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Customer implements Serializable {
	private int id;
	private String accountNumber;
	private String customerName;
	private String salesrep;
	private boolean codOnly;
	private boolean live;
	private String abbr;
	private boolean monitored; // Credit monitored indicator
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private boolean hold;  // Adminstrative hold on account
	private String fax;
	private ArrayList<Contact> contacts;
	private ArrayList<Contact> pressCheckContacts;
	private ArrayList<Shipping> shippings;

	public Customer() {
		setContacts(new ArrayList<Contact>());
		setPressCheckContacts(new ArrayList<Contact>());
		setShippings(new ArrayList<Shipping>());
	}

	/* JSON fields */
	public String getFullAddress() {
		String fullAddress = this.address1;
		if (this.address2 != null && this.address2.trim().length() > 1)
			fullAddress += " " + this.address2;
		if (this.city != null && this.city.trim().length() > 1)
			fullAddress += " " + this.city;
		if (this.state != null && this.state.trim().length() > 1) {
			if (this.city != null && this.city.trim().length() > 1)
				fullAddress += ", " + this.state;
			else
				fullAddress += " " + this.state;}
		if (this.zip != null && this.zip.trim().length() > 1)
			fullAddress += "  " + this.zip;
		return fullAddress;
	}
	public String getFormattedAddress() {
		String formattedAddress = this.address1;
		if (this.address2 != null && this.address2.trim().length() > 1)
			formattedAddress += "\n" + this.address2;
		if (this.city != null && this.city.trim().length() > 1)
			formattedAddress += "\n" + this.city;
		if (this.state != null && this.state.trim().length() > 1) {
			if (this.city != null && this.city.trim().length() > 1)
				formattedAddress += ", " + this.state;
			else
				formattedAddress += " " + this.state;}
		if (this.zip != null && this.zip.trim().length() > 1)
			formattedAddress += "  " + this.zip;
		return formattedAddress;
	}
	/*****/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCustomerName() {
		return customerName == null ? "" : customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSalesrep() {
		return salesrep;  // Salesrep may be null.
	}

	public void setSalesrep(String salesrep) {
		this.salesrep = salesrep;
	}

	public boolean isCodOnly() {
		return codOnly;
	}

	public void setCodOnly(boolean codOnly) {
		this.codOnly = codOnly;
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public String getAbbr() {
		return abbr == null ? "" : abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public boolean isMonitored() {
		return monitored;
	}

	public void setMonitored(boolean monitored) {
		this.monitored = monitored;
	}

	public String getAddress1() {
		return address1 == null ? "" : address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2 == null ? "" : address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city == null ? "" : city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state == null ? "" : state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip == null ? "" : zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public boolean isHold() {
		return hold;
	}

	public void setHold(boolean hold) {
		this.hold = hold;
	}

	public String getFax() {
		return fax == null ? "" : fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void addContact(Contact contact) {
		this.contacts.add(contact);
	}

	public ArrayList<Contact> getPressCheckContacts() {
		return pressCheckContacts;
	}

	public void setPressCheckContacts(ArrayList<Contact> pressCheckContacts) {
		this.pressCheckContacts = pressCheckContacts;
	}
	
	public void addPressCheckContact(Contact contact) {
		this.pressCheckContacts.add(contact);
	}

	public ArrayList<Shipping> getShippings() {
		return shippings;
	}

	public void setShippings(ArrayList<Shipping> shippings) {
		this.shippings = shippings;
	}
	
	public void addShipping(Shipping shipping) {
		this.shippings.add(shipping);
	}
}
