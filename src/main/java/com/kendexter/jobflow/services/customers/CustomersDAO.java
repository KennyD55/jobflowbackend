package com.kendexter.jobflow.services.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

public class CustomersDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(CustomersDAO.class);

	private CustomersDAO() {
	}

	public static Customer getCustomer(Connection connection, String customerName) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID, ACCOUNT_NUMBER, CUSTOMER_NAME, SALESREP, COD_ONLY, LIVE, ABBR,"
				+ " MONITORED, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, HOLD, FAX "
				+ " FROM CUSTOMERS WHERE CUSTOMER_NAME = ?");
		int p = 0;
		select.setString(++p, customerName);
		ResultSet rs = select.executeQuery();
		Customer customer = null;
		if (rs.next()) {
			customer = loadCustomerFromResultSet(connection, rs);
		}
		rs.close();
		select.close();
		return customer;
	}

	public static Customer getCustomerByAccount(Connection connection, String accountNumber) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID, ACCOUNT_NUMBER, CUSTOMER_NAME, SALESREP, COD_ONLY, LIVE, ABBR,"
				+ " MONITORED, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, HOLD, FAX "
				+ " FROM CUSTOMERS WHERE ACCOUNT_NUMBER = ?");
		int p = 0;
		select.setString(++p, accountNumber);
		ResultSet rs = select.executeQuery();
		Customer customer = null;
		if (rs.next()) {
			customer = loadCustomerFromResultSet(connection, rs);
		}
		rs.close();
		select.close();
		return customer;
	}

	public static Collection<String> getCustomerNameList(Connection connection, User user) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;

		Collection<String> customerNames = new LinkedList<String>();

		if (user.hasAuthority(AppContext.MAINTAIN_JOBS)) {
			select = connection.prepareStatement("SELECT DISTINCT CUSTOMER_NAME FROM JOBS ORDER BY CUSTOMER_NAME");
		} else {
			select = connection.prepareStatement("SELECT DISTINCT CUSTOMER_NAME FROM JOBS WHERE SALESREP = ? ORDER BY CUSTOMER_NAME");
			select.setString(1, user.getName());
		}

		rs = select.executeQuery();
		while (rs.next())
			customerNames.add(rs.getString(1));
		rs.close();
		select.close();
		return customerNames;
	}

	public static Collection<Customer> getCustomerList(Connection connection, User user) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;

		Collection<Customer> customers = new LinkedList<Customer>();

		if (user.hasAuthority(AppContext.MAINTAIN_ALL_CUSTOMERS)) {
			select = connection.prepareStatement(
					"SELECT ID, ACCOUNT_NUMBER, CUSTOMER_NAME, SALESREP, COD_ONLY, LIVE, ABBR, MONITORED, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, HOLD, FAX"
							+ "	FROM CUSTOMERS ORDER BY CUSTOMER_NAME");
		} else {
			select = connection.prepareStatement(
					"SELECT ID, ACCOUNT_NUMBER, CUSTOMER_NAME, SALESREP, COD_ONLY, LIVE, ABBR, MONITORED, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, HOLD, FAX"
							+ "	FROM CUSTOMERS WHERE SALESREP = ? ORDER BY CUSTOMER_NAME");
			select.setString(1, user.getName());
		}
		rs = select.executeQuery();
		while (rs.next())
			customers.add(loadCustomerFromResultSet(connection, rs));
		rs.close();
		select.close();
		return customers;
	}

	private static Customer loadCustomerFromResultSet(Connection connection, ResultSet rsIn) throws Exception {
		Customer customer = new Customer();
		customer.setId(rsIn.getInt("ID"));
		customer.setAccountNumber(rsIn.getString("ACCOUNT_NUMBER"));
		customer.setCustomerName(rsIn.getString("CUSTOMER_NAME"));
		customer.setSalesrep(rsIn.getString("SALESREP"));
		customer.setCodOnly(rsIn.getInt("COD_ONLY") == 1);
		customer.setLive(rsIn.getInt("LIVE") == 1);
		customer.setAbbr(rsIn.getString("ABBR"));
		customer.setMonitored(rsIn.getInt("MONITORED") == 1);
		customer.setAddress1(rsIn.getString("ADDRESS1"));
		customer.setAddress2(rsIn.getString("ADDRESS2"));
		customer.setCity(rsIn.getString("CITY"));
		customer.setState(rsIn.getString("STATE"));
		customer.setZip(rsIn.getString("ZIP"));
		customer.setHold(rsIn.getInt("HOLD") == 1);
		customer.setFax(rsIn.getString("FAX"));

		customer.setContacts(getContacts(connection, customer.getId()));
		PreparedStatement select = connection.prepareStatement("SELECT ID, SEQ,  CONTACT_NAME, CONTACT_PHONE FROM PRESS_CONTACTS WHERE ID = ? ORDER BY SEQ");
		select.setInt(1, customer.getId());
		ResultSet rs = select.executeQuery();
		if (rs.next()) {
			do {
				Contact contact = new Contact();
				contact.setId(rs.getInt("ID"));
				contact.setSeq(rs.getInt("SEQ"));
				contact.setName(rs.getString("CONTACT_NAME"));
				contact.setPhone(rs.getString("CONTACT_PHONE"));
				customer.addPressCheckContact(contact);
			} while (rs.next());
		} else {
			Contact contact = new Contact();
			contact.setId(customer.getId());
			customer.addPressCheckContact(contact);
		}
		rs.close();
		select.close();

		select = connection.prepareStatement("SELECT ID, SEQ,  SHIP_TO, SHIPPING_ADDRESS, SHIP_VIA FROM SHIPPING WHERE ID = ? " + "ORDER BY SEQ" + " limit 12"//TODO TESTONLY
		);
		select.setInt(1, customer.getId());
		rs = select.executeQuery();
		while (rs.next()) {
			Shipping shipping = new Shipping();
			shipping.setId(rs.getInt("ID"));
			shipping.setSeq(rs.getInt("SEQ"));
			shipping.setShipTo(rs.getString("SHIP_TO"));
			shipping.setShippingAddress(rs.getString("SHIPPING_ADDRESS"));
			shipping.setShipVia(rs.getString("SHIP_VIA"));
			customer.addShipping(shipping);
		}
		rs.close();
		select.close();

		return customer;
	}

	public static ArrayList<Contact> getContacts(Connection connection, int id) throws Exception {
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		PreparedStatement select = connection.prepareStatement("SELECT ID, SEQ, CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL FROM CONTACTS WHERE ID = ?");
		select.setInt(1, id);
		ResultSet rs = select.executeQuery();
		if (rs.next()) {
			do {
				Contact contact = new Contact();
				contact.setId(rs.getInt("ID"));
				contact.setSeq(rs.getInt("SEQ"));
				contact.setName(rs.getString("CONTACT_NAME"));
				contact.setPhone(rs.getString("CONTACT_PHONE"));
				contact.setEmail(rs.getString("CONTACT_EMAIL"));
				contacts.add(contact);
			} while (rs.next());
		} else {
			Contact contact = new Contact();
			contact.setId(id);
			contacts.add(contact);
		}
		rs.close();
		select.close();
		return contacts;
	}

	public static synchronized void upsertCustomer(Connection connection, CustomerForm customerForm, User user) throws Exception {

		PreparedStatement upsert = user.getFunctions().contains(AppContext.COD)
				? upsert = connection.prepareStatement(
						"INSERT INTO CUSTOMERS(ID, ACCOUNT_NUMBER, CUSTOMER_NAME, LIVE, ABBR, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, FAX, SALESREP, COD_ONLY, MONITORED, HOLD)"
								+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
								+ " ON DUPLICATE KEY UPDATE ACCOUNT_NUMBER= ?, CUSTOMER_NAME= ?, LIVE= ?, ABBR= ?, ADDRESS1= ?, ADDRESS2= ?, CITY= ?, STATE= ?, ZIP= ?, FAX= ?, SALESREP= ?, COD_ONLY= ?, MONITORED= ?, HOLD= ?",
						Statement.RETURN_GENERATED_KEYS)
				: connection.prepareStatement(
						"INSERT INTO CUSTOMERS(ID, ACCOUNT_NUMBER, CUSTOMER_NAME, LIVE, ABBR, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, FAX, SALESREP)"
								+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
								+ " ON DUPLICATE KEY UPDATE ACCOUNT_NUMBER= ?, CUSTOMER_NAME= ?, LIVE= ?, ABBR= ?, ADDRESS1= ?, ADDRESS2= ?, CITY= ?, STATE= ?, ZIP= ?, FAX= ?",
						Statement.RETURN_GENERATED_KEYS);
		int p = 0;
		/*
		 *  Inserts
		 */
		upsert.setInt(++p, customerForm.getId());
		upsert.setString(++p, customerForm.getAccountNumber());
		upsert.setString(++p, customerForm.getCustomerName());
		upsert.setInt(++p, customerForm.isLive() ? 1 : 0);
		upsert.setString(++p, customerForm.getAbbr());
		upsert.setString(++p, customerForm.getAddress1());
		upsert.setString(++p, customerForm.getAddress2());
		upsert.setString(++p, customerForm.getCity());
		upsert.setString(++p, customerForm.getState());
		upsert.setString(++p, customerForm.getZip());
		upsert.setString(++p, customerForm.getFax());
		if (user.getFunctions().contains(AppContext.COD)) {
			upsert.setString(++p, customerForm.getSalesrep());
			upsert.setInt(++p, customerForm.isCodOnly() ? 1 : 0);
			upsert.setInt(++p, customerForm.isMonitored() ? 1 : 0);
			upsert.setInt(++p, customerForm.isHold() ? 1 : 0);
		} else {
			upsert.setString(++p, user.getName());
		}
		/*
		 *  Updates
		 */
		upsert.setString(++p, customerForm.getAccountNumber());
		upsert.setString(++p, customerForm.getCustomerName());
		upsert.setInt(++p, customerForm.isLive() ? 1 : 0);
		upsert.setString(++p, customerForm.getAbbr());
		upsert.setString(++p, customerForm.getAddress1());
		upsert.setString(++p, customerForm.getAddress2());
		upsert.setString(++p, customerForm.getCity());
		upsert.setString(++p, customerForm.getState());
		upsert.setString(++p, customerForm.getZip());
		upsert.setString(++p, customerForm.getFax());
		if (user.getFunctions().contains(AppContext.COD)) {
			upsert.setString(++p, customerForm.getSalesrep());
			upsert.setInt(++p, customerForm.isCodOnly() ? 1 : 0);
			upsert.setInt(++p, customerForm.isMonitored() ? 1 : 0);
			upsert.setInt(++p, customerForm.isHold() ? 1 : 0);
		}

		upsert.executeUpdate();
		ResultSet rs = upsert.getGeneratedKeys();
		if (rs.next()) {
			customerForm.setId(rs.getInt(1));
		}
		rs.close();
		upsert.close();

		upsert = connection.prepareStatement("INSERT INTO CONTACTS(ID, SEQ, CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL)"
				+ " VALUES (?, ?, ?, ?, ?)"
				+ " ON DUPLICATE KEY UPDATE CONTACT_NAME= ?, CONTACT_PHONE= ?, CONTACT_EMAIL= ?");
		p = 0;
		// Inserts
		upsert.setInt(++p, customerForm.getId());
		upsert.setInt(++p, customerForm.getContactSeq());
		upsert.setString(++p, customerForm.getContactName());
		upsert.setString(++p, customerForm.getContactPhone());
		upsert.setString(++p, customerForm.getContactEmail());
		// Updates
		upsert.setString(++p, customerForm.getContactName());
		upsert.setString(++p, customerForm.getContactPhone());
		upsert.setString(++p, customerForm.getContactEmail());

		upsert.executeUpdate();
		upsert.close();

		upsert = connection.prepareStatement("INSERT INTO PRESS_CONTACTS (ID, SEQ, CONTACT_NAME, CONTACT_PHONE)"
				+ " VALUES (?, ?, ?, ?)"
				+ " ON DUPLICATE KEY UPDATE CONTACT_NAME= ?, CONTACT_PHONE= ?");
		p = 0;
		// Inserts
		upsert.setInt(++p, customerForm.getId());
		upsert.setInt(++p, customerForm.getPressCheckContactSeq());
		upsert.setString(++p, customerForm.getContactName());
		upsert.setString(++p, customerForm.getContactPhone());
		// Updates
		upsert.setString(++p, customerForm.getContactName());
		upsert.setString(++p, customerForm.getContactPhone());

		upsert.executeUpdate();
		upsert.close();

		upsert = connection.prepareStatement("INSERT INTO SHIPPING (ID, SEQ, SHIP_TO, SHIPPING_ADDRESS, SHIP_VIA)"
				+ " VALUES (?, ?, ?, ?, ?)"
				+ " ON DUPLICATE KEY UPDATE SHIP_TO= ?, SHIPPING_ADDRESS= ?, SHIP_VIA= ?");
		p = 0;
		// Inserts
		upsert.setInt(++p, customerForm.getId());
		upsert.setInt(++p, customerForm.getShipSeq());
		upsert.setString(++p, customerForm.getShipTo());
		upsert.setString(++p, customerForm.getShippingAddress());
		upsert.setString(++p, customerForm.getShipVia());
		// Updates
		upsert.setString(++p, customerForm.getShipTo());
		upsert.setString(++p, customerForm.getShippingAddress());
		upsert.setString(++p, customerForm.getShipVia());

		upsert.executeUpdate();
		upsert.close();

		if (customerForm.getAccountNumber() == null && user.hasAuthority(AppContext.COD)) {
			int nextAccountNumber = getNextAccountNumber(connection);
			customerForm.setAccountNumber(nextAccountNumber + "");
			upsert = connection.prepareStatement("UPDATE CUSTOMERS SET ACCOUNT_NUMBER = ? WHERE ID = ?");
			upsert.setInt(1, nextAccountNumber);
			upsert.setInt(2, customerForm.getId());
			upsert.executeUpdate();
			upsert.close();
		}
	}

	private static int getNextAccountNumber(Connection connection) throws Exception {
		int nextAccountNumber = Integer.parseInt(AppContext.getParameterValue(AppContext.NEXT_ACCOUNT_NUMBER));
		PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM CUSTOMERS WHERE ACCOUNT_NUMBER = ?");
		select.setInt(1, nextAccountNumber);
		ResultSet rs = select.executeQuery();
		rs.next();
		int count = rs.getInt(1);
		rs.close();
		while (count != 0) {
			nextAccountNumber++;
			select.setInt(1, nextAccountNumber);
			rs = select.executeQuery();
			rs.next();
			count = rs.getInt(1);
			rs.close();
		}
		select.close();
		AppContext.setParameterValue(connection, AppContext.NEXT_ACCOUNT_NUMBER, (nextAccountNumber + 1) + "");
		return nextAccountNumber;

	}

	public static synchronized Collection<Contact> deleteContact(Connection connection, int id, String name) throws Exception {
		PreparedStatement delete = connection.prepareStatement("DELETE FROM CONTACTS WHERE ID = ? AND CONTACT_NAME = ?");
		delete.setInt(1, id);
		delete.setString(2, name);
		delete.executeUpdate();
		delete.close();
		return getContacts(connection, id);
	}

	public static synchronized void upsertContact(Connection connection, String accountNumber, String contactName, String contactPhone, String contactEmail) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID FROM CUSTOMERS WHERE ACCOUNT_NUMBER = ?");
		select.setString(1, accountNumber);
		ResultSet rs = select.executeQuery();
		if (!rs.next())
			throw new RuntimeException("No customer found for account number " + accountNumber);
		int id = rs.getInt("ID");
		rs.close();
		select.close();

		select = connection.prepareStatement("SELECT SEQ FROM CONTACTS WHERE ID = ? AND CONTACT_NAME = ?");
		select.setInt(1, id);
		select.setString(2, contactName);
		rs = select.executeQuery();
		if (rs.next()) {
			PreparedStatement update = connection.prepareStatement("UPDATE CONTACTS SET CONTACT_PHONE = ?, CONTACT_EMAIL = ? WHERE ID = ? AND SEQ = ?");
			update.setString(1, contactPhone);
			update.setString(2, contactEmail);
			update.setInt(3, id);
			update.setInt(4, rs.getInt("SEQ"));
			update.executeUpdate();
			update.close();
			rs.close();
			select.close();
		} else {
			rs.close();
			select.close();
			select = connection.prepareStatement("SELECT MAX(SEQ) FROM CONTACTS WHERE ID = ?");
			select.setInt(1, id);
			rs = select.executeQuery();
			int seq = 0;
			if (rs.next())
				seq = rs.getInt(1) + 1;
			rs.close();
			select.close();
			PreparedStatement insert = connection.prepareStatement("INSERT INTO CONTACTS (ID, SEQ, CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL) VALUES(?,?,?,?,?)");
			insert.setInt(1, id);
			insert.setInt(2, seq);
			insert.setString(3, contactName);
			insert.setString(4, contactPhone);
			insert.setString(5, contactEmail);
			insert.executeUpdate();
			insert.close();
		}
	}

	public static synchronized void upsertPressContact(Connection connection, String accountNumber, String pressCheckContactName, String pressCheckContactPhone)
			throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID FROM CUSTOMERS WHERE ACCOUNT_NUMBER = ?");
		select.setString(1, accountNumber);
		ResultSet rs = select.executeQuery();
		if (!rs.next())
			throw new RuntimeException("No customer found for account number " + accountNumber);
		int id = rs.getInt("ID");
		rs.close();
		select.close();

		select = connection.prepareStatement("SELECT SEQ FROM PRESS_CONTACTS WHERE ID = ? AND CONTACT_NAME = ?");
		select.setInt(1, id);
		select.setString(2, pressCheckContactName);
		rs = select.executeQuery();
		if (rs.next()) {
			PreparedStatement update = connection.prepareStatement("UPDATE PRESS_CONTACTS SET CONTACT_PHONE = ? WHERE ID = ? AND SEQ = ?");
			update.setString(1, pressCheckContactPhone);
			update.setInt(2, id);
			update.setInt(3, rs.getInt("SEQ"));
			update.executeUpdate();
			update.close();
			rs.close();
			select.close();
		} else {
			rs.close();
			select.close();
			select = connection.prepareStatement("SELECT MAX(SEQ) FROM PRESS_CONTACTS WHERE ID = ?");
			select.setInt(1, id);
			rs = select.executeQuery();
			int seq = 0;
			if (rs.next())
				seq = rs.getInt(1) + 1;
			rs.close();
			select.close();
			PreparedStatement insert = connection.prepareStatement("INSERT INTO PRESS_CONTACTS (ID, SEQ, CONTACT_NAME, CONTACT_PHONE) VALUES(?,?,?,?)");
			insert.setInt(1, id);
			insert.setInt(2, seq);
			insert.setString(3, pressCheckContactName);
			insert.setString(4, pressCheckContactPhone);
			insert.executeUpdate();
			insert.close();
		}
	}
}
