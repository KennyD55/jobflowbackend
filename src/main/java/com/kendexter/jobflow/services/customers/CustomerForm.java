package com.kendexter.jobflow.services.customers;

@SuppressWarnings("serial")
public class CustomerForm extends Customer {

	private int contactSeq;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private int pressCheckContactSeq;
	private String pressCheckContactName;
	private String pressCheckContactPhone;
	private int shipSeq;
	private String shipTo;
	private String shippingAddress;
	private String shipVia;

	public CustomerForm() {
		super();
	}

	public int getContactSeq() {
		return contactSeq;
	}

	public void setContactSeq(int contactSeq) {
		this.contactSeq = contactSeq;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public int getPressCheckContactSeq() {
		return pressCheckContactSeq;
	}

	public void setPressCheckContactSeq(int pressCheckContactSeq) {
		this.pressCheckContactSeq = pressCheckContactSeq;
	}

	public String getPressCheckContactName() {
		return pressCheckContactName;
	}

	public void setPressCheckContactName(String pressCheckContactName) {
		this.pressCheckContactName = pressCheckContactName;
	}

	public String getPressCheckContactPhone() {
		return pressCheckContactPhone;
	}

	public void setPressCheckContactPhone(String pressCheckContactPhone) {
		this.pressCheckContactPhone = pressCheckContactPhone;
	}

	public int getShipSeq() {
		return shipSeq;
	}

	public void setShipSeq(int shipSeq) {
		this.shipSeq = shipSeq;
	}

	public String getShipTo() {
		return shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getShipVia() {
		return shipVia;
	}

	public void setShipVia(String shipVia) {
		this.shipVia = shipVia;
	}
}
