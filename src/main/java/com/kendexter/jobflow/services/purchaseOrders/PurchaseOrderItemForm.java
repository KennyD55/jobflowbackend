package com.kendexter.jobflow.services.purchaseOrders;

import java.io.Serializable;
import java.text.DecimalFormat;

@SuppressWarnings("serial")
public class PurchaseOrderItemForm implements Serializable {
	private static final ThreadLocal<DecimalFormat> CURRENCY_FORMAT = new ThreadLocal<DecimalFormat>() {
		@Override
		public DecimalFormat initialValue() {
			return new DecimalFormat("$###,###,##0.00");
		}
	};
	private int poNumber;
	private int seq;
	private String item;
	private String description;
	private String quantity;
	private String rate;

	public boolean empty() {
		return ((item == null || item.trim().equals(""))
				&& (description == null || description.trim().equals(""))
				&& (quantity == null || quantity.trim().equals(""))
				&& (rate == null || rate.trim().equals("")));
	}

	public String getItem() {
		return this.item == null ? "" : this.item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getPoNumber() {
		return this.poNumber;
	}

	public void setPoNumber(int poNumber) {
		this.poNumber = poNumber;
	}

	public String getDescription() {
		return this.description == null ? "" : this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuantity() {
		return this.quantity == null ? "" : this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getRate() {
		return this.rate == null ? "" : this.rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getTotal() {
		try {
			double quantity = Double.parseDouble(this.quantity.replaceAll(",", ""));
			double rate = Double.parseDouble(this.rate);
			return CURRENCY_FORMAT.get().format(quantity * rate);
		} catch (Exception e) {
			return "";
		}
	}
}
