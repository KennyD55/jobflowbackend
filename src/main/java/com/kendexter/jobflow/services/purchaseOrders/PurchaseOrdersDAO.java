package com.kendexter.jobflow.services.purchaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

import jersey.repackaged.org.objectweb.asm.Type;

public class PurchaseOrdersDAO {
	private static Logger logger = Logger.getLogger(PurchaseOrdersDAO.class);

	private static final ThreadLocal<DecimalFormat> QUANTITY_FORMAT = new ThreadLocal<DecimalFormat>() {
		@Override
		public DecimalFormat initialValue() {
			return new DecimalFormat("###,###,###");
		}
	};
	private static final ThreadLocal<DecimalFormat> RATE_FORMAT = new ThreadLocal<DecimalFormat>() {
		@Override
		public DecimalFormat initialValue() {
			return new DecimalFormat("##,##0.00##");
		}
	};

	private PurchaseOrdersDAO() {
	}

	public static Collection<PurchaseOrderForm> getPOList(Connection connection, String status) throws Exception {
		LinkedList<PurchaseOrderForm> purchaseOrders = new LinkedList<PurchaseOrderForm>();
		ResultSet rs = null;
		PreparedStatement select = null;

		int p = 0;
		if (status.equals("Archived")) {
			select = connection.prepareStatement(
					"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
							+ " FROM PURCHASE_ORDERS"
							+ " WHERE STATUS = 'Archived' "
							+ " ORDER BY PO_NUMBER DESC");
		} else {
			select = connection.prepareStatement(
					"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
							+ " FROM PURCHASE_ORDERS"
							+ " WHERE STATUS != 'Archived' "
							+ "	  AND (? = 'ALL' OR STATUS = ?)"
							+ " ORDER BY PO_NUMBER DESC");
			select.setString(++p, status.equals("All") ? "All" : "");
			select.setString(++p, status);
		}
		rs = select.executeQuery();
		while (rs.next()) {
			PurchaseOrderForm purchaseOrder = loadFromResultSet(rs);
			purchaseOrders.add(purchaseOrder);
		}
		rs.close();
		select.close();
		return purchaseOrders;
	}

	public static Collection<PurchaseOrderForm> getPOListForJob(Connection connection, String jobNumber) throws Exception {
		LinkedList<PurchaseOrderForm> purchaseOrders = new LinkedList<PurchaseOrderForm>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			int p = 0;
			select = connection.prepareStatement(
					"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
							+ " FROM PURCHASE_ORDERS"
							+ " WHERE JOB_NUMBER = ? "
							+ " ORDER BY PO_NUMBER ASC");
			select.setInt(++p, Integer.parseInt(jobNumber));
			rs = select.executeQuery();
			while (rs.next()) {
				PurchaseOrderForm purchaseOrder = loadFromResultSet(rs);
				purchaseOrders.add(purchaseOrder);
			}
			return purchaseOrders;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static Collection<PurchaseOrderForm> getPOListForVendor(Connection connection, String vendor) throws Exception {
		LinkedList<PurchaseOrderForm> purchaseOrders = new LinkedList<PurchaseOrderForm>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			int p = 0;
			select = connection.prepareStatement(
					"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
							+ " FROM PURCHASE_ORDERS"
							+ " WHERE VENDOR_NAME = ? "
							+ " ORDER BY PO_NUMBER DESC");
			select.setString(++p, vendor);
			rs = select.executeQuery();
			while (rs.next()) {
				PurchaseOrderForm purchaseOrder = loadFromResultSet(rs);
				purchaseOrders.add(purchaseOrder);
			}
			return purchaseOrders;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static Collection<PurchaseOrderForm> getPOListForPONumber(Connection connection, String poNumber) throws Exception {
		LinkedList<PurchaseOrderForm> purchaseOrders = new LinkedList<PurchaseOrderForm>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			int p = 0;
			select = connection.prepareStatement(
					"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
							+ " FROM PURCHASE_ORDERS"
							+ " WHERE PO_NUMBER = ?");
			select.setInt(++p, Integer.parseInt(poNumber));
			rs = select.executeQuery();
			while (rs.next()) {
				PurchaseOrderForm purchaseOrder = loadFromResultSet(rs);
				purchaseOrders.add(purchaseOrder);
			}
			return purchaseOrders;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	private static PurchaseOrderForm loadFromResultSet(ResultSet rs) throws Exception {

		PurchaseOrderForm purchaseOrder = new PurchaseOrderForm();
		purchaseOrder.setPoNumber(rs.getInt("PO_NUMBER"));
		purchaseOrder.setJobNumber(rs.getInt("JOB_NUMBER") == 0 ? "" : rs.getInt("JOB_NUMBER") + "");
		purchaseOrder.setEntryDate(rs.getTimestamp("ENTRY_DATE"));
		purchaseOrder.setShipVia(rs.getString("SHIP_VIA"));
		purchaseOrder.setTotal(rs.getDouble("TOTAL"));
		purchaseOrder.setOrderedBy(rs.getString("ORDERED_BY"));
		purchaseOrder.setApprovedBy(rs.getString("APPROVED_BY"));
		purchaseOrder.setShipTo(rs.getString("SHIP_TO"));
		purchaseOrder.setVendorName(rs.getString("VENDOR_NAME"));
		purchaseOrder.setVendorAccount(rs.getString("VENDOR_ACCOUNT"));
		purchaseOrder.setVendorAddress(rs.getString("VENDOR_ADDRESS"));
		purchaseOrder.setVendorPhone(rs.getString("VENDOR_PHONE"));
		purchaseOrder.setVendorFax(rs.getString("VENDOR_FAX"));
		purchaseOrder.setExpectedDate(rs.getString("EXPECTED_DATE"));
		purchaseOrder.setStatus(rs.getString("STATUS"));
		purchaseOrder.setStatusDate(rs.getTimestamp("STATUS_DATE"));
		purchaseOrder.setAbbr(rs.getString("ABBR"));
		purchaseOrder.setReference(rs.getString("REFERENCE"));

		return purchaseOrder;
	}

	public static Collection<String> getVendors(Connection connection) throws Exception {
		LinkedList<String> vendors = new LinkedList<String>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement("SELECT DISTINCT VENDOR_NAME" + " FROM PURCHASE_ORDERS" + " ORDER BY VENDOR_NAME");
			rs = select.executeQuery();
			while (rs.next()) {
				vendors.add(rs.getString(1));
			}
			return vendors;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static PurchaseOrderForm getPurchaseOrder(Connection connection, String poNumber) throws Exception {
		return getPurchaseOrder(connection, Integer.parseInt(poNumber));
	}

	public static PurchaseOrderForm getPurchaseOrder(Connection connection, int poNumber) throws Exception {
		PreparedStatement select = connection.prepareStatement(
				"SELECT PO_NUMBER, JOB_NUMBER, ENTRY_DATE, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY, SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS,"
						+ " VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, STATUS_DATE, ABBR, REFERENCE"
						+ " FROM PURCHASE_ORDERS WHERE PO_NUMBER = ?");
		select.setInt(1, poNumber);
		ResultSet rs = select.executeQuery();
		rs.next();
		PurchaseOrderForm purchaseOrder = loadFromResultSet(rs);
		purchaseOrder.setPurchaseOrderItems(getPOItems(connection, poNumber));
		rs.close();
		select.close();
		return purchaseOrder;
	}

	public static ArrayList<PurchaseOrderItemForm> getPOItems(Connection connection, int poNumber) throws Exception {
		ArrayList<PurchaseOrderItemForm> items = new ArrayList<PurchaseOrderItemForm>();
		PreparedStatement select = connection
				.prepareStatement("SELECT PO_NUMBER, SEQ, ITEM, DESCRIPTION, QUANTITY, RATE FROM PURCHASE_ORDER_ITEMS WHERE PO_NUMBER = ? ORDER BY SEQ");
		select.setInt(1, poNumber);
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			PurchaseOrderItemForm item = new PurchaseOrderItemForm();
			item.setPoNumber(rs.getInt("PO_NUMBER"));
			item.setSeq(rs.getInt("SEQ"));
			item.setItem(rs.getString("ITEM"));
			item.setDescription(rs.getString("DESCRIPTION"));
			double quantity = rs.getDouble("QUANTITY");
			if (quantity != 0)
				item.setQuantity(QUANTITY_FORMAT.get().format(quantity));
			double rate = rs.getDouble("RATE");
			if (rate != 0)
				item.setRate(RATE_FORMAT.get().format(rate));
			items.add(item);
		}
		return items;
	}

	public static synchronized void updateStatus(Connection connection, String poNumber, String newStatus) throws Exception {
		updateStatus(connection, Integer.parseInt(poNumber), newStatus);
	}

	public static synchronized void updateStatus(Connection connection, int poNumber, String newStatus) throws Exception {
		PreparedStatement update = connection.prepareStatement("UPDATE PURCHASE_ORDERS SET STATUS = ?, STATUS_DATE = CURRENT_TIMESTAMP WHERE PO_NUMBER = ?");
		update.setString(1, newStatus);
		update.setInt(2, poNumber);
		update.executeUpdate();
		update.close();
		// Archived purchase orders closed thirty days ago or longer.
		update = connection
				.prepareStatement("UPDATE PURCHASE_ORDERS SET STATUS = 'Archived' WHERE STATUS = 'Closed' AND STATUS_DATE < CURRENT_TIMESTAMP - INTERVAL 30 DAY");
		update.executeUpdate();
		update.close();
	}

	public static synchronized int upsertPO(Connection connection, PurchaseOrderForm purchaseOrder, User user) throws Exception {
		Vendor vendor = VendorsDAO.getVendorByName(connection, purchaseOrder.getVendorName());
		PreparedStatement upsert = connection.prepareStatement("INSERT INTO PURCHASE_ORDERS (PO_NUMBER, JOB_NUMBER, SHIP_VIA, TOTAL, ORDERED_BY, APPROVED_BY,"
				+ " SHIP_TO, VENDOR_NAME, VENDOR_ACCOUNT, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, EXPECTED_DATE, STATUS, ABBR, REFERENCE)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
				+ " ON DUPLICATE KEY UPDATE JOB_NUMBER= ?, SHIP_VIA= ?, TOTAL= ?, ORDERED_BY= ?, APPROVED_BY= ?, SHIP_TO= ?, VENDOR_NAME= ?,"
				+ " VENDOR_ACCOUNT= ?, VENDOR_ADDRESS= ?, VENDOR_PHONE= ?, VENDOR_FAX= ?, EXPECTED_DATE= ?, STATUS= ?, ABBR= ?, REFERENCE= ?");
		int p = 0;
		// Insert
		int poNumber = purchaseOrder.getPoNumber() == 0 ? getNextPONumber(connection) : purchaseOrder.getPoNumber();
		purchaseOrder.setPoNumber(poNumber);
		upsert.setInt(++p, poNumber);
		if (purchaseOrder.getJobNumber() != null && !purchaseOrder.getJobNumber().trim().equals(""))
			upsert.setInt(++p, Integer.parseInt(purchaseOrder.getJobNumber()));
		else
			upsert.setNull(++p, Type.INT);
		upsert.setString(++p, purchaseOrder.getShipVia());
		upsert.setDouble(++p, purchaseOrder.getTotal());
		upsert.setString(++p, user.getName());
		upsert.setString(++p, user.hasAuthority(AppContext.APPROVE_PURCHASE_ORDERS) ? user.getName() : null);
		upsert.setString(++p, purchaseOrder.getShipTo());
		upsert.setString(++p, vendor.getName());
		//TODO Vendor Account number ????
		//		upsert.setString(++p, vendor.getAccountNumber();
		upsert.setString(++p, "");
		upsert.setString(++p, vendor.getAddress());
		upsert.setString(++p, vendor.getPhone());
		upsert.setString(++p, vendor.getFax());
		upsert.setString(++p, purchaseOrder.getExpectedDate());
		upsert.setString(++p,
				user.hasAuthority(AppContext.APPROVE_PURCHASE_ORDERS)
						|| purchaseOrder.getTotal() <= Double.parseDouble(AppContext.getParameterValue(AppContext.CEILING_LIMIT)) ? PurchaseOrderForm.OPEN
								: PurchaseOrderForm.PENDING);
		upsert.setString(++p, vendor.getAbbr());
		upsert.setString(++p, purchaseOrder.getReference());
		// Update
		if (purchaseOrder.getJobNumber() != null && !purchaseOrder.getJobNumber().trim().equals(""))
			upsert.setInt(++p, Integer.parseInt(purchaseOrder.getJobNumber()));
		else
			upsert.setNull(++p, Type.INT);
		upsert.setString(++p, purchaseOrder.getShipVia());
		upsert.setDouble(++p, purchaseOrder.getTotal());
		upsert.setString(++p, user.getName());
		upsert.setString(++p, user.hasAuthority(AppContext.APPROVE_PURCHASE_ORDERS) ? user.getName() : null);
		upsert.setString(++p, purchaseOrder.getShipTo());
		upsert.setString(++p, vendor.getName());
		//TODO Vendor Account number ????
		//		upsert.setString(++p, vendor.getAccountNumber();
		upsert.setString(++p, "");
		upsert.setString(++p, vendor.getAddress());
		upsert.setString(++p, vendor.getPhone());
		upsert.setString(++p, vendor.getFax());
		upsert.setString(++p, purchaseOrder.getExpectedDate());
		upsert.setString(++p,
				user.hasAuthority(AppContext.APPROVE_PURCHASE_ORDERS)
						|| purchaseOrder.getTotal() <= Double.parseDouble(AppContext.getParameterValue(AppContext.CEILING_LIMIT)) ? PurchaseOrderForm.OPEN
								: PurchaseOrderForm.PENDING);
		upsert.setString(++p, vendor.getAbbr());
		upsert.setString(++p, purchaseOrder.getReference());
		upsert.executeUpdate();
		upsertPOItems(connection, purchaseOrder);
		upsertVendorItems(connection, purchaseOrder);
		return poNumber;
	}

	private static synchronized void upsertPOItems(Connection connection, PurchaseOrderForm purchaseOrder) throws Exception {
		ArrayList<PurchaseOrderItemForm> items = new ArrayList<PurchaseOrderItemForm>();
		for (int i = purchaseOrder.getPurchaseOrderItems().size() - 1; i >= 0; i--) {  // Remove empty items from the bottom
			PurchaseOrderItemForm item = purchaseOrder.getPurchaseOrderItems().get(i);
			if (!item.empty()) {
				for (; i >= 0; i--) {
					item = purchaseOrder.getPurchaseOrderItems().get(i);
					items.add(0, item);
				}
			}
		}
		PreparedStatement delete = connection.prepareStatement("DELETE FROM PURCHASE_ORDER_ITEMS WHERE PO_NUMBER = ?");
		delete.setInt(1, purchaseOrder.getPoNumber());
		delete.executeUpdate();
		delete.close();

		int seq = 0;
		PreparedStatement insert = connection
				.prepareStatement("INSERT INTO PURCHASE_ORDER_ITEMS (PO_NUMBER, SEQ, ITEM, DESCRIPTION, QUANTITY, RATE)" + " VALUES (?, ?, ?, ?, ?, ?)");
		for (PurchaseOrderItemForm item : items) {
			int p = 0;
			insert.setInt(++p, purchaseOrder.getPoNumber());
			insert.setInt(++p, seq++);
			if (item.getItem().trim().equals(""))
				insert.setNull(++p, Type.CHAR);
			else
				insert.setString(++p, item.getItem());
			if (item.getDescription().trim().equals(""))
				insert.setNull(++p, Type.CHAR);
			else
				insert.setString(++p, item.getDescription());
			try {
				double quantity = Double.parseDouble(item.getQuantity().replaceAll(",", ""));
				insert.setDouble(++p, quantity);
			} catch (Exception e) {
				insert.setNull(++p, Type.DOUBLE);
			}
			try {
				double rate = Double.parseDouble(item.getRate().replaceAll(",", ""));
				insert.setDouble(++p, rate);
			} catch (Exception e) {
				insert.setNull(++p, Type.DOUBLE);
			}
			insert.executeUpdate();
		}
		insert.close();
	}

	private static synchronized void upsertVendorItems(Connection connection, PurchaseOrderForm purchaseOrder) throws Exception {
		PreparedStatement delete = connection.prepareStatement("DELETE FROM VENDOR_ITEMS WHERE LAST_ORDERED_DATE < CURRENT_TIMESTAMP - INTERVAL 5 YEAR;");
		delete.executeUpdate();
		delete.close();
		PreparedStatement update = connection.prepareStatement("INSERT INTO VENDOR_ITEMS (VENDOR_ID, DESCRIPTION, ITEM_NUMBER, RATE)"
				+ " VALUES ((SELECT ID FROM VENDORS WHERE NAME = ?), ?, ?, ?)"
				+ " ON DUPLICATE KEY UPDATE ITEM_NUMBER = ?, RATE = ?");
		update.setString(1, purchaseOrder.getVendorName());
		for (PurchaseOrderItemForm item : purchaseOrder.getPurchaseOrderItems()) {
			if (item.getRate().equals(""))
				continue;
			int p = 1;
			update.setString(++p, item.getDescription());
			update.setString(++p, item.getItem());
			update.setDouble(++p, Double.parseDouble(item.getRate()));
			update.setString(++p, item.getItem());
			update.setDouble(++p, Double.parseDouble(item.getRate()));
			update.executeUpdate();
		}
		update.close();
	}

	private static int getNextPONumber(Connection connection) throws Exception {
		int nextPONumber = Integer.parseInt(AppContext.getParameterValue(AppContext.NEXT_PO_NUMBER));
		PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM PURCHASE_ORDERS WHERE PO_NUMBER = ?");
		select.setInt(1, nextPONumber);
		ResultSet rs = select.executeQuery();
		rs.next();
		int count = rs.getInt(1);
		rs.close();
		while (count != 0) {
			nextPONumber++;
			select.setInt(1, nextPONumber);
			rs = select.executeQuery();
			rs.next();
			count = rs.getInt(1);
			rs.close();
		}
		select.close();
		AppContext.setParameterValue(connection, AppContext.NEXT_PO_NUMBER, (nextPONumber + 1) + "");
		return nextPONumber;

	}

	public static Collection<VendorItem> getVendorItems(Connection connection, String vendorName) throws Exception {
		LinkedList<VendorItem> items = new LinkedList<VendorItem>();
		PreparedStatement select = connection.prepareStatement(
				"SELECT VENDOR_ID, DESCRIPTION, ITEM_NUMBER, RATE FROM VENDOR_ITEMS WHERE VENDOR_ID = (SELECT ID FROM VENDORS WHERE NAME = ?) ORDER BY DESCRIPTION");
		select.setString(1, vendorName);
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			VendorItem item = new VendorItem();
			item.setVendorId(rs.getInt("VENDOR_ID"));
			item.setDescription(rs.getString("DESCRIPTION"));
			item.setItemNumber(rs.getString("ITEM_NUMBER"));
			item.setRate(rs.getDouble("RATE"));
			items.add(item);
		}
		rs.close();
		select.close();
		return items;
	}
}