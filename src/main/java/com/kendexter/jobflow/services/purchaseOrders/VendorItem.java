package com.kendexter.jobflow.services.purchaseOrders;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VendorItem implements Serializable {
	private int vendorId;
	private String description;
	private String itemNumber;
	private double rate;

	public int getVendorId() {
		return this.vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemNumber() {
		return this.itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public double getRate() {
		return this.rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
}
