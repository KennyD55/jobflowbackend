package com.kendexter.jobflow.services.purchaseOrders;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.kendexter.jobflow.beans.TokenAttribute;
import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.filters.AuthenticationFilter;
import com.kendexter.jobflow.services.users.User;

//TODO Need to handle approval et al....
@Path("purchaseOrders")
public class PurchaseOrdersServices {
	private static Logger logger = Logger.getLogger(PurchaseOrdersServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("polist")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobList(@QueryParam("status") String status, @QueryParam("jobNumber") String jobNumber, @QueryParam("vendorName") String vendorName,
			@QueryParam("poNumber") String poNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<PurchaseOrderForm> purchaseOrders = jobNumber != null ? PurchaseOrdersDAO.getPOListForJob(connection, jobNumber)
					: poNumber != null ? PurchaseOrdersDAO.getPOListForPONumber(connection, poNumber)
							: vendorName != null ? PurchaseOrdersDAO.getPOListForVendor(connection, vendorName) : PurchaseOrdersDAO.getPOList(connection, status);
			return Response.ok().entity(purchaseOrders).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertPO")
	@POST
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertPO(PurchaseOrderForm purchaseOrder, @Context HttpServletRequest request) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			int poNumber = PurchaseOrdersDAO.upsertPO(connection, purchaseOrder, (User) request.getAttribute("user"));
			connection.commit();
			return Response.ok().entity("{\"poNumber\":\"" + poNumber + "\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	@Path("getVendorItems")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendorItems(@QueryParam("vendorName") String vendorName) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			return Response.ok().entity(PurchaseOrdersDAO.getVendorItems(connection, vendorName)).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getPO")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPO(@QueryParam("poNumber") String poNumber) {
		if (poNumber.equals("0")) { // If the po number is zero return an empty Purchase Order
			PurchaseOrderForm po = new PurchaseOrderForm();
			for (int i = 0; i < 6; i++)
				po.addPurchaseOrderItem(new PurchaseOrderItemForm());
			return Response.ok().entity(po).build();
		}
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			return Response.ok().entity(PurchaseOrdersDAO.getPurchaseOrder(connection, poNumber)).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("vendorList")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendorList() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<String> vendors = PurchaseOrdersDAO.getVendors(connection);
			return Response.ok().entity(vendors).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("viewPO")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	public Response viewPO(@QueryParam("poNumber") String poNumber, @QueryParam("onetimeToken") String onetimeToken, @Context HttpServletRequest request) {
		try {
			TokenAttribute ta = AuthenticationFilter.getOneTimeToken(context, onetimeToken);
			if (ta == null) {
				ResponseBuilder rb = Response.ok("<html><head></head><body>Page has expired</body><html>");
				rb.type(MediaType.TEXT_HTML);
				return rb.build();
			}

			PDDocument document = new PurchaseOrderPDF(request).generatePDF();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			document.save(baos);
			document.close();
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ResponseBuilder rb = Response.ok((Object) bais);
			rb.type("application/pdf");
			return rb.build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("");
		}
	}

	@Path("updateStatus")
	@POST
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })

	public Response updateStatus(@QueryParam("poNumber") String poNumber, @QueryParam("newStatus") String newStatus) {
		try {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PurchaseOrdersDAO.updateStatus(connection, poNumber, newStatus);
				connection.commit();
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("");
		}
	}
}
