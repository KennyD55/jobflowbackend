package com.kendexter.jobflow.services.purchaseOrders;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PurchaseOrderForm implements Serializable {
	private int poNumber;
	private String vendorName;
	private String vendorAccount;
	private String orderedBy;
	private String vendorAddress;
	private String jobNumber;
	private Timestamp statusDate;
	private String shipVia;
	private String abbr;
	private String shipTo;

	private String status;
	public static final String OPEN = "Open";
	public static final String PENDING = "Pending";
	public static final String CLOSED = "Closed";
	public static final String ARCHIVED = "Archived";
	public static final String CANCELLED = "Cancelled";

	private String vendorPhone;
	private String expectedDate;
	private String reference;
	private String vendorFax;
	private Timestamp entryDate;
	private String approvedBy;
	private double total;
	private ArrayList<PurchaseOrderItemForm> purchaseOrderItems;

	public PurchaseOrderForm() {
		this.setPurchaseOrderItems(new ArrayList<PurchaseOrderItemForm>());
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getVendorName() {
		return this.vendorName == null ? "" : this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorAccount() {
		return this.vendorAccount == null ? "" : this.vendorAccount;
	}

	public void setVendorAccount(String vendorAccount) {
		this.vendorAccount = vendorAccount;
	}

	public String getOrderedBy() {
		return this.orderedBy == null ? "" : this.orderedBy;
	}

	public void setOrderedBy(String orderedBy) {
		this.orderedBy = orderedBy;
	}

	public String getVendorAddress() {
		return this.vendorAddress == null ? "" : this.vendorAddress;
	}

	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public Timestamp getStatusDate() {
		return this.statusDate;
	}

	public void setStatusDate(Timestamp statusDate) {
		this.statusDate = statusDate;
	}

	public String getShipVia() {
		return this.shipVia == null ? "" : this.shipVia;
	}

	public void setShipVia(String shipVia) {
		this.shipVia = shipVia;
	}

	public String getAbbr() {
		return this.abbr == null ? "" : this.abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public int getPoNumber() {
		return this.poNumber;
	}

	public void setPoNumber(int poNumber) {
		this.poNumber = poNumber;
	}

	public String getShipTo() {
		return this.shipTo == null ? "" : this.shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getStatus() {
		return this.status == null ? "" : this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVendorPhone() {
		return this.vendorPhone == null ? "" : this.vendorPhone;
	}

	public void setVendorPhone(String vendorPhone) {
		this.vendorPhone = vendorPhone;
	}

	public String getExpectedDate() {
		return this.expectedDate == null ? "" : this.expectedDate;
	}

	public void setExpectedDate(String expectedDate) {
		this.expectedDate = expectedDate;
	}

	public String getReference() {
		return this.reference == null ? "" : this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getVendorFax() {
		return this.vendorFax == null ? "" : this.vendorFax;
	}

	public void setVendorFax(String vendorFax) {
		this.vendorFax = vendorFax;
	}

	public Timestamp getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Timestamp entryDate) {
		this.entryDate = entryDate;
	}

	public String getApprovedBy() {
		return this.approvedBy == null ? "" : this.approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public ArrayList<PurchaseOrderItemForm> getPurchaseOrderItems() {
		return purchaseOrderItems;
	}

	public void setPurchaseOrderItems(ArrayList<PurchaseOrderItemForm> purchaseOrderItems) {
		this.purchaseOrderItems = purchaseOrderItems;
	}

	public void addPurchaseOrderItem(PurchaseOrderItemForm purchaseOrderItem) {
		this.purchaseOrderItems.add(purchaseOrderItem);
	}

	public boolean isOpen() {
		return OPEN.equals(this.status);
	}

	public boolean isPending() {
		return PENDING.equals(this.status);
	}

	public boolean isClosed() {
		return CLOSED.equals(this.status);
	}

	public boolean isArchived() {
		return ARCHIVED.equals(this.status);
	}

	public boolean isCancelled() {
		return CANCELLED.equals(this.status);
	}
}
