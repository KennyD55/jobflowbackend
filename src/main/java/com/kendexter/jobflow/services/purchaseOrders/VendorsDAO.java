package com.kendexter.jobflow.services.purchaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class VendorsDAO {
	private static Logger logger = Logger.getLogger(VendorsDAO.class);

	private VendorsDAO() {
	}

	public static Collection<Vendor> getVendorList(Connection connection) throws Exception {
		LinkedList<Vendor> vendors = new LinkedList<Vendor>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement("SELECT ID, NAME, ADDRESS, PHONE, FAX, ABBR FROM VENDORS ORDER BY NAME");
			rs = select.executeQuery();
			while (rs.next()) {
				Vendor vendor = loadFromResultSet(rs);
				vendors.add(vendor);
			}
			return vendors;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	private static Vendor loadFromResultSet(ResultSet rs) throws Exception {
		Vendor vendor = new Vendor();
		vendor.setId(rs.getInt("ID"));
		vendor.setName(rs.getString("NAME"));
		vendor.setAddress(rs.getString("ADDRESS"));
		vendor.setPhone(rs.getString("PHONE"));
		vendor.setFax(rs.getString("FAX"));
		vendor.setAbbr(rs.getString("ABBR"));
		return vendor;
	}

	public static Vendor getVendorByName(Connection connection, String name) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement("SELECT ID, NAME, ADDRESS, PHONE, FAX, ABBR FROM VENDORS WHERE NAME = ?");
			select.setString(1, name);
			rs = select.executeQuery();
			if (!rs.next())
				return null;
			Vendor vendor = loadFromResultSet(rs);
			return vendor;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static synchronized int upsertVendor(Connection connection, Vendor vendor) throws Exception {
		PreparedStatement upsert = null;
		try {
			upsert = connection.prepareStatement("INSERT INTO VENDORS (ID, NAME, ADDRESS, PHONE, FAX, ABBR) "
					+ " VALUES (?, ?, ?, ?, ?, ?)"
					+ " ON DUPLICATE KEY UPDATE NAME = ?, ADDRESS = ?, PHONE = ?, FAX = ?, ABBR = ? ");
			int p = 0;
			//  Insert parameters
			upsert.setInt(++p, vendor.getId());
			upsert.setString(++p, vendor.getName());
			upsert.setString(++p, vendor.getAddress());
			upsert.setString(++p, vendor.getPhone());
			upsert.setString(++p, vendor.getFax());
			upsert.setString(++p, vendor.getAbbr());
			//  Update parameters
			upsert.setString(++p, vendor.getName());
			upsert.setString(++p, vendor.getAddress());
			upsert.setString(++p, vendor.getPhone());
			upsert.setString(++p, vendor.getFax());
			upsert.setString(++p, vendor.getAbbr());

			return upsert.executeUpdate();
		} finally {
			try {
				upsert.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}