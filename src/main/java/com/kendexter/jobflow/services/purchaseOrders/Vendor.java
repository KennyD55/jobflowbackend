package com.kendexter.jobflow.services.purchaseOrders;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class Vendor implements Serializable {
	private int id;
	private String name;
	private String address;
	private String phone;
	private String fax;
	private String abbr;
	private Collection<VendorItem> vendorItems;

	public Vendor() {
		this.vendorItems = new LinkedList<VendorItem>();
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAbbr() {
		return this.abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public Collection<VendorItem> getVendorItems() {
		return vendorItems;
	}

	public void setVendorItems(Collection<VendorItem> vendorItems) {
		this.vendorItems = vendorItems;
	}
	
	public void addVendorItem(VendorItem vendorItem) {
		this.vendorItems.add(vendorItem);
	}
}
