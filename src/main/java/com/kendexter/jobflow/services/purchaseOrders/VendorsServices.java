package com.kendexter.jobflow.services.purchaseOrders;

import java.sql.Connection;
import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("vendors")
public class VendorsServices {
	private static Logger logger = Logger.getLogger(VendorsServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("vendorList")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendorList() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Vendor> vendors = VendorsDAO.getVendorList(connection);
			return Response.ok().entity(vendors).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getVendor")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendor(@QueryParam("name") String name) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Vendor vendor = VendorsDAO.getVendorByName(connection, name);
			return Response.ok().entity(vendor).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyVendorName")
	@GET
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyVendorName(@QueryParam("name") String name, @QueryParam("id") int id) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Vendor vendor = VendorsDAO.getVendorByName(connection, name);
			if (vendor != null && id != vendor.getId()) {
				ErrorMsg errorMsg = new ErrorMsg("Vendor name " + name + " already in use.");
				return Response.ok().entity(errorMsg).build();
			}
			return Response.ok().entity(new ErrorMsg(null)).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertVendor")
	@POST
	@RolesAllowed({ AppContext.APPROVE_PURCHASE_ORDERS, AppContext.ENTER_PURCHASE_ORDERS })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertVendor(Vendor vendor) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			VendorsDAO.upsertVendor(connection, vendor);
			connection.commit();
			return Response.ok().entity(vendor).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}	
}
