package com.kendexter.jobflow.services.purchaseOrders;

import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;
import com.kendexter.pdf.Line;
import com.kendexter.pdf.PDFBoxServlet;

public class PurchaseOrderPDF extends PDFBoxServlet {
	private static final SimpleDateFormat sdf = new SimpleDateFormat("M/d/yy");
	private static final Color GRAY = Color.GRAY;
	private static final Color BLACK = Color.BLACK;
	private static final PDFont TIMES = PDType1Font.TIMES_ROMAN;
	private static final PDFont FIXED = PDType1Font.COURIER_BOLD;
	private static final DecimalFormat DF2 = new DecimalFormat("#,###,###.00;#,###,###.00CR");
	private static final DecimalFormat DF0 = new DecimalFormat("#,###,###.######");
	private static final DecimalFormat DF6 = new DecimalFormat("#,###,###.00####");
	private static final DecimalFormat $DF = new DecimalFormat("\u00A4#,###,###.00;\u00A4#,###,###.00CR");
	public static final int LINESPERPAGE = 21;
	private int pageNo;
	private int totalPages;

	public PurchaseOrderPDF(HttpServletRequest request) {
		super(request);
	}

	protected void process(User user) throws Exception {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();

			PurchaseOrderForm po = PurchaseOrdersDAO.getPurchaseOrder(connection, request.getParameter("poNumber"));
			document.getDocumentInformation().setTitle("PO " + po.getPoNumber());
			// Create a page to add to the document
			ArrayList<PurchaseOrderItemForm> items = po.getPurchaseOrderItems();
			if (items.size() == 0) {
				items.add(new PurchaseOrderItemForm());
			}
			totalPages = items.size() / LINESPERPAGE;
			if (items.size() % LINESPERPAGE != 0)
				totalPages++;
			for (int i = 0; i < totalPages; i++) {
				pageNo = i + 1;
				int beginLine = i * LINESPERPAGE;
				PurchaseOrderItemForm[] item = null;
				if (beginLine + LINESPERPAGE > items.size()) {  //Partial page
					item = new PurchaseOrderItemForm[items.size() - beginLine];
					int ix = 0;
					for (int k = beginLine; k < items.size(); k++) {
						item[ix++] = items.get(k);
					}
				} else {    // Full  page.
					item = new PurchaseOrderItemForm[LINESPERPAGE];
					int ix = 0;
					for (int k = beginLine; k < beginLine + LINESPERPAGE; k++) {
						item[ix++] = items.get(k);
					}
				}
				addPage(612f, 792f);
				dateAndPONumber(po);
				addText("P u r c h a s e   O r d e r", FIXED, 24f, 106f, 85f, Color.BLACK);
				headers1(po, AppContext.getParameterValue(AppContext.BILL_TO_ADDRESS));
				headers2(po);
				body(po, item);
				footer(po);
				if (po.isPending() || po.isCancelled()) {
					addText("VOID", FIXED, 150, 0, 78);
					addText("VOID", FIXED, 150, 150, 600);
				}
				pageContentStream.close();
			}
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
			}
		}

	}

	/*
	 * Job, Account, Expected date, and Ship Via
	 */
	private static final float jobXBase = 212f;
	private static final float jobYBase = 110f;
	private static final float jobLength = 77f;
	private static final float jobHeight = 40f;
	private static final float jobHeaderHeight = 13f;
	private static final float acctXBase = 349f;
	private static final float acctLength = 77f;
	private static final float expectedXBase = acctXBase + acctLength;
	private static final float expectedLength = 77f;
	private static final float shipViaXBase = expectedXBase + expectedLength;
	private static final float shipViaLength = 77f;
	private static final float referenceYBase = jobYBase + 47;

	private void headers1(PurchaseOrderForm po, String billToAddress) throws IOException {
		String address[] = billToAddress.split("\\\n");
		if (address.length > 0)
			addText(address[0], PDType1Font.COURIER, 12f, 12f, 110f, BLACK);
		if (address.length > 1)
			addText(address[1], PDType1Font.COURIER, 12f, 12f, 122f, BLACK);
		if (address.length > 2)
			addText(address[2], PDType1Font.COURIER, 12f, 12f, 134f, BLACK);
		if (address.length > 3)
			addText(address[3], PDType1Font.COURIER, 12f, 12f, 146f, BLACK);
		/*
		 * Job Number
		 */
		// Horizontal
		addLine(new Line(jobXBase, jobYBase, jobXBase + jobLength, jobYBase, GRAY));
		addLine(new Line(jobXBase, jobYBase + jobHeaderHeight, jobXBase + jobLength, jobYBase + jobHeaderHeight, GRAY));
		addLine(new Line(jobXBase, jobYBase + jobHeight, jobXBase + jobLength, jobYBase + jobHeight, GRAY));
		// Vertical
		addLine(new Line(jobXBase, jobYBase, jobXBase, jobYBase + jobHeight, GRAY));
		addLine(new Line(jobXBase + jobLength, jobYBase, jobXBase + jobLength, jobYBase + jobHeight, GRAY));
		// Job Number label and data
		addText("Job Number", jobXBase, jobYBase, jobLength, 12f, TIMES, 12, GRAY);
		addText(po.getJobNumber(), jobXBase, jobYBase + 18, jobLength, 24f, TIMES, 14, BLACK);
		/*
		 * Account #, Excpected date, and Ship Via horizontal lines
		 */
		// Horizontal
		addLine(new Line(acctXBase, jobYBase, acctXBase + acctLength, jobYBase, GRAY));
		addLine(new Line(acctXBase, jobYBase + jobHeaderHeight, acctXBase + acctLength, jobYBase + jobHeaderHeight, GRAY));
		addLine(new Line(acctXBase, jobYBase + jobHeight, acctXBase + acctLength, jobYBase + jobHeight, GRAY));
		// Vertical
		addLine(new Line(acctXBase, jobYBase, acctXBase, jobYBase + jobHeight, GRAY));
		addLine(new Line(acctXBase + acctLength, jobYBase, acctXBase + acctLength, jobYBase + jobHeight, GRAY));
		/*
		 * Account #
		 */
		addText("Account #", acctXBase, jobYBase, acctLength, 12f, TIMES, 12, GRAY);
		addText(po.getVendorAccount(), acctXBase, 126f, acctLength, 24f, TIMES, 12, BLACK);
		/*
		 * Expected date
		 */
		// Horizontal
		addLine(new Line(expectedXBase, jobYBase, expectedXBase + expectedLength, jobYBase, GRAY));
		addLine(new Line(expectedXBase, jobYBase + jobHeaderHeight, expectedXBase + expectedLength, jobYBase + jobHeaderHeight, GRAY));
		addLine(new Line(expectedXBase, jobYBase + jobHeight, expectedXBase + expectedLength, jobYBase + jobHeight, GRAY));
		// Vertical
		addLine(new Line(expectedXBase, jobYBase, expectedXBase, jobYBase + jobHeight, GRAY));
		addLine(new Line(expectedXBase + expectedLength, jobYBase, expectedXBase + expectedLength, jobYBase + jobHeight, GRAY));
		addText("Expected", expectedXBase, jobYBase, expectedLength, 12f, TIMES, 12, GRAY);
		if (po.getExpectedDate() != null)
			addText(po.getExpectedDate(), expectedXBase, 126f, expectedLength, 24f, TIMES, 14, BLACK);
		else
			addText(" ", expectedXBase, 126f, expectedLength, 24f, TIMES, 14, BLACK);
		/*
		 * Ship via
		 */
		// Horizontal
		addLine(new Line(shipViaXBase, jobYBase, shipViaXBase + shipViaLength, jobYBase, GRAY));
		addLine(new Line(shipViaXBase, jobYBase + jobHeaderHeight, shipViaXBase + shipViaLength, jobYBase + jobHeaderHeight, GRAY));
		addLine(new Line(shipViaXBase, jobYBase + jobHeight, shipViaXBase + shipViaLength, jobYBase + jobHeight, GRAY));
		// Vertical
		addLine(new Line(shipViaXBase, jobYBase, shipViaXBase, jobYBase + jobHeight, GRAY));
		addLine(new Line(shipViaXBase + shipViaLength, jobYBase, shipViaXBase + shipViaLength, jobYBase + jobHeight, GRAY));
		addText("Ship Via", shipViaXBase, jobYBase, shipViaLength, 12f, TIMES, 12, GRAY);
		addText(po.getShipVia(), shipViaXBase, 126f, shipViaLength, 24f, TIMES, 14, BLACK);
		/*
		 * Reference
		 */
		if (po.getReference() != null && po.getReference().trim().length() > 0) {
			addText("Reference:", jobXBase, referenceYBase, 70f, 12f, TIMES, 14, BLACK);
			addText(po.getReference(), jobXBase + 72f, referenceYBase, 200f, 12f, TIMES, 14, BLACK);
		}
	}

	private void footer(PurchaseOrderForm po) throws IOException {
		/*
		 * Vendor phone,fax; ordered by, approved by
		 */
		//		private void footer(Page objPage, int page, int of, PurchaseOrderForm po) {
		/*
		 * Vendor phone and fax
		 */
		// Horizontal lines
		addLine(new Line(12f, 680f, 186f, 680f));
		addLine(new Line(12f, 692f, 186f, 692f));
		addLine(new Line(12f, 710f, 186f, 710f));
		// Vertical lines
		addLine(new Line(12f, 680f, 12f, 710f));
		addLine(new Line(99f, 680f, 99f, 710f));
		addLine(new Line(186f, 680f, 186f, 710f));
		//
		addText("Phone #", 12f, 680f, 87f, 12f, TIMES, 12f, GRAY);
		addText("Fax #", 99f, 680f, 87f, 12f, TIMES, 12f, GRAY);
		//  
		float fontSize = po.getVendorPhone().length() < 13 ? 12f : 8f;
		String text = po.getVendorPhone().length() < 19 ? po.getVendorPhone() : po.getVendorPhone().substring(0, 18);
		addText(text, 2.5f, 692f, 87f, 14f, FIXED, fontSize, BLACK);
		//
		fontSize = po.getVendorFax().length() < 13 ? 12f : 8f;
		text = po.getVendorFax().length() < 19 ? po.getVendorFax() : po.getVendorFax().substring(0, 18);
		addText(text, 90f, 692f, 87f, 14f, FIXED, fontSize, BLACK);
		/*
		 * Ordered by, approved by
		 */
		addText("Ordered by:", 186f, 680f, 90f, 12f, TIMES, 12f, GRAY);
		addText(po.getOrderedBy(), 284f, 680f, 90f, 12, TIMES, 12f, BLACK);
		addText("Approved by:", 186f, 692f, 90f, 12f, TIMES, 12f, GRAY);
		addText(po.getApprovedBy(), 284f, 692f, 90f, 12, TIMES, 12f, BLACK);
		addText("Page " + pageNo + " of " + totalPages, 500f, 742f, 100f, 14f, TIMES, 12f, BLACK);
	}

	/*
	 * Purchase order items table
	 */
	private void body(PurchaseOrderForm po, PurchaseOrderItemForm[] item) throws IOException {
		/*
		 * Purchase order items addLine(new Line(dy(Page objPage, PurchaseOrderForm po, PurchaseOrderItemForm[] item) {
			/*
			 * Headers
			 */
		float y = 280f;
		addLine(new Line(12f, y, 605f, y, GRAY));
		addText("Item", 35f, y, 57f, 14f, TIMES, 12f, GRAY);
		addText("Description", 144f, y, 271f, 14f, TIMES, 12f, GRAY);
		addText("Qty", 428f, y, 33f, 14f, TIMES, 12f, GRAY);
		addText("Rate", 484f, y, 62f, 14f, TIMES, 12f, GRAY);
		addText("Amount", 540f, y, 60f, 14f, TIMES, 12f, GRAY);
		/*
		* Vertical lines
		*/
		addLine(new Line(12f, 280f, 12f, 659.5f, GRAY));
		addLine(new Line(144f, 280f, 144f, 659.5f, GRAY));
		addLine(new Line(425f, 280f, 425f, 659.5f, GRAY));
		addLine(new Line(477f, 280f, 477f, 659.5f, GRAY));
		addLine(new Line(541f, 280f, 541f, 679f, GRAY));
		addLine(new Line(605F, 280f, 605F, 679f, GRAY));
		/*
		 * Horizontal lines and body
		*/
		y += 14f;
		for (int i = 0; i < LINESPERPAGE; i++) {
			addLine(new Line(12f, y, 605f, y, GRAY));
			if (i < item.length) {
				addText(item[i].getItem(), 14f, y, 97f, 12f, FIXED, 8f, BLACK);
				addText(item[i].getDescription(), 144f, y, 271f, 7f, FIXED, 8f, BLACK);
				//Right-align the quantity
				if (!item[i].getQuantity().equals("")) {
					String text = DF0.format(item[i].getQuantity());
					while (text.length() < 10)
						text = " " + text;
					addText(text + "", 416f, y, 48f, 14f, FIXED, 8f, BLACK);
				}
				//Decimal-align the rate
				if (!item[i].getRate().equals("")) {
					StringTokenizer st = new StringTokenizer(DF6.format(item[i].getRate()), ".");
					String right = st.nextToken();
					String left = "";
					if (st.hasMoreTokens()) {
						left = right;
						right = st.nextToken();
					}
					while (left.length() < 6)
						left = " " + left;
					while (right.length() < 6)
						right = right + " ";
					addText(left + "." + right, 468f, y, 60f, 14f, FIXED, 8f, BLACK);
				}
				//Right-align the total
				if (!item[i].getTotal().equals("")) {
					{
						String text = DF2.format(item[i].getTotal());
						while (text.length() < 13)
							text = " " + text;
						addText(text + "", 532f, y, 90f, 14f, FIXED, 8f, BLACK);
					}
				} else
					addText("", 487f, y, 90f, 14f, FIXED, 8f, BLACK);
			}
			y += 17.5;
		}
		addLine(new Line(12f, y, 605f, y, GRAY));  // bottom line
		/*
		 * Footer
		 */
		addLine(new Line(540f, 679f, 605f, 679f, GRAY));
		addText("Total", 500f, 665f, 35f, 14f, TIMES, 12f, BLACK);
		{		//Right-align the total
			String text = $DF.format(po.getTotal());
			while (text.length() < 14)
				text = " " + text;
			addText(text, 527f, 665f, 90f, 14f, FIXED, 8f, BLACK);
		}
	}

	private void headers2(PurchaseOrderForm po) throws IOException {
		/*
		 * Vendor
		 */
		// Horizonal
		addLine(new Line(12f, 180f, 212f, 180f, GRAY));
		addLine(new Line(12f, 192f, 212f, 192f, GRAY));
		addLine(new Line(12f, 252f, 212f, 252f, GRAY));
		// Vertical
		addLine(new Line(12f, 180f, 12f, 252f, GRAY));
		addLine(new Line(212f, 180f, 212f, 252f, GRAY));
		// Label and address
		addText("Vendor", 12f, 180f, 200f, 12f, TIMES, 12f, GRAY);
		addText(po.getVendorName(), 15f, 194f, 197f, 12f, TIMES, 12f, BLACK);
		String address[] = po.getVendorAddress().split("\\\n");
		float ybase = 204f;
		float xbase = 15f;
		if (address.length > 0)
			addText(address[0], TIMES, 12f, xbase, ybase, BLACK);
		if (address.length > 1)
			addText(address[1], TIMES, 12f, xbase, ybase + 12, BLACK);
		if (address.length > 2)
			addText(address[2], TIMES, 12f, xbase, ybase + 24, BLACK);
		if (address.length > 3)
			addText(address[3], TIMES, 12f, xbase, ybase + 36, BLACK);
		//		addText(po.getVendorAddress(), 15f, 208f, 197f, 48f, TIMES, 12f, BLACK);
		/*
		 * This company
		 */
		// Horizonal
		addLine(new Line(380f, 180f, 580f, 180f, GRAY));
		addLine(new Line(380f, 192f, 580f, 192f, GRAY));
		addLine(new Line(380f, 252f, 580f, 252f, GRAY));
		// Vertical
		addLine(new Line(380f, 180f, 380f, 252f, GRAY));
		addLine(new Line(580f, 180f, 580f, 252f, GRAY));
		// Label and address
		addText("Ship To", 380f, 180f, 200f, 12f, TIMES, 12f, GRAY);
		address = po.getShipTo().split("\\\n");
		if (address.length > 0)
			addText(address[0], 383f, 194f, 197f, 60f, TIMES, 12f, BLACK);
		if (address.length > 1)
			addText(address[1], 383f, 206f, 197f, 60f, TIMES, 12f, BLACK);
		if (address.length > 2)
			addText(address[2], 383f, 218f, 197f, 60f, TIMES, 12f, BLACK);
		if (address.length > 3)
			addText(address[3], 383f, 230f, 197f, 60f, TIMES, 12f, BLACK);
		//		addText(po.getShipTo(), 383f, 194f, 197f, 60f, TIMES, 12f, BLACK);
	}

	private void dateAndPONumber(PurchaseOrderForm po) throws IOException {
		// Horizontal
		addHorizontal(1f, 460f, 580, 27f, GRAY);
		addHorizontal(1f, 460f, 580, 39f, GRAY);
		addHorizontal(1f, 460f, 580, 63f, GRAY);
		// Vertical
		addVertical(1f, 460f, 27f, 63f, GRAY);
		addVertical(1f, 520f, 27f, 63f, GRAY);
		addVertical(1f, 580f, 27f, 63f, GRAY);
		// Labels and data
		addText("   Date", TIMES, 12f, 460f, 27f, GRAY);
		addText(sdf.format(po.getEntryDate()), TIMES, 12f, 460f, 45f, BLACK);
		addText("P.O. No.", TIMES, 12f, 520f, 27f, GRAY);
		addText(po.getPoNumber() + "", TIMES, 12f, 520f, 45f, BLACK);
	}

	/*
	 * Legacy PDF generator hacks
	 */
	protected void addText(String text, PDFont font, float fontSize, float x, float y, Color color) throws IOException {
		super.addText(text, font, fontSize, x + 10, y + 10, color);
	}

	private void addLine(Line line) throws IOException {
		if (line.x1 == line.x2) {
			addVertical(1f, line.x1, line.y1, line.y2, line.color);
		} else if (line.y1 == line.y2) {
			addHorizontal(1f, line.x1, line.x2, line.y1, line.color);
		} else {
			System.err.println("Invalid horizontal or vertical line");
		}
	}

	private void addText(int text, float x, float y, float notused, float alsonotused, PDFont font, float fontSize, Color color) throws IOException {
		addText(text+"", font, fontSize, x, y, color);
	}

	private void addText(String text, float x, float y, float notused, float alsonotused, PDFont font, float fontSize, Color color) throws IOException {
		addText(text, font, fontSize, x, y, color);
	}
}
