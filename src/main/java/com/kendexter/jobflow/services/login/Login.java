package com.kendexter.jobflow.services.login;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.TokenAttribute;
import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;
import com.kendexter.jobflow.services.users.UserServices;
import com.kendexter.jobflow.services.users.UsersDAO;

@SuppressWarnings("unchecked")
@Path("login")
public class Login {
	private static Logger logger = Logger.getLogger(Login.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@POST
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(SignOn signOn) {
		try {
			/*
			 * Edit for valid credentials
			 */
			User user = UserServices.retrieveUser(signOn.getUserID());
			if (user == null)
				return Response.serverError().entity("{\"error\":\"Invalid credentials\"}").build();
			if (!user.getPass().equals(signOn.getPassword())) {
				return Response.ok().entity("{\"error\":\"Invalid credentials\"}").build();
			}
			if (!signOn.getCompanyCode().equals(AppContext.getParameterValue("company_code"))) {
				return Response.ok().entity("{\"error\":\"Invalid credentials\"}").build();
			}

			Collection<Integer> monitoredJobs = UserServices.retrieveMonitoredJobs(signOn.getUserID());

			/*
			 * Get a random number
			 */
			SecureRandom random = new SecureRandom();
			byte bytes[] = new byte[20];
			random.nextBytes(bytes);
			String token = bytes.toString();
			TokenAttribute tokenAttribute = new TokenAttribute(token, user, monitoredJobs);

			HashMap<String, TokenAttribute> tokenMap = (HashMap<String, TokenAttribute>) context.getAttribute("tokenMap");
			tokenMap.put(token, tokenAttribute);
			return Response.ok().entity(tokenAttribute).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}
	
	@GET
	@PermitAll
	@Path("getUserColors") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserColors() {
		try {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				LinkedList<String []> userColors = new LinkedList<String []>();
				PreparedStatement select = connection.prepareStatement("SELECT NAME, COLOR FROM USERS");
				ResultSet rs = select.executeQuery();
				while (rs.next()) {
					userColors.add(new String [] {rs.getString(1), rs.getString(2)});
				}
				return Response.ok().entity(userColors).build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}
	
	@GET
	@PermitAll
	@Path("getJobsWPurchaseOrders") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobsWPurchaseOrders() {
		try {
			AppContext.loadJobsWithPurchaseOrders();
			return Response.ok().entity(context.getAttribute("jobsWPurchaseOrders")).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}

	@GET
	@PermitAll
	@Path("logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout() {
		try {
			try {  // Delete the bearer token. If it's not there it's probably a restart.
				Map<String, TokenAttribute> map = ((Map<String, TokenAttribute>) context.getAttribute("tokenMap"));
				final MultivaluedMap<String, String> headers = requestContext.getHeaders();
				String bearerToken = headers.get("authorization").iterator().next().substring(7);
				map.remove(bearerToken);
			} catch (Exception e1) {
			}
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}

	/*
	 * Since I haven't found a way to use request headers for window open, I generate a onetime token for the window popups
	 *  and check it outside the Authentication Filter 
	 */
	@GET
	@PermitAll
	@Path("onetimeToken")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getOnetimeToken() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		String token = bytes.toString();
		User user = AppContext.getUser(context, requestContext);
		TokenAttribute tokenAttribute = new TokenAttribute(token, user, null);
		HashMap<String, TokenAttribute> tokenMap = (HashMap<String, TokenAttribute>) context.getAttribute("tokenMap");
		tokenMap.put(token, tokenAttribute);
		return Response.ok().entity(token).build();
	}

	public static HashSet<String> getRoles(String userID) throws Exception {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			HashSet<String> roles = UsersDAO.getRoles(connection, userID);
			return roles;
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
