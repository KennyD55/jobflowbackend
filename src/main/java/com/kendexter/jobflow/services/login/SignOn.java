package com.kendexter.jobflow.services.login;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SignOn implements Serializable {
	private String userID;
	private String password;
	private String companyCode;

	public SignOn() {
	}

	public String getUserID() {
		return userID;
	}

	public void setUserId(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
}
