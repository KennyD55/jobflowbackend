package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;
import java.util.StringTokenizer;

@SuppressWarnings("serial")
public class Runtime implements Serializable {
	private int hours = 0;
	private int minutes = 0;
	private int totalMinutes = 0;
	private boolean valid;

	public Runtime(String runtime) {
		try {
			runtime = runtime.trim();
			if (runtime.startsWith(":")) {
				runtime = runtime.substring(1);
				hours = 0;
				minutes = Integer.parseInt(runtime);
				totalMinutes = minutes;
				valid = true;
				return;
			}
			StringTokenizer st = new StringTokenizer(runtime, ":");
			String st1 = st.nextToken();
			if (st1.length() > 0)
				hours = Integer.parseInt(st1);
			if (st.hasMoreTokens()) {
				st1 = st.nextToken();
				if (st1.length() > 0)
					minutes = Integer.parseInt(st1);
			}
			totalMinutes = hours * 60 + minutes;
			valid = true;
		} catch (Exception e) { // Default to invalid
		}
	}

	public Runtime(int hours, int minutes) {
		this.hours = hours;
		this.minutes = minutes;
		totalMinutes = hours * 60 + minutes;
		valid = true;
	}

	public Runtime(int totalMinutes) {
		this.totalMinutes = totalMinutes;
		this.hours = totalMinutes / 60;
		this.minutes = totalMinutes % 60;
		valid = true;
	}

	public String getRunTime() {
		if (minutes < 10)
			return hours + ":" + "0" + minutes;
		else
			return hours + ":" + minutes;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getTotalMinutes() {
		return totalMinutes;
	}

	public void setTotalMinutes(int totalMinutes) {
		this.totalMinutes = totalMinutes;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
