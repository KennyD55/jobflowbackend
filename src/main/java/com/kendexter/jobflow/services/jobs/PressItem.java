package com.kendexter.jobflow.services.jobs;
import java.io.Serializable;

 
@SuppressWarnings("serial")
public class PressItem implements Serializable {
	private int jobNumber;
	private int seq;
	private String formNumber;
	private String press;
	private String form;
	private String pressColor;
	private String colorOfInk;
	private String basicNoOfSheets;
	private String overs;
	private String total;
	private String finalCount;
	private String initials;
	private String runTime;
	private long taskId;
	private double overRidePressHours;

	public String getRunTime() {
		return runTime == null ? "" : runTime;
	}

	public void setRunTime(String runTime) {
		Runtime runtime = new Runtime(runTime);
		if (runtime.isValid())
			this.runTime = runtime.getRunTime();
		else
			this.runTime = runTime;
	}

	public boolean empty() {
		return ((formNumber == null || formNumber.trim().length() == 0)
				&& (press == null || press.trim().length() == 0)
				&& (form == null || form.trim().length() == 0)
				&& (pressColor == null || pressColor.trim().length() == 0)
				&& (colorOfInk == null || colorOfInk.trim().length() == 0)
				&& (basicNoOfSheets == null || basicNoOfSheets.trim().length() == 0)
				&& (overs == null || overs.trim().length() == 0)
				&& (total == null || total.trim().length() == 0)
				&& (finalCount == null || finalCount.trim().length() == 0) && (initials == null || initials.trim().length() == 0));
	}

	/*** JSON Gets ***/

	public int getTotalMinutes() {
		Runtime runtime = new Runtime(runTime);
		if (runtime.isValid()) {
			return runtime.getTotalMinutes();
		} else {
			return 0;
		}
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getBasicNoOfSheets() {
		return basicNoOfSheets == null ? "" : basicNoOfSheets;
	}

	public String getColorOfInk() {
		return colorOfInk == null ? "" : colorOfInk;
	}

	public String getFinalCount() {
		return finalCount == null ? "" : finalCount;
	}

	public String getForm() {
		return form == null ? "" : form;
	}

	public String getFormNumber() {
		return formNumber == null ? "" : formNumber;
	}

	public String getInitials() {
		return initials == null ? "" : initials;
	}

	public String getOvers() {
		return overs == null ? "" : overs;
	}

	public String getPress() {
		return press == null ? "" : press;
	}

	public String getPressColor() {
		return pressColor == null ? "" : pressColor;
	}

	public String getTotal() {
		return total == null ? "" : total;
	}

	public void setBasicNoOfSheets(String basicNoOfSheets) {
		this.basicNoOfSheets = basicNoOfSheets;
	}

	public void setColorOfInk(String colorOfInk) {
		this.colorOfInk = colorOfInk;
	}

	public void setFinalCount(String finalCount) {
		this.finalCount = finalCount;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public void setOvers(String overs) {
		this.overs = overs;
	}

	public void setPress(String press) {
		this.press = press;
	}

	public void setPressColor(String pressColor) {
		this.pressColor = pressColor;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public double getOverRidePressHours() {
		return overRidePressHours;
	}

	public void setOverRidePressHours(double overRidePressHours) {
		this.overRidePressHours = overRidePressHours;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}
}
