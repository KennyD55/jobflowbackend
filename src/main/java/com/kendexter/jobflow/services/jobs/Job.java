package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class Job implements Serializable {
	private int jobNumber;
	private String accountNumber;
	private Date dateDue;
	private Timestamp dateSubmitted;
	private String whoSubmitted;
	private boolean hot;
	private String status;
	private String salesrep;
	private Date billedDate;
	private Date shippedDate;
	private Date deliveryDate;
	private String customerName;
	private String customerAddress;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private boolean cod;
	private int quoted;
	public static final int QUOTEDTRUE = 1;
	public static final int QUOTEDFALSE = 2;
	private String quoteNumber;
	private String newReprintRevised;
	public static final String NEW = "New";
	public static final String REPRINT = "Reprint";
	public static final String REVISED = "Revised";
	private String previousJobNumber;
	private String poNumber;
	private int pressCheck;
	public static final int PRESSCHECKFALSE = 1;
	public static final int PRESSCHECKTRUE = 2;
	private String pressCheckContactName;
	private String pressCheckContactPhone;
	private String description;
	private boolean pdf2Go;
	private boolean bwLaser;
	private boolean iris;
	private boolean digitalColor;
	private String proofOther;
	private String macpc;
	private boolean cdRom;
	private boolean ftp;
	private boolean floppy;
	private boolean email;
	private boolean zip;
	private boolean jaz;
	private boolean film;
	private String typeOther;
	private String proofTo;
	private Date proofToDate;
	private String prePressNotes;
	private String trimTo1;
	private String trimTo2;
	private String foldTo1;
	private String foldTo2;
	private String shrinkIn;
	private boolean score;
	private boolean perf;
	private boolean drill;
	private String drillSize;
	private boolean saddlestitch;
	private boolean pocket;
	private String binderyNotes;
	private String shipTo;
	private String shippingAddress;
	private String shipVia;
	private String shippingNotes;
	private String deliveryTicketNumber;
	private String quantityShipped;
	private String numberOfBoxes;
	private String quantityPerBox;
	private String numberOfSamples;
	private String samplesTo;
	private String deliveryNotes;
	private String chargeableRevisions;
	private double price;
	private String priceComments;
	private Timestamp jeopardyTime;
	private Date orderedDate;
	private int digitalOffset;
	public static final int DIGITAL = 1;
	public static final int OFFSET = 2;
	private boolean archived;
	private boolean printToday;
	private boolean backupFlag;
	private boolean scheduled;
	private int mailed;
	public static final int MAILREQUIRED = 1;
	public static final int MAILNOTREQUIRED = 2;
	private String mailingVendor;
	private boolean inHouse;
	private Date dropDate;
	private boolean letterpress;
	private boolean quoteGenerated;
	private int quoteQuantity;
	private boolean stockPrint;  // The only use for this is have a slightly different color on the "STOCK" button after the first time it is clicked
	private Timestamp lastUpdate;
	private String whoModified;
	private ArrayList<JobItem> jobItems;
	private ArrayList<StockItem> stockItems;
	private ArrayList<PressItem> pressItems;
	private ArrayList<OSItem> osItems;
	//	private String loadQuoteNumber;
	//	
	@SuppressWarnings("unused") private static final Logger logger = Logger.getLogger(Job.class);

	public Job() {
		jobItems = new ArrayList<JobItem>();
		stockItems = new ArrayList<StockItem>();
		pressItems = new ArrayList<PressItem>();
		osItems = new ArrayList<OSItem>();
		setQuoted(QUOTEDFALSE);
	}

	double retrieveStockTotal() {
		double total = 0d;
		for (int i = 0;i < stockItems.size();i++) {
		StockItem item = this.stockItems.get(i);
			try {
				double pb = Double.parseDouble(item.getPb());
				double sheets = Double.parseDouble(item.getSheets());
				double ppm = Double.parseDouble(item.getPpm());
				total += (sheets / pb) * ppm;
			} catch (Exception e) {
			}
		};
		return total;
	}

	/*
	 * JSON only
	 */
	public boolean isInJeopardy() {
		return this.jeopardyTime != null && this.jeopardyTime.getTime() < System.currentTimeMillis() && !archived;
	}

	/*
	 * End JSON only
	 */
	public ArrayList<OSItem> getOsItems() {
		return osItems;
	}

	public void setOsItems(ArrayList<OSItem> outsideServiceItem) {
		this.osItems = outsideServiceItem;
	}

	public void addOsItem(OSItem osItem) {
		if (osItem != null && !osItem.empty())
			this.osItems.add(osItem);
	}

	public ArrayList<PressItem> getPressItems() {
		return pressItems;
	}

	public void setPressItems(ArrayList<PressItem> pressItem) {
		this.pressItems = pressItem;
	}

	public void addPressItem(PressItem pressItem) {
		if (pressItem != null && !pressItem.empty())
			this.pressItems.add(pressItem);
	}

	public ArrayList<StockItem> getStockItems() {
		return stockItems;
	}

	public void setStockItems(ArrayList<StockItem> stockItem) {
		this.stockItems = stockItem;
	}

	public void addStockItem(StockItem stockItem) {
		if (stockItem != null && !stockItem.empty())
			this.stockItems.add(stockItem);
	}

	public ArrayList<JobItem> getJobItems() {
		return jobItems;
	}

	public void setJobItems(ArrayList<JobItem> jobItem) {
		this.jobItems = jobItem;
	}

	public void addJobItem(JobItem jobItem) {
		if (jobItem != null && !jobItem.empty())
			this.jobItems.add(jobItem);
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public Date getBilledDate() {
		return billedDate;
	}

	public String getBinderyNotes() {
		return binderyNotes == null ? "" : binderyNotes;
	}

	public boolean isBwLaser() {
		return bwLaser;
	}

	public boolean isCdRom() {
		return cdRom;
	}

	public String getChargeableRevisions() {
		return chargeableRevisions == null ? "" : chargeableRevisions;
	}

	public boolean isCod() {
		return cod;
	}

	public String getContactEmail() {
		return contactEmail == null ? "" : contactEmail;
	}

	public String getContactName() {
		return contactName == null ? "" : contactName;
	}

	public String getContactPhone() {
		return contactPhone == null ? "" : contactPhone;
	}

	public String getCustomerAddress() {
		return customerAddress == null ? "" : customerAddress;
	}

	public String getCustomerName() {
		return customerName == null ? "" : customerName;
	}

	public Date getDateDue() {
		return dateDue;
	}

	public Timestamp getDateSubmitted() {
		return dateSubmitted;
	}

	public String getDeliveryNotes() {
		return deliveryNotes == null ? "" : deliveryNotes;
	}

	public String getDeliveryTicketNumber() {
		return deliveryTicketNumber == null ? "" : deliveryTicketNumber;
	}

	public String getDescription() {
		return description == null ? "" : description;
	}

	public boolean isDigitalColor() {
		return digitalColor;
	}

	public boolean isDrill() {
		return drill;
	}

	public String getDrillSize() {
		return drillSize == null ? "" : drillSize;
	}

	public boolean isEmail() {
		return email;
	}

	public boolean isFilm() {
		return film;
	}

	public boolean isFloppy() {
		return floppy;
	}

	public String getFoldTo1() {
		return foldTo1 == null ? "" : foldTo1;
	}

	public String getFoldTo2() {
		return foldTo2 == null ? "" : foldTo2;
	}

	public boolean isFtp() {
		return ftp;
	}

	public boolean isHot() {
		return hot;
	}

	public boolean isIris() {
		return iris;
	}

	public boolean isJaz() {
		return jaz;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public String getMacpc() {
		return macpc == null ? "" : macpc;
	}

	public String getNewReprintRevised() {
		return newReprintRevised == null ? "" : newReprintRevised;
	}

	public String getNumberOfBoxes() {
		return numberOfBoxes == null ? "" : numberOfBoxes;
	}

	public String getNumberOfSamples() {
		return numberOfSamples == null ? "" : numberOfSamples;
	}

	public boolean isPdf2Go() {
		return pdf2Go;
	}

	public boolean isPerf() {
		return perf;
	}

	public boolean isPocket() {
		return pocket;
	}

	public String getPoNumber() {
		return poNumber == null ? "" : poNumber;
	}

	public String getPrePressNotes() {
		return prePressNotes == null ? "" : prePressNotes;
	}

	public int getPressCheck() {
		return pressCheck;
	}

	public String getPressCheckContactName() {
		return pressCheckContactName == null ? "" : pressCheckContactName;
	}

	public String getPressCheckContactPhone() {
		return pressCheckContactPhone == null ? "" : pressCheckContactPhone;
	}

	public String getPreviousJobNumber() {
		return previousJobNumber == null ? "" : previousJobNumber;
	}

	public double getPrice() {
		return price;
	}

	public String getProofOther() {
		return proofOther == null ? "" : proofOther;
	}

	public String getProofTo() {
		return proofTo == null ? "" : proofTo;
	}

	public Date getProofToDate() {
		return proofToDate;
	}

	public String getQuantityPerBox() {
		return quantityPerBox == null ? "" : quantityPerBox;
	}

	public String getQuantityShipped() {
		return quantityShipped == null ? "" : quantityShipped;
	}

	public int getQuoted() {
		return quoted;
	}

	public String getQuoteNumber() {
		return quoteNumber == null ? "" : quoteNumber;
	}

	public boolean isSaddlestitch() {
		return saddlestitch;
	}

	public String getSalesrep() {
		return salesrep == null ? "" : salesrep;
	}

	public String getSamplesTo() {
		return samplesTo == null ? "" : samplesTo;
	}

	public boolean isScore() {
		return score;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public String getShippingAddress() {
		return shippingAddress == null ? "" : shippingAddress;
	}

	public String getShippingNotes() {
		return shippingNotes == null ? "" : shippingNotes;
	}

	public String getShipTo() {
		return shipTo == null ? "" : shipTo;
	}

	public String getShipVia() {
		return shipVia == null ? "" : shipVia;
	}

	public String getShrinkIn() {
		return shrinkIn == null ? "" : shrinkIn;
	}

	public String getStatus() {
		return status == null ? "" : status;
	}

	public String getTrimTo1() {
		return trimTo1 == null ? "" : trimTo1;
	}

	public String getTrimTo2() {
		return trimTo2 == null ? "" : trimTo2;
	}

	public String getTypeOther() {
		return typeOther == null ? "" : typeOther;
	}

	public String getWhoSubmitted() {
		return whoSubmitted == null ? "" : whoSubmitted;
	}

	public boolean isZip() {
		return zip;
	}

	public void setBilledDate(Date billedDate) {
		this.billedDate = billedDate;
	}

	public void setBinderyNotes(String binderyNotes) {
		this.binderyNotes = binderyNotes;
	}

	public void setBwLaser(boolean bwLaser) {
		this.bwLaser = bwLaser;
	}

	public void setCdRom(boolean cdRom) {
		this.cdRom = cdRom;
	}

	public void setChargeableRevisions(String chargeableRevisions) {
		this.chargeableRevisions = chargeableRevisions;
	}

	public void setCod(boolean cod) {
		this.cod = cod;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setDateDue(Date dateDue) {
		this.dateDue = dateDue;
	}

	public void setDateSubmitted(Timestamp dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public void setDeliveryTicketNumber(String deliveryTicketNumber) {
		this.deliveryTicketNumber = deliveryTicketNumber;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDigitalColor(boolean digitalColor) {
		this.digitalColor = digitalColor;
	}

	public void setDrill(boolean drill) {
		this.drill = drill;
	}

	public void setDrillSize(String drillSize) {
		this.drillSize = drillSize;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public void setFilm(boolean film) {
		this.film = film;
	}

	public void setFloppy(boolean floppy) {
		this.floppy = floppy;
	}

	public void setFoldTo1(String foldTo1) {
		this.foldTo1 = foldTo1;
	}

	public void setFoldTo2(String foldTo2) {
		this.foldTo2 = foldTo2;
	}

	public void setFtp(boolean ftp) {
		this.ftp = ftp;
	}

	public void setHot(boolean hot) {
		this.hot = hot;
	}

	public void setIris(boolean iris) {
		this.iris = iris;
	}

	public void setJaz(boolean jaz) {
		this.jaz = jaz;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public void setMacpc(String macpc) {
		this.macpc = macpc;
	}

	public void setNewReprintRevised(String newReprintRevised) {
		this.newReprintRevised = newReprintRevised;
	}

	public void setNumberOfBoxes(String numberOfBoxes) {
		this.numberOfBoxes = numberOfBoxes;
	}

	public void setNumberOfSamples(String numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public void setPdf2Go(boolean pdf2Go) {
		this.pdf2Go = pdf2Go;
	}

	public void setPerf(boolean perf) {
		this.perf = perf;
	}

	public void setPocket(boolean pocket) {
		this.pocket = pocket;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public void setPrePressNotes(String prePressNotes) {
		this.prePressNotes = prePressNotes;
	}

	public void setPressCheck(int pressCheck) {
		this.pressCheck = pressCheck;
	}

	public void setPressCheckContactName(String pressCheckContactName) {
		this.pressCheckContactName = pressCheckContactName;
	}

	public void setPressCheckContactPhone(String pressCheckContactPhone) {
		this.pressCheckContactPhone = pressCheckContactPhone;
	}

	public void setPreviousJobNumber(String previousJobNumber) {
		this.previousJobNumber = previousJobNumber;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setProofOther(String proofOther) {
		this.proofOther = proofOther;
	}

	public void setProofTo(String proofTo) {
		this.proofTo = proofTo;
	}

	public void setProofToDate(Date proofToDate) {
		this.proofToDate = proofToDate;
	}

	public void setQuantityPerBox(String quantityPerBox) {
		this.quantityPerBox = quantityPerBox;
	}

	public void setQuantityShipped(String quantityShipped) {
		this.quantityShipped = quantityShipped;
	}

	public void setQuoted(int quoted) {
		this.quoted = quoted;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public void setSaddlestitch(boolean saddlestitch) {
		this.saddlestitch = saddlestitch;
	}

	public void setSalesrep(String salesrep) {
		this.salesrep = salesrep;
	}

	public void setSamplesTo(String samplesTo) {
		this.samplesTo = samplesTo;
	}

	public void setScore(boolean score) {
		this.score = score;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public void setShipVia(String shipVia) {
		this.shipVia = shipVia;
	}

	public void setShrinkIn(String shrinkIn) {
		this.shrinkIn = shrinkIn;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTrimTo1(String trimTo1) {
		this.trimTo1 = trimTo1;
	}

	public void setTrimTo2(String trimTo2) {
		this.trimTo2 = trimTo2;
	}

	public void setTypeOther(String typeOther) {
		this.typeOther = typeOther;
	}

	public void setWhoSubmitted(String whoSubmitted) {
		this.whoSubmitted = whoSubmitted;
	}

	public void setZip(boolean zip) {
		this.zip = zip;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public boolean isArchived() {
		return this.archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Timestamp getJeopardyTime() {
		return jeopardyTime;
	}

	public void setJeopardyTime(Timestamp jeopardyTime) {
		this.jeopardyTime = jeopardyTime;
	}

	public Date getOrderedDate() {
		return this.orderedDate;
	}

	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}

	public void setMonitor(boolean monitor) {
		// No longer used, but must keep for Marshaller
	}

	public boolean getMonitor() {
		return false;
	}
	//	public double retrieveStockTotal() {
	//		double total = new double(0).setScale(6, RoundingMode.HALF_UP);
	//		try {
	//			Vector<StockItem> items = getStockItem();
	//			for (int i = 0; i < items.size(); i++) {
	//				StockItem item = items.get(i);
	//				int noOfSheets = 0;
	//				double ppm = item.getPpm();
	//				try {
	//					noOfSheets = new Integer(item.getSheets()).intValue();
	//				} catch (Exception e) {
	//				}
	//				int pb = 0;
	//				try {
	//					pb = new Integer(item.getPb()).intValue();
	//				} catch (Exception e) {
	//					continue;
	//				}
	//				double lineTotal = ppm.multiply(new double(noOfSheets)).setScale(6, RoundingMode.HALF_UP);
	//				lineTotal = lineTotal.divide(new double(pb), RoundingMode.HALF_UP);
	//				total = total.add(lineTotal);
	//				lineTotal.toString();
	//				total.toString();
	//			}
	//			total = total.setScale(2, RoundingMode.HALF_UP);
	//		} catch (Exception e) {
	//			logger.error("", e);
	//			e.printStackTrace();
	//		}
	//		return total;
	//	}

	//	public int retrieveNoOfStockLines() {
	//		return stockItems == null ? 0 : stockItems.size();
	//	}
	public String getPriceComments() {
		return priceComments == null ? "" : priceComments;
	}

	public void setPriceComments(String priceComments) {
		this.priceComments = priceComments;
	}

	public int getDigitalOffset() {
		return digitalOffset;
	}

	public void setDigitalOffset(int digitalOffset) {
		this.digitalOffset = digitalOffset;
	}

	public void setLastStatus(String dummy) {
		// Stop the warning from ORM....last status isn't used anymore
	}

	//	public void removeEmpty() {
	//		for (int i = 0; i < jobItems.size(); i++) {
	//			JobItem item = (JobItem) jobItems.get(i);
	//			if (item.empty()) {
	//				jobItems.remove(i);
	//				i--;
	//			}
	//		}
	//		for (int i = 0; i < stockItems.size(); i++) {
	//			StockItem item = (StockItem) stockItems.get(i);
	//			if (item.empty()) {
	//				stockItems.remove(i);
	//				i--;
	//			}
	//		}
	//		for (int i = 0; i < pressItems.size(); i++) {
	//			PressItem item = (PressItem) pressItems.get(i);
	//			if (item.empty()) {
	//				pressItems.remove(i);
	//				i--;
	//			}
	//		}
	//		for (int i = 0; i < osItems.size(); i++) {
	//			OutsideServiceItem item = (OutsideServiceItem) osItems.get(i);
	//			if (item.empty()) {
	//				osItems.remove(i);
	//				i--;
	//			}
	//		}
	//	}
	public boolean isPrintToday() {
		return printToday;
	}

	public void setPrintToday(boolean printToday) {
		this.printToday = printToday;
	}

	public boolean isBackupFlag() {
		return backupFlag;
	}

	public void setBackupFlag(boolean backupFlag) {
		this.backupFlag = backupFlag;
	}

	public boolean isScheduled() {
		return scheduled;
	}

	public void setScheduled(boolean scheduled) {
		this.scheduled = scheduled;
	}

	public int getMailed() {
		return mailed;
	}

	public void setMailed(int mailed) {
		this.mailed = mailed;
	}

	public String getMailingVendor() {
		return mailingVendor == null ? "" : mailingVendor;
	}

	public void setMailingVendor(String mailingVendor) {
		this.mailingVendor = mailingVendor;
	}

	public boolean isLetterpress() {
		return letterpress;
	}

	public void setLetterpress(boolean letterpress) {
		this.letterpress = letterpress;
	}

	public boolean isQuoteGenerated() {
		return quoteGenerated;
	}

	public void setQuoteGenerated(boolean quoteGenerated) {
		this.quoteGenerated = quoteGenerated;
	}

	public int getQuoteQuantity() {
		return quoteQuantity;
	}

	public void setQuoteQuantity(int quoteQuantity) {
		this.quoteQuantity = quoteQuantity;
	}

	public boolean isInHouse() {
		return inHouse;
	}

	public void setInHouse(boolean inHouse) {
		this.inHouse = inHouse;
	}

	public Date getDropDate() {
		return dropDate;
	}

	public void setDropDate(Date dropDate) {
		this.dropDate = dropDate;
	}

	public boolean isStockPrint() {
		return stockPrint;
	}

	public void setStockPrint(boolean stockPrint) {
		this.stockPrint = stockPrint;
	}

	public String getWhoModified() {
		return whoModified;
	}

	public void setWhoModified(String whoModified) {
		this.whoModified = whoModified;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
