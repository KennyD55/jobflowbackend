package com.kendexter.jobflow.services.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class JobsDAO {
	private static Logger logger = Logger.getLogger(JobsDAO.class);

	private JobsDAO() {
	}

	private static final String SUMMARYFIELDS = "SELECT JOB_NUMBER, ACCOUNT_NUMBER, CUSTOMER_NAME, DATE_SUBMITTED, WHO_SUBMITTED, SALESREP, STATUS, PRICE, DESCRIPTION,JEOPARDY_TIME, DATE_DUE,"
			+ " HOT, DIGITAL_OFFSET, ARCHIVED, PRINT_TODAY, BACKUP_FLAG, MAILED, DROP_DATE, STOCK_PRINT, LAST_UPDATE";

	public static LinkedList<JobSummary> getJobList(Connection connection, String customer, boolean includeArchive, String jobNumber, String description)
			throws Exception {
		LinkedList<JobSummary> jobs = new LinkedList<JobSummary>();
		String desclike = description == null ? "%" : "%" + description.replaceAll("\\x2a", "%") + "%";
		if (jobNumber == null || jobNumber.trim().equals("")) {
			PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS
					+ " FROM JOBS"
					+ " WHERE (ARCHIVED = 0 OR ARCHIVED = ?)"
					+ " AND (CUSTOMER_NAME = ? OR ? = 'All')"
					+ " AND DESCRIPTION LIKE ?"
					+ " ORDER BY JOB_NUMBER DESC"
					+ " LIMIT 2001");
			int p = 0;
			select.setInt(++p, includeArchive ? 1 : 0);
			if (customer == null || customer.equals("All")) {
				select.setString(++p, "");
				select.setString(++p, "All");
			} else {
				select.setString(++p, customer);
				select.setString(++p, "");
			}
			select.setString(++p, desclike);
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				JobSummary job = loadJobSummaryFromResultSet(connection, rs);
				jobs.add(job);
			}
			rs.close();
			select.close();
		} else {
			PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS + " FROM JOBS" + " WHERE JOB_NUMBER = ?");
			select.setInt(1, Integer.parseInt(jobNumber));
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				JobSummary job = loadJobSummaryFromResultSet(connection, rs);
				jobs.add(job);
			}
			rs.close();
			select.close();
		}
		return jobs;
	}

	private static void loadJobNoteFlags(Connection connection, JobSummary job) throws Exception {
		ArrayList<JobNoteFlag> jobNoteFlags = new ArrayList<JobNoteFlag>();
		PreparedStatement select = connection.prepareStatement("SELECT DISTINCT A.NAME, A.COLOR"
				+ " FROM DEPARTMENTS A, JOB_NOTES_DEPARTMENTS B"
				+ " WHERE B.JOB_NUMBER = ? AND A.ID = B.DEPARTMENT_ID"
				+ " ORDER BY A.SEQ");
		select.setInt(1, job.getJobNumber());
		ResultSet rs = select.executeQuery();
		if (rs.next()) {
			jobNoteFlags.add(new JobNoteFlag("Notes", "#FFFFFF"));
			do {
				jobNoteFlags.add(new JobNoteFlag(rs.getString("NAME"), "#" + rs.getString("COLOR")));
			} while (rs.next());
			rs.close();
			select.close();
		} else {
			rs.close();
			select.close();
			select = connection.prepareStatement("SELECT COUNT(*) FROM JOB_NOTES WHERE JOB_NUMBER = ?");
			select.setInt(1, job.getJobNumber());
			rs = select.executeQuery();
			if (rs.next() && rs.getInt(1) > 0) {
				jobNoteFlags.add(new JobNoteFlag("Notes", "#FFFFFF"));
				rs.close();
				select.close();
			}
		}
		job.setJobNoteFlags(jobNoteFlags);
	}

	public static LinkedList<JobNote> getJobNotes(Connection connection, int jobNumber) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID, JOB_NUMBER, USER_NAME, TS, COMMENT FROM JOB_NOTES WHERE JOB_NUMBER = ?");
		select.setInt(1, jobNumber);
		ResultSet rs = select.executeQuery();
		LinkedList<JobNote> jobNotes = new LinkedList<JobNote>();
		while (rs.next())
			jobNotes.add(new JobNote(rs.getInt("ID"), rs.getInt("JOB_NUMBER"), rs.getString("USER_NAME"), rs.getTimestamp("TS"), rs.getString("COMMENT")));
		rs.close();
		select.close();
		return jobNotes;
	}

	public static Job getJob(Connection connection, String jobNumber) throws Exception {
		return getJob(connection, Integer.parseInt(jobNumber));
	}

	public static Job getJob(Connection connection, int jobNumber) throws Exception {
		PreparedStatement select = connection
				.prepareStatement("SELECT JOB_NUMBER, ACCOUNT_NUMBER, CUSTOMER_NAME, CUSTOMER_ADDRESS, DATE_SUBMITTED, WHO_SUBMITTED, SALESREP, STATUS, PRICE,"
						+ " DESCRIPTION, JEOPARDY_TIME, DATE_DUE, HOT, DIGITAL_OFFSET, ARCHIVED, PRINT_TODAY, BACKUP_FLAG, MAILED, DROP_DATE, STOCK_PRINT, BILLED_DATE,"
						+ " SHIPPED_DATE, DELIVERY_DATE, CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL, COD, QUOTED, QUOTE_NUMBER, NEW_REPRINT_REVISED,"
						+ " PREVIOUS_JOB_NUMBER, PO_NUMBER, PRESS_CHECK, PRESS_CHECK_CONTACT_NAME, PRESS_CHECK_CONTACT_PHONE, PDF_2_GO, BW_LASER, IRIS,"
						+ " DIGITAL_COLOR, PROOF_OTHER, MACPC, CD_ROM, FTP, FLOPPY, EMAIL, ZIP, JAZ, FILM, TYPE_OTHER, PROOF_TO, PROOF_TO_DATE, PRE_PRESS_NOTES,"
						+ " TRIM_TO_1, TRIM_TO_2, FOLD_TO_1, FOLD_TO_2, SHRINK_IN, SCORE, PERF, DRILL, DRILL_SIZE, SADDLESTITCH, POCKET, BINDERY_NOTES, SHIP_TO,"
						+ " SHIPPING_ADDRESS, SHIP_VIA, SHIPPING_NOTES, DELIVERY_TICKET_NUMBER, QUANTITY_SHIPPED, NUMBER_OF_BOXES, QUANTITY_PER_BOX,"
						+ " NUMBER_OF_SAMPLES, SAMPLES_TO, DELIVERY_NOTES, CHARGEABLE_REVISIONS, PRICE_COMMENTS, ORDERED_DATE, SCHEDULED, MAILING_VENDOR, IN_HOUSE,"
						+ " LETTERPRESS, QUOTE_GENERATED, WHO_MODIFIED, LAST_UPDATE"
						+ " FROM JOBS WHERE JOB_NUMBER = ?");
		select.setInt(1, jobNumber);
		ResultSet rs = select.executeQuery();
		if (!rs.next())
			return null;
		Job job = new Job();
		job.setJobNumber(rs.getInt("JOB_NUMBER"));
		job.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
		job.setCustomerName(rs.getString("CUSTOMER_NAME"));
		job.setCustomerAddress(rs.getString("CUSTOMER_ADDRESS"));
		job.setDateSubmitted(rs.getTimestamp("DATE_SUBMITTED"));
		job.setWhoSubmitted(rs.getString("WHO_SUBMITTED"));
		job.setSalesrep(rs.getString("SALESREP"));
		job.setStatus(rs.getString("STATUS"));
		job.setPrice(rs.getDouble("PRICE"));
		job.setDescription(rs.getString("DESCRIPTION"));
		job.setJeopardyTime(rs.getTimestamp("JEOPARDY_TIME"));
		job.setDateDue(rs.getDate("DATE_DUE"));
		job.setHot(rs.getInt("HOT") == 1);
		job.setDigitalOffset(rs.getInt("DIGITAL_OFFSET"));
		job.setArchived(rs.getInt("ARCHIVED") == 1);
		job.setPrintToday(rs.getInt("PRINT_TODAY") == 1);
		job.setBackupFlag(rs.getInt("BACKUP_FLAG") == 1);
		job.setMailed(rs.getInt("MAILED"));
		job.setDropDate(rs.getDate("DROP_DATE"));
		job.setStockPrint(rs.getInt("STOCK_PRINT") == 1);
		job.setBilledDate(rs.getDate("BILLED_DATE"));
		job.setShippedDate(rs.getDate("SHIPPED_DATE"));
		job.setDeliveryDate(rs.getDate("DELIVERY_DATE"));
		job.setContactName(rs.getString("CONTACT_NAME"));
		job.setContactPhone(rs.getString("CONTACT_PHONE"));
		job.setContactEmail(rs.getString("CONTACT_EMAIL"));
		job.setCod(rs.getInt("COD") == 1);
		job.setQuoted(rs.getInt("QUOTED"));
		job.setQuoteNumber(rs.getString("QUOTE_NUMBER"));
		job.setNewReprintRevised(rs.getString("NEW_REPRINT_REVISED"));
		job.setPreviousJobNumber(rs.getString("PREVIOUS_JOB_NUMBER"));
		job.setPoNumber(rs.getString("PO_NUMBER"));
		job.setPressCheck(rs.getInt("PRESS_CHECK"));
		job.setPressCheckContactName(rs.getString("PRESS_CHECK_CONTACT_NAME"));
		job.setPressCheckContactPhone(rs.getString("PRESS_CHECK_CONTACT_PHONE"));
		job.setPdf2Go(rs.getInt("PDF_2_GO") == 1);
		job.setBwLaser(rs.getInt("BW_LASER") == 1);
		job.setIris(rs.getInt("IRIS") == 1);
		job.setDigitalColor(rs.getInt("DIGITAL_COLOR") == 1);
		job.setProofOther(rs.getString("PROOF_OTHER"));
		job.setMacpc(rs.getString("MACPC"));
		job.setCdRom(rs.getInt("CD_ROM") == 1);
		job.setFtp(rs.getInt("FTP") == 1);
		job.setFloppy(rs.getInt("FLOPPY") == 1);
		job.setEmail(rs.getInt("EMAIL") == 1);
		job.setZip(rs.getInt("ZIP") == 1);
		job.setJaz(rs.getInt("JAZ") == 1);
		job.setFilm(rs.getInt("FILM") == 1);
		job.setTypeOther(rs.getString("TYPE_OTHER"));
		job.setProofTo(rs.getString("PROOF_TO"));
		job.setProofToDate(rs.getDate("PROOF_TO_DATE"));
		job.setPrePressNotes(rs.getString("PRE_PRESS_NOTES"));
		job.setTrimTo1(rs.getString("TRIM_TO_1"));
		job.setTrimTo2(rs.getString("TRIM_TO_2"));
		job.setFoldTo1(rs.getString("FOLD_TO_1"));
		job.setFoldTo2(rs.getString("FOLD_TO_2"));
		job.setShrinkIn(rs.getString("SHRINK_IN"));
		job.setScore(rs.getInt("SCORE") == 1);
		job.setPerf(rs.getInt("PERF") == 1);
		job.setDrill(rs.getInt("DRILL") == 1);
		job.setDrillSize(rs.getString("DRILL_SIZE"));
		job.setSaddlestitch(rs.getInt("SADDLESTITCH") == 1);
		job.setPocket(rs.getInt("POCKET") == 1);
		job.setBinderyNotes(rs.getString("BINDERY_NOTES"));
		job.setShipTo(rs.getString("SHIP_TO"));
		job.setShippingAddress(rs.getString("SHIPPING_ADDRESS"));
		job.setShipVia(rs.getString("SHIP_VIA"));
		job.setShippingNotes(rs.getString("SHIPPING_NOTES"));
		job.setDeliveryTicketNumber(rs.getString("DELIVERY_TICKET_NUMBER"));
		job.setQuantityShipped(rs.getString("QUANTITY_SHIPPED"));
		job.setNumberOfBoxes(rs.getString("NUMBER_OF_BOXES"));
		job.setQuantityPerBox(rs.getString("QUANTITY_PER_BOX"));
		job.setNumberOfSamples(rs.getString("NUMBER_OF_SAMPLES"));
		job.setSamplesTo(rs.getString("SAMPLES_TO"));
		job.setDeliveryNotes(rs.getString("DELIVERY_NOTES"));
		job.setChargeableRevisions(rs.getString("CHARGEABLE_REVISIONS"));
		job.setPriceComments(rs.getString("PRICE_COMMENTS"));
		job.setOrderedDate(rs.getDate("ORDERED_DATE"));
		job.setScheduled(rs.getInt("SCHEDULED") == 1);
		job.setMailingVendor(rs.getString("MAILING_VENDOR"));
		job.setInHouse(rs.getInt("IN_HOUSE") == 1);
		job.setLetterpress(rs.getInt("LETTERPRESS") == 1);
		job.setQuoteGenerated(rs.getInt("QUOTE_GENERATED") == 1);
		job.setWhoModified(rs.getString("WHO_MODIFIED"));
		job.setLastUpdate(rs.getTimestamp("LAST_UPDATE"));
		// 
		loadJobItems(connection, job);
		loadStockItems(connection, job);
		loadPressItems(connection, job);
		loadOSItems(connection, job);
		return job;
	}

	private static void loadOSItems(Connection connection, Job job) throws Exception {
		PreparedStatement select = connection.prepareStatement(
				"SELECT JOB_NUMBER, SEQ, VENDOR, ITEM, PO_NUMBER, REFERENCE, PRICE" + " FROM OUTSIDE_SERVICES" + " WHERE JOB_NUMBER = ?" + " ORDER BY SEQ");
		select.setInt(1, job.getJobNumber());
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			OSItem osItem = new OSItem();
			osItem.setJobNumber(rs.getInt("JOB_NUMBER"));
			osItem.setSeq(rs.getInt("SEQ"));
			osItem.setVendor(rs.getString("VENDOR"));
			osItem.setItem(rs.getString("ITEM"));
			osItem.setPoNumber(rs.getString("PO_NUMBER"));
			osItem.setReference(rs.getString("REFERENCE"));
			osItem.setPrice(rs.getString("PRICE"));
			job.addOsItem(osItem);
		}
		rs.close();
		select.close();
	}

	private static void loadPressItems(Connection connection, Job job) throws Exception {
		PreparedStatement select = connection.prepareStatement(
				"SELECT JOB_NUMBER, SEQ, FORM_NUMBER, PRESS, FORM, PRESS_COLOR, COLOR_OF_INK, BASIC_NO_OF_SHEETS, OVERS, TOTAL, FINAL_COUNT, INITIALS, RUN_TIME"
						+ " FROM PRESS_ITEMS"
						+ " WHERE JOB_NUMBER = ?"
						+ " ORDER BY SEQ");
		select.setInt(1, job.getJobNumber());
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			PressItem pressItem = new PressItem();
			pressItem.setJobNumber(rs.getInt("JOB_NUMBER"));
			pressItem.setSeq(rs.getInt("SEQ"));
			pressItem.setFormNumber(rs.getString("FORM_NUMBER"));
			pressItem.setPress(rs.getString("PRESS"));
			pressItem.setForm(rs.getString("FORM"));
			pressItem.setPressColor(rs.getString("PRESS_COLOR"));
			pressItem.setColorOfInk(rs.getString("COLOR_OF_INK"));
			pressItem.setBasicNoOfSheets(rs.getString("BASIC_NO_OF_SHEETS"));
			pressItem.setOvers(rs.getString("OVERS"));
			pressItem.setTotal(rs.getString("TOTAL"));
			pressItem.setFinalCount(rs.getString("FINAL_COUNT"));
			pressItem.setInitials(rs.getString("INITIALS"));
			pressItem.setRunTime(rs.getString("RUN_TIME"));
			job.addPressItem(pressItem);
		}
		rs.close();
		select.close();
	}

	public static LinkedList<JobSummary> getJobList(Connection connection, String userName, String customer, boolean includeArchive, String jobNumber,
			String description) throws Exception {
		LinkedList<JobSummary> jobs = new LinkedList<JobSummary>();

		String desclike = description == null ? "%" : "%" + description.replaceAll("\\x2a", "%") + "%";

		if (jobNumber == null || jobNumber.equals("")) {
			PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS
					+ " FROM JOBS"
					+ " WHERE SALESREP = ?"
					+ " AND  (ARCHIVED = 0 OR ARCHIVED = ?)"
					+ " AND (CUSTOMER_NAME = ? OR ? = 'All')"
					+ " AND DESCRIPTION LIKE ?"
					+ " ORDER BY JOB_NUMBER DESC");
			int p = 0;
			select.setString(++p, userName);
			select.setInt(++p, includeArchive ? 1 : 0);
			if (customer == null || customer.equals("All")) {
				select.setString(++p, "");
				select.setString(++p, "All");
			} else {
				select.setString(++p, customer);
				select.setString(++p, desclike);
			}
			select.setString(++p, desclike);
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				JobSummary job = loadJobSummaryFromResultSet(connection, rs);
				jobs.add(job);
			}
			rs.close();
			select.close();
		} else {
			PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS + " FROM JOBS" + "WHERE SALESREP = ? AND JOB_NUMBER = ?");
			select.setString(1, userName);
			select.setInt(2, Integer.parseInt(jobNumber));
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				JobSummary job = loadJobSummaryFromResultSet(connection, rs);
				jobs.add(job);
			}
			rs.close();
			select.close();
		}
		return jobs;
	}

	public static JobSummary getJobSummary(Connection connection, String jobNumber) throws Exception {
		return getJobSummary(connection, Integer.parseInt(jobNumber));
	}

	public static JobSummary getJobSummary(Connection connection, int jobNumber) throws Exception {
		PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS + " FROM JOBS" + " WHERE JOB_NUMBER = ?");
		select.setInt(1, jobNumber);
		ResultSet rs = select.executeQuery();
		if (!rs.next())
			return null;
		JobSummary job = loadJobSummaryFromResultSet(connection, rs);
		rs.close();
		select.close();
		return job;
	}

	private static void loadJobItems(Connection connection, Job job) throws Exception {
		PreparedStatement select = connection
				.prepareStatement("SELECT JOB_NUMBER, SEQ, QUANTITY, DESCRIPTION, FLAT_SIZE, FINAL_SIZE FROM JOB_ITEMS" + " WHERE JOB_NUMBER = ?" + " ORDER BY SEQ");
		select.setInt(1, job.getJobNumber());
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			JobItem jobItem = new JobItem();
			jobItem.setJobNumber(rs.getInt("JOB_NUMBER"));
			jobItem.setSeq(rs.getInt("SEQ"));
			jobItem.setQuantity(rs.getString("QUANTITY"));
			jobItem.setDescription(rs.getString("DESCRIPTION"));
			jobItem.setFlatSize(rs.getString("FLAT_SIZE"));
			jobItem.setFinalSize(rs.getString("FINAL_SIZE"));
			job.addJobItem(jobItem);
		}
		rs.close();
		select.close();
	}

	private static void loadStockItems(Connection connection, Job job) throws Exception {
		PreparedStatement select = connection.prepareStatement(
				"SELECT JOB_NUMBER, SEQ, SHEETS, SIZE, WEIGHT, GRADE_COLOR, COVER_TEXT, ON_HAND_OR_ORDER_FROM, PRESS_SHEET_SIZE, PS_OUT, NUMBER_UP, PPM, PB, LOT"
						+ " FROM STOCK_ITEMS"
						+ " WHERE JOB_NUMBER = ?"
						+ " ORDER BY SEQ");
		select.setInt(1, job.getJobNumber());
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			StockItem stockItem = new StockItem();
			stockItem.setJobNumber(rs.getInt("JOB_NUMBER"));
			stockItem.setSeq(rs.getInt("SEQ"));
			stockItem.setSheets(rs.getString("SHEETS"));
			stockItem.setSize(rs.getString("SIZE"));
			stockItem.setWeight(rs.getString("WEIGHT"));
			stockItem.setGradeColor(rs.getString("GRADE_COLOR"));
			stockItem.setCoverText(rs.getString("COVER_TEXT"));
			stockItem.setOnHandOrOrderFrom(rs.getString("ON_HAND_OR_ORDER_FROM"));
			stockItem.setPressSheetSize(rs.getString("PRESS_SHEET_SIZE"));
			stockItem.setPsOut(rs.getString("PS_OUT"));
			stockItem.setNumberUp(rs.getString("NUMBER_UP"));
			stockItem.setPpm(rs.getString("PPM"));
			stockItem.setPb(rs.getString("PB"));
			stockItem.setLot(rs.getInt("LOT") == 1);
			job.addStockItem(stockItem);
		}
		rs.close();
		select.close();
	}

	private static JobSummary loadJobSummaryFromResultSet(Connection connection, ResultSet rs) throws Exception {
		JobSummary job = new JobSummary();
		job.setJobNumber(rs.getInt("JOB_NUMBER"));
		job.setCustomerName(rs.getString("CUSTOMER_NAME"));
		job.setDateSubmitted(rs.getTimestamp("DATE_SUBMITTED"));
		job.setWhoSubmitted(rs.getString("WHO_SUBMITTED"));
		job.setSalesrep(rs.getString("SALESREP"));
		job.setStatus(rs.getString("STATUS"));
		job.setPrice(rs.getDouble("PRICE"));
		job.setDescription(rs.getString("DESCRIPTION"));
		job.setJeopardyTime(rs.getTimestamp("JEOPARDY_TIME"));
		job.setDateDue(rs.getDate("DATE_DUE"));
		job.setHot(rs.getInt("HOT") == 1);
		job.setDigitalOffset(rs.getInt("DIGITAL_OFFSET"));
		job.setArchived(rs.getInt("ARCHIVED") == 1);
		job.setPrintToday(rs.getInt("PRINT_TODAY") == 1);
		job.setBackupFlag(rs.getInt("BACKUP_FLAG") == 1);
		job.setMailed(rs.getInt("MAILED"));
		job.setDropDate(rs.getDate("DROP_DATE"));
		job.setStockPrint(rs.getInt("STOCK_PRINT") == 1);
		job.setLastUpdate(rs.getTimestamp("LAST_UPDATE"));

		loadJobNoteFlags(connection, job);

		return job;
	}

	public static synchronized int upsertJob(Connection connection, Job job, String userName) throws Exception {
		PreparedStatement insert = connection
				.prepareStatement("REPLACE INTO JOBS (JOB_NUMBER, ACCOUNT_NUMBER, CUSTOMER_NAME, DATE_SUBMITTED, WHO_SUBMITTED, SALESREP, STATUS, PRICE,"
						+ " DESCRIPTION, JEOPARDY_TIME, DATE_DUE, HOT, DIGITAL_OFFSET, ARCHIVED, PRINT_TODAY, BACKUP_FLAG, MAILED, DROP_DATE, STOCK_PRINT, BILLED_DATE, SHIPPED_DATE,"
						+ " DELIVERY_DATE, CUSTOMER_ADDRESS, CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL, COD, QUOTED, QUOTE_NUMBER, NEW_REPRINT_REVISED,"
						+ " PREVIOUS_JOB_NUMBER, PO_NUMBER, PRESS_CHECK, PRESS_CHECK_CONTACT_NAME, PRESS_CHECK_CONTACT_PHONE, PDF_2_GO, BW_LASER, IRIS, DIGITAL_COLOR, PROOF_OTHER,"
						+ " MACPC, CD_ROM, FTP, FLOPPY, EMAIL, ZIP, JAZ, FILM, TYPE_OTHER, PROOF_TO, PROOF_TO_DATE, PRE_PRESS_NOTES, TRIM_TO_1, TRIM_TO_2, FOLD_TO_1, FOLD_TO_2,"
						+ " SHRINK_IN, SCORE, PERF, DRILL, DRILL_SIZE, SADDLESTITCH, POCKET, BINDERY_NOTES, SHIP_TO, SHIPPING_ADDRESS, SHIP_VIA, SHIPPING_NOTES,"
						+ " DELIVERY_TICKET_NUMBER, QUANTITY_SHIPPED, NUMBER_OF_BOXES, QUANTITY_PER_BOX, NUMBER_OF_SAMPLES, SAMPLES_TO, DELIVERY_NOTES, CHARGEABLE_REVISIONS,"
						+ " PRICE_COMMENTS, ORDERED_DATE, SCHEDULED, MAILING_VENDOR, IN_HOUSE, LETTERPRESS, QUOTE_GENERATED, WHO_MODIFIED)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		int p = 0;
		insert.setInt(++p, job.getJobNumber());
		insert.setString(++p, job.getAccountNumber());
		insert.setString(++p, job.getCustomerName());
		insert.setTimestamp(++p, job.getDateSubmitted());
		insert.setString(++p, job.getWhoSubmitted());
		insert.setString(++p, job.getSalesrep());
		insert.setString(++p, job.getStatus());
		insert.setDouble(++p, job.getPrice());
		insert.setString(++p, job.getDescription());
		insert.setTimestamp(++p, job.getJeopardyTime());
		insert.setDate(++p, job.getDateDue());
		insert.setInt(++p, job.isHot() ? 1 : 0);
		insert.setInt(++p, job.getDigitalOffset());
		insert.setInt(++p, job.isArchived() ? 1 : 0);
		insert.setInt(++p, job.isPrintToday() ? 1 : 0);
		insert.setInt(++p, job.isBackupFlag() ? 1 : 0);
		insert.setInt(++p, job.getMailed());
		insert.setDate(++p, job.getDropDate());
		insert.setInt(++p, job.isStockPrint() ? 1 : 0);
		insert.setDate(++p, job.getBilledDate());
		insert.setDate(++p, job.getShippedDate());
		insert.setDate(++p, job.getDeliveryDate());
		insert.setString(++p, job.getCustomerAddress());
		insert.setString(++p, job.getContactName());
		insert.setString(++p, job.getContactPhone());
		insert.setString(++p, job.getContactEmail());
		insert.setInt(++p, job.isCod() ? 1 : 0);
		insert.setInt(++p, job.getQuoted());
		insert.setString(++p, job.getQuoteNumber());
		insert.setString(++p, job.getNewReprintRevised());
		insert.setString(++p, job.getPreviousJobNumber());
		insert.setString(++p, job.getPoNumber());
		insert.setInt(++p, job.getPressCheck());
		insert.setString(++p, job.getPressCheckContactName());
		insert.setString(++p, job.getPressCheckContactPhone());
		insert.setInt(++p, job.isPdf2Go() ? 1 : 0);
		insert.setInt(++p, job.isBwLaser() ? 1 : 0);
		insert.setInt(++p, job.isIris() ? 1 : 0);
		insert.setInt(++p, job.isDigitalColor() ? 1 : 0);
		insert.setString(++p, job.getProofOther());
		insert.setString(++p, job.getMacpc());
		insert.setInt(++p, job.isCdRom() ? 1 : 0);
		insert.setInt(++p, job.isFtp() ? 1 : 0);
		insert.setInt(++p, job.isFloppy() ? 1 : 0);
		insert.setInt(++p, job.isEmail() ? 1 : 0);
		insert.setInt(++p, job.isZip() ? 1 : 0);
		insert.setInt(++p, job.isJaz() ? 1 : 0);
		insert.setInt(++p, job.isFilm() ? 1 : 0);
		insert.setString(++p, job.getTypeOther());
		insert.setString(++p, job.getProofTo());
		insert.setDate(++p, job.getProofToDate());
		insert.setString(++p, job.getPrePressNotes());
		insert.setString(++p, job.getTrimTo1());
		insert.setString(++p, job.getTrimTo2());
		insert.setString(++p, job.getFoldTo1());
		insert.setString(++p, job.getFoldTo2());
		insert.setString(++p, job.getShrinkIn());
		insert.setInt(++p, job.isScore() ? 1 : 0);
		insert.setInt(++p, job.isPerf() ? 1 : 0);
		insert.setInt(++p, job.isDrill() ? 1 : 0);
		insert.setString(++p, job.getDrillSize());
		insert.setInt(++p, job.isSaddlestitch() ? 1 : 0);
		insert.setInt(++p, job.isPocket() ? 1 : 0);
		insert.setString(++p, job.getBinderyNotes());
		insert.setString(++p, job.getShipTo());
		insert.setString(++p, job.getShippingAddress());
		insert.setString(++p, job.getShipVia());
		insert.setString(++p, job.getShippingNotes());
		insert.setString(++p, job.getDeliveryTicketNumber());
		insert.setString(++p, job.getQuantityShipped());
		insert.setString(++p, job.getNumberOfBoxes());
		insert.setString(++p, job.getQuantityPerBox());
		insert.setString(++p, job.getNumberOfSamples());
		insert.setString(++p, job.getSamplesTo());
		insert.setString(++p, job.getDeliveryNotes());
		insert.setString(++p, job.getChargeableRevisions());
		insert.setString(++p, job.getPriceComments());
		insert.setDate(++p, job.getOrderedDate());
		insert.setInt(++p, job.isScheduled() ? 1 : 0);
		insert.setString(++p, job.getMailingVendor());
		insert.setInt(++p, job.isInHouse() ? 1 : 0);
		insert.setInt(++p, job.isLetterpress() ? 1 : 0);
		insert.setInt(++p, job.isQuoteGenerated() ? 1 : 0);
		insert.setString(++p, userName);
		int count = insert.executeUpdate();
		upsertJobItems(connection, job);
		upsertOSItems(connection, job);
		upsertPressItems(connection, job);
		upsertStockItems(connection, job);
		insert.close();
		return count;
	}

	private static synchronized int upsertJobItems(Connection connection, Job job) throws Exception {
		PreparedStatement delete = connection.prepareStatement("DELETE FROM JOB_ITEMS WHERE JOB_NUMBER = ?");
		delete.setInt(1, job.getJobNumber());
		delete.executeUpdate();
		PreparedStatement insert = connection
				.prepareStatement("INSERT INTO JOB_ITEMS (JOB_NUMBER, SEQ, QUANTITY, DESCRIPTION, FLAT_SIZE, FINAL_SIZE) " + "VALUES (?, ?, ?, ?, ?, ?)");
		int seq = 0;
		for (JobItem jobItem : job.getJobItems()) {
			int p = 0;
			if (jobItem.empty())
				continue;
			insert.setInt(++p, job.getJobNumber());
			insert.setInt(++p, seq++);
			insert.setString(++p, jobItem.getQuantity());
			insert.setString(++p, jobItem.getDescription());
			insert.setString(++p, jobItem.getFlatSize());
			insert.setString(++p, jobItem.getFinalSize());
			insert.executeUpdate();
		}
		delete.close();
		insert.close();
		return seq;
	}

	private static synchronized int upsertOSItems(Connection connection, Job job) throws Exception {
		PreparedStatement delete = null;
		PreparedStatement insert = null;
		try {
			delete = connection.prepareStatement("DELETE FROM OUTSIDE_SERVICES WHERE JOB_NUMBER = ?");
			delete.setInt(1, job.getJobNumber());
			delete.executeUpdate();
			insert = connection
					.prepareStatement("INSERT INTO OUTSIDE_SERVICES (JOB_NUMBER, SEQ, VENDOR, ITEM, PO_NUMBER, REFERENCE, PRICE)" + "VALUES (?, ?, ?, ?, ?, ?, ?)");
			int seq = 0;
			for (OSItem osItem : job.getOsItems()) {
				int p = 0;
				if (osItem.empty())
					continue;
				insert.setInt(++p, job.getJobNumber());
				insert.setInt(++p, seq++);
				insert.setString(++p, osItem.getVendor());
				insert.setString(++p, osItem.getItem());
				insert.setString(++p, osItem.getPoNumber());
				insert.setString(++p, osItem.getReference());
				insert.setString(++p, osItem.getPrice());
				insert.executeUpdate();
			}
			return seq;
		} catch (Exception e) {
			logger.error("Error upserting to OUTSIDE_SERVICES table");
			throw e;
		} finally {
			try {
				delete.close();
				insert.close();
			} catch (Exception e) {
			}
		}
	};

	private static synchronized int upsertPressItems(Connection connection, Job job) throws Exception {

		PreparedStatement delete = connection.prepareStatement("DELETE FROM PRESS_ITEMS WHERE JOB_NUMBER = ?");
		delete.setInt(1, job.getJobNumber());
		delete.executeUpdate();
		PreparedStatement insert = connection.prepareStatement(
				"INSERT INTO PRESS_ITEMS (JOB_NUMBER, SEQ, FORM_NUMBER, PRESS, FORM, PRESS_COLOR, COLOR_OF_INK, BASIC_NO_OF_SHEETS, OVERS, TOTAL, FINAL_COUNT, INITIALS, RUN_TIME)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		int seq = 0;
		for (PressItem pressItem : job.getPressItems()) {
			int p = 0;
			if (pressItem.empty())
				continue;
			insert.setInt(++p, job.getJobNumber());
			insert.setInt(++p, seq++);
			insert.setString(++p, pressItem.getFormNumber());
			insert.setString(++p, pressItem.getPress());
			insert.setString(++p, pressItem.getForm());
			insert.setString(++p, pressItem.getPressColor());
			insert.setString(++p, pressItem.getColorOfInk());
			insert.setString(++p, pressItem.getBasicNoOfSheets());
			insert.setString(++p, pressItem.getOvers());
			insert.setString(++p, pressItem.getTotal());
			insert.setString(++p, pressItem.getFinalCount());
			insert.setString(++p, pressItem.getInitials());
			insert.setString(++p, pressItem.getRunTime());
			insert.executeUpdate();
		}
		delete.close();
		insert.close();
		return seq;
	}

	private static synchronized int upsertStockItems(Connection connection, Job job) throws Exception {

		PreparedStatement delete = connection.prepareStatement("DELETE FROM STOCK_ITEMS WHERE JOB_NUMBER = ?");
		delete.setInt(1, job.getJobNumber());
		delete.executeUpdate();
		PreparedStatement insert = connection.prepareStatement(
				"INSERT INTO STOCK_ITEMS (JOB_NUMBER, SEQ, SHEETS, SIZE, WEIGHT, GRADE_COLOR, COVER_TEXT, ON_HAND_OR_ORDER_FROM, PRESS_SHEET_SIZE, PS_OUT, NUMBER_UP, PPM, PB, LOT)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		int seq = 0;
		for (StockItem stockItem : job.getStockItems()) {
			int p = 0;
			if (stockItem.empty())
				continue;
			insert.setInt(++p, job.getJobNumber());
			insert.setInt(++p, seq++);
			insert.setString(++p, stockItem.getSheets());
			insert.setString(++p, stockItem.getSize());
			insert.setString(++p, stockItem.getWeight());
			insert.setString(++p, stockItem.getGradeColor());
			insert.setString(++p, stockItem.getCoverText());
			insert.setString(++p, stockItem.getOnHandOrOrderFrom());
			insert.setString(++p, stockItem.getPressSheetSize());
			insert.setString(++p, stockItem.getPsOut());
			insert.setString(++p, stockItem.getNumberUp());
			insert.setString(++p, stockItem.getPpm());
			insert.setString(++p, stockItem.getPb());
			insert.setInt(++p, stockItem.isLot() ? 1 : 0);
			insert.executeUpdate();
		}
		delete.close();
		insert.close();
		return seq;
	}

	public static LinkedList<JobHistory> getJobHistory(Connection connection, int jobNumber) throws Exception {
		LinkedList<JobHistory> histories = new LinkedList<JobHistory>();
		PreparedStatement select = connection
				.prepareStatement("SELECT JOB_NUMBER, ID, NAME, STATUS, COMMENT, TS FROM JOB_HISTORY WHERE JOB_NUMBER = ? ORDER BY ID");
		select.setInt(1, jobNumber);
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			JobHistory history = new JobHistory();
			history.setJobNumber(rs.getInt("JOB_NUMBER"));
			history.setId(rs.getInt("ID"));
			history.setName(rs.getString("NAME"));
			history.setStatus(rs.getString("STATUS"));
			history.setComment(rs.getString("COMMENT"));
			history.setTs(rs.getTimestamp("TS"));
			histories.add(history);
		}
		rs.close();
		select.close();
		return histories;
	}

	public static synchronized void insertJobHistory(Connection connection, int jobNumber, String comment, String status,  String userName) throws Exception {
		PreparedStatement insert = connection.prepareStatement("INSERT INTO JOB_HISTORY (JOB_NUMBER, NAME, STATUS, COMMENT) VALUES (?, ?, ?, ?)");
		insert.setInt(1, jobNumber);
		insert.setString(2, userName);
		insert.setString(3, status);
		insert.setString(4, comment);
		insert.executeUpdate();
		insert.close();
	}

	public static Collection<Job> getJobsReadyForBackup(Connection connection) throws Exception {
		LinkedList<Job> jobsReadyForBackup = new LinkedList<Job>();
		PreparedStatement select = connection.prepareStatement("SELECT A.JOB_NUMBER, A.DIGITAL_OFFSET, A.CUSTOMER_NAME, A.DESCRIPTION, A.DATE_DUE, A.STATUS"
				+ " FROM JOBS A, JOB_STATUSES B"
				+ " WHERE A.STATUS = B.STATUS AND B.READY_FOR_BACKUP = 1 AND A.BACKUP_FLAG = 1 ORDER BY JOB_NUMBER");
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			Job job = new Job();
			job.setJobNumber(rs.getInt("JOB_NUMBER"));
			job.setDigitalOffset(rs.getInt("DIGITAL_OFFSET"));
			job.setCustomerName(rs.getString("CUSTOMER_NAME"));
			job.setDescription(rs.getString("DESCRIPTION"));
			job.setDateDue(rs.getDate("DATE_DUE"));
			job.setStatus(rs.getString("STATUS"));
			jobsReadyForBackup.add(job);
		}
		return jobsReadyForBackup;
	}

	public static Collection<JobSummary> getJobsByStatus(Connection connection, String jobStatus) throws Exception {
		LinkedList<JobSummary> jobs = new LinkedList<JobSummary>();
		PreparedStatement select = connection.prepareStatement(SUMMARYFIELDS + " FROM JOBS WHERE STATUS = ? AND ARCHIVED = 0 ORDER BY JOB_NUMBER");
		select.setString(1, jobStatus);
		ResultSet rs = select.executeQuery();
		while (rs.next()) {
			JobSummary job = loadJobSummaryFromResultSet(connection, rs);
			jobs.add(job);
		}
		return jobs;
	}
}
