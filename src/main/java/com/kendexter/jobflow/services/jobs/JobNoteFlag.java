package com.kendexter.jobflow.services.jobs;

public class JobNoteFlag {
	private String department;
	private String color;

	public JobNoteFlag() {

	}

	public JobNoteFlag(String department, String color) {
		super();
		this.department = department;
		this.color = color;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
