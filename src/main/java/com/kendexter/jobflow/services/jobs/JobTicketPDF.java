package com.kendexter.jobflow.services.jobs;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.customers.Customer;
import com.kendexter.jobflow.services.customers.CustomersDAO;
import com.kendexter.jobflow.services.users.User;
import com.kendexter.pdf.PDFBoxServlet;

@SuppressWarnings("serial")
public class JobTicketPDF extends PDFBoxServlet {
	public static final ThreadLocal<SimpleDateFormat> SDF = new ThreadLocal<SimpleDateFormat>() {
		public SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};
	public static final ThreadLocal<DecimalFormat> DF = new ThreadLocal<DecimalFormat>() {
		public DecimalFormat initialValue() {
			return new DecimalFormat("$#,###,###.00");
		}
	};
	private static final float binderyY = 712f;
	private static final float shippingY = 815f;
	private static final int descriptionLinesCount = 8;
	private static final int stockLinesCount = 8;
	private static final int pressLinesCount = 8;
	private static final int osLinesCount = 4;

	public JobTicketPDF(HttpServletRequest request) {
		super(request);
	}

	protected void process(User user) throws Exception {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Job job = JobsDAO.getJob(connection, request.getParameter("jobNumber"));
			Customer customer = CustomersDAO.getCustomer(connection, job.getCustomerName());
			PDDocument document = (PDDocument) request.getAttribute("document");
			document.getDocumentInformation().setTitle("Job " + job.getJobNumber());
			// Add mailing vendor if present
			//		if (job.getMailed() == Job.MAILREQUIRED)
			//			job.addMailingOS(job.getMailingVendor());
			// Create a document and set it's properties
			pageOne(job, 180f, request, customer);
			if (job.getJobItems().size() > descriptionLinesCount
					|| job.getStockItems().size() > stockLinesCount - 1
					|| job.getPressItems().size() > pressLinesCount
					|| job.getOsItems().size() > osLinesCount)
				pageTwo(job);
			pageContentStream.close();
			//		if (job.getJobItem().size() > 19
			//				|| descriptionLinesCount < job.getStockItem().size()
			//				||pressLinesCount < job.getPressItem().size()
			//				|| osLinesCount < job.getOutsideServiceItem().size()) {
			//			addPage(new PDRectangle(720f, 936f));
			//			pageTwo(job, format, request, connection);
			//			pageContentStream.close();
			//		}
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	private void pageOne(Job job, float yc, HttpServletRequest request, Customer customer) throws IOException {
		addPage(new PDRectangle(720f, 936f));
		line1(job);
		line2(job);
		line3(job);
		line4(job);
		line5(job);
		line6(job);
		line7(job);
		pressCheckBox(job);
		descriptionTable(job, yc);
		//
		float yc1 = yc + 17 + (12 * descriptionLinesCount);
		block(25, yc - 2, 50, yc1 + 1, "DESCRIPTION");
		yc = yc1;
		prePressTable(job, yc);
		yc1 = yc + 132;
		block(25, yc - 2, 50, yc1 + 1, "PRE-PRESS");
		yc = yc1;
		stockTable(job, yc);
		yc1 = yc + 16 + (12 * stockLinesCount);
		block(25, yc - 2, 50, yc1 + 1, "STOCK");
		yc = yc1;
		pressTable(job, yc);
		yc1 = yc + 16 + (12 * pressLinesCount);
		block(25, yc - 2, 50, yc1 + 1, "PRESSROOM");
		yc = yc1;
		outsideServicesTable(job, yc);
		yc1 = yc + 16 + (12 * osLinesCount);
		block(25, yc - 2, 50, yc1 + 1, "OS/MISC");
		binderyTable(job, binderyY);
		block(25, binderyY - 1, 50, binderyY + 100, "BINDERY");
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(25, pageHeight - shippingY, 636, 18);
		addText("DELIVERY", PDType1Font.TIMES_BOLD_ITALIC, 12f, 150, shippingY - 5, Color.WHITE);
		addText("SHIPPING", PDType1Font.TIMES_BOLD_ITALIC, 12f, 470, shippingY - 5, Color.WHITE);
		deliveryTable(job, shippingY);
		shippingTable(job, shippingY);
		if (customer.isMonitored()) {
			addText("CREDIT", PDType1Font.TIMES_BOLD_ITALIC, 10, 50, shippingY + 110);
		}
	}

	protected void pageTwo(Job job) throws IOException {
		addPage(new PDRectangle(612f, 792f));
		float y = 50f;
		if (job.getJobItems().size() > descriptionLinesCount) {
			page2DescriptionTable(job, y);
			y += 40 + (12 * (job.getJobItems().size() - descriptionLinesCount));
		}
		if (job.getStockItems().size() > stockLinesCount - 2) {
			page2StockTable(job, y);
			y += 40 + (12 * (job.getStockItems().size() - (stockLinesCount - 1)));
		}
		if (job.getPressItems().size() > pressLinesCount) {
			page2PressTable(job, y);
			y += 40 + (12 * (job.getPressItems().size() - pressLinesCount));
		}
		if (job.getOsItems().size() > osLinesCount) {
			page2OutsideServicesTable(job, y);
		}
	}

	protected void line1(Job job) throws IOException {
		int yc = 20;
		float ycf = yc + 1;
		gen12("JOB NUMBER", 24, yc);
		if (job.getDigitalOffset() == Job.DIGITAL) {
			addText("D", PDType1Font.TIMES_BOLD, 12f, 106f, yc);
			gen12Bold(job.getJobNumber() + "", 116f, yc);
		} else
			gen12Bold(job.getJobNumber() + "", 106, yc);
		addLine(1, 106f, ycf, 182f, ycf);
		gen12("ENTER DATE", 190, yc);
		addLine(1, 266f, ycf, 346f, ycf);
		gen12Bold(job.getDateSubmitted().toString().substring(0, 10), 266, yc);
		gen12("DEL DATE", 354, yc);
		addLine(1, 418f, ycf, 496f, ycf);
		if (job.getDateDue() != null)
			gen12Bold(SDF.get().format(job.getDateDue()), 418, yc);
		gen12("COD", 508, yc);
		addLine(1, 544f, ycf, 662f, ycf);
		if (job.isCod())
			gen12Bold("YES", 544, yc);
		else
			gen12Bold("NO", 544, yc);
	}

	protected void line2(Job job) throws IOException {
		int yc = 44;
		float ycf = yc + 1;
		gen12("CUSTOMER", 24, yc);
		gen12Bold(job.getCustomerName().trim(), 106, yc);
		addLine(1, 106f, ycf, 396f, ycf);
		gen12("ACCT:", 400, yc);
		//TODO need account number on job (DB, Bean, DAO).
		//		gen12Bold(job.getAccountNumber().trim(), 445, yc);
		addLine(1, 445f, ycf, 490f, ycf);
		gen12("SHIPPED", 508, yc);
		addLine(1, 562f, ycf, 662f, ycf);
		if (job.getShippedDate() != null)
			gen12Bold(SDF.get().format(job.getShippedDate()), 562, yc);
	}

	protected void line3(Job job) throws IOException {
		int yc = 68;
		float ycf = yc + 1;
		gen12("ADDRESS", 24, yc);
		gen12Bold(job.getCustomerAddress().trim().replaceAll("&nbsp;", " "), 106, yc);
		addLine(1, 106f, ycf, 496f, ycf);
		gen12("BILLED", 508, yc);
		addLine(1, 562f, ycf, 662f, ycf);
		if (job.getBilledDate() != null)
			gen12Bold(SDF.get().format(job.getBilledDate()), 562, yc);
	}

	protected void line4(Job job) throws IOException {
		int yc = 92;
		float ycf = yc + 1f;
		gen12("CONTACT", 24, yc);
		addLine(1, 106f, ycf, 254f, ycf);
		gen12Bold(job.getContactName().trim(), 106, yc);
		gen12("PHONE", 266, yc);
		addLine(1, 314f, ycf, 496f, ycf);
		gen12Bold(job.getContactPhone().trim(), 314, yc);
	}

	protected void line5(Job job) throws IOException {
		int yc = 116;
		float ycf = yc + 1f;
		gen12("SALES REP", 24, yc);
		addLine(1, 106f, ycf, 496f, ycf);
		gen12Bold(job.getSalesrep().trim(), 106, yc);
		addLine(1, 106f, ycf, 188f, ycf);
	}

	protected void line6(Job job) throws IOException {
		int yc = 140;
		float ycf = yc + 1f;
		gen12("QUOTED", 24, yc);
		gen12("YES", 96, yc);
		gen12("NO", 148, yc);
		checkBox(84, yc + 3, job.getQuoted() == Job.QUOTEDTRUE ? true : false);
		checkBox(132, yc + 3, job.getQuoted() == Job.QUOTEDFALSE ? true : false);
		gen12("QUOTE NUMBER:", 183, yc);
		addLine(1, 285f, ycf, 365f, ycf);
		gen12Bold(job.getQuoteNumber(), 285, yc);
	}

	protected void line7(Job job) throws IOException {
		int yc = 164;
		float ycf = yc + 1;
		checkBox(24, yc + 3, Job.NEW.equals(job.getNewReprintRevised()) ? true : false);
		gen12("NEW", 40, yc);
		checkBox(74, yc + 3, Job.REPRINT.equals(job.getNewReprintRevised()) ? true : false);
		gen12("REPRINT", 90, yc);
		checkBox(150, yc + 3, Job.REVISED.equals(job.getNewReprintRevised()) ? true : false);
		gen12("REVISED", 166, yc);
		gen12("PREV. JOB #", 220, yc);
		addLine(1, 300f, ycf, 385f, ycf);
		gen12Bold(job.getPreviousJobNumber(), 300, yc);
		gen12("P.O.#", 387, yc);
		addLine(1, 411f, ycf, 661f, ycf);
		gen12Bold(job.getPoNumber(), 419, yc);
	}

	protected void pressCheckBox(Job job) throws IOException {
		addBox(1f, 504f, 77f, 662f, 135f, Color.GRAY);
		gen12("Press Check", 510, 92);
		gen8("yes", 596, 90);
		gen8("no", 633, 90);
		checkBox(580, 93, job.getPressCheck() == Job.PRESSCHECKTRUE ? true : false);
		checkBox(617, 93, job.getPressCheck() == Job.PRESSCHECKFALSE ? true : false);
		gen12("Contact:", 510, 108);
		addText(job.getPressCheckContactName(), PDType1Font.TIMES_BOLD, 10, 555, 108);
		addHorizontal(1, 555f, 662f, 110f, Color.GRAY);
		gen12("Phone:", 510, 126);
		addHorizontal(1, 555f, 662f, 127f, Color.GRAY);
		addText(job.getPressCheckContactPhone(), PDType1Font.TIMES_BOLD, 10, 555, 126);
	}

	private void descriptionTable(Job job, float yc) throws IOException {
		addHorizontal(2, 50, 660, yc - 1, Color.LIGHT_GRAY);
		addVertical(1, 50, yc - 2, yc + 12, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 2, yc + 12, Color.LIGHT_GRAY);
		int[][] columns = new int[][] { { 50 }, { 107 }, { 500 }, { 580 }, { 660 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] { "Quantity", "                          Item", "Flat Size", "Final Size" });
			}
		}, PDType1Font.TIMES_ITALIC, 12f, yc, 12f, Color.LIGHT_GRAY, true);
		LinkedList<String[]> descLines = new LinkedList<String[]>();
		for (int i = 0; i < descriptionLinesCount; i++) {
			if (job.getJobItems().size() > descriptionLinesCount && i == descriptionLinesCount - 1) {
				break;
			} else if (job.getJobItems().size() > i) {
				JobItem item = job.getJobItems().get(i);
				descLines.add(new String[] { item.getQuantity(), item.getDescription(), item.getFlatSize(), item.getFinalSize() });
			} else {
				descLines.add(new String[] { "", "", "", "" });
			}
		}
		columns = new int[][] { { 50, 11 }, { 107, 81 }, { 500, 15 }, { 580, 15 }, { 660 } };
		addTable(columns, descLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
		if (job.getJobItems().size() > descriptionLinesCount) {
			addText("                          ****** " + "ADDITIONAL LINES ON ATTACHMENT" + " ******                 ", PDType1Font.COURIER_BOLD_OBLIQUE, 10f, 50f,
					yc + 109, Color.BLACK);
			addVertical(1f, 660, yc + 100, yc + 116, Color.LIGHT_GRAY);
		}
	}

	private void page2DescriptionTable(Job job, float yc) throws IOException {
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(0, pageHeight - yc, 610, 8f);
		addHorizontal(2, 1, 611, yc, Color.LIGHT_GRAY);
		addText("DESCRIPTION", PDType1Font.COURIER_BOLD, 11f, 275f, yc, Color.WHITE);
		int[][] columns = new int[][] { { 1 }, { 58 }, { 451 }, { 531 }, { 611 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] { "Quantity", "                          Item", "Flat Size", "Final Size" });
			}
		}, PDType1Font.TIMES_ITALIC, 12f, yc, 12f, Color.LIGHT_GRAY, true);
		LinkedList<String[]> descLines = new LinkedList<String[]>();
		for (int i = descriptionLinesCount - 1; i < job.getJobItems().size(); i++) {
			JobItem item = job.getJobItems().get(i);
			descLines.add(new String[] { item.getQuantity(), item.getDescription(), item.getFlatSize(), item.getFinalSize() });
		}
		columns = new int[][] { { 1, 11 }, { 58, 81 }, { 451, 15 }, { 531, 15 }, { 611 } };
		addTable(columns, descLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
	}

	private void prePressTable(Job job, float yc) throws IOException {
		//			block("PRE-PRESS", 12, 27, yc + 130, 135, 25, 270);
		addHorizontal(2f, 50f, 660f, yc, Color.LIGHT_GRAY);
		addVertical(1, 50, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addBox(50f, yc, 660f, yc + 131, Color.LIGHT_GRAY);
		addVertical(1, 159f, yc - 1, yc + 131, Color.LIGHT_GRAY);
		addVertical(1, 298f, yc - 1, yc + 131, Color.LIGHT_GRAY);
		typeOfProof(job, 60f, yc + 12);
		customerSupplied(job, 170f, yc + 12);
		prePress3(job, 300f, yc + 12);
	}

	private void typeOfProof(Job job, float xc, float yc) throws IOException {
		float cxc = xc + 3;
		float sxc = xc + 16;
		float vspace = 16;
		gen12("Type of Proof:", xc + 18, yc);
		yc += vspace;
		checkBox(cxc, yc + 3, job.isPdf2Go());
		gen("Pdf2Go", sxc, yc);
		yc += vspace;
		checkBox(cxc, yc + 3, job.isBwLaser());
		gen("B/W Laser", sxc, yc);
		yc += vspace;
		checkBox(cxc, yc + 3, job.isIris());
		gen("Iris", sxc, yc);
		yc += vspace;
		checkBox(cxc, yc + 3, job.isDigitalColor());
		gen("Digital Color", sxc, yc);
		yc += vspace;
		gen12("Other:", cxc, yc);
		addText(job.getProofOther(), PDType1Font.TIMES_BOLD, 12, cxc, yc + 12);
	}

	private void customerSupplied(Job job, float xc, float yc) throws IOException {
		gen12("Customer supplied", xc, yc);
		float vspace = 16;
		yc += vspace;
		checkBox(xc, yc + 3, "mac".equalsIgnoreCase(job.getMacpc()));
		gen12("Mac", xc + 19, yc);
		checkBox(xc + 62, yc + 3, "pc".equalsIgnoreCase(job.getMacpc()));
		gen12("PC", xc + 75, yc);
		addHorizontal(1, 159f, 298f, yc + 10, Color.LIGHT_GRAY);
		yc += vspace + 10;
		checkBox(xc, yc + 3, job.isCdRom());
		gen("CD-Rom", xc + 19, yc);
		yc += vspace;
		checkBox(xc, yc + 3, job.isFloppy());
		gen("Floppy", xc + 19, yc);
		yc += vspace;
		checkBox(xc, yc + 3, job.isZip());
		gen("Zip", xc + 19, yc);
		yc += vspace;
		checkBox(xc, yc + 3, job.isJaz());
		gen("Jaz", xc + 19, yc);
		yc += vspace;
		checkBox(xc, yc + 3, job.isFilm());
		gen("Film", xc + 19, yc);
		xc += 62;
		yc -= vspace * 4;
		checkBox(xc, yc + 3, job.isFtp());
		gen("FTP", xc + 19, yc);
		yc += vspace;
		checkBox(xc, yc + 3, job.isEmail());
		gen("Email", xc + 19, yc);
		yc += vspace;
		gen12("Other:", xc, yc);
		yc += vspace;
		addText(job.getTypeOther(), PDType1Font.TIMES_BOLD, 12, xc, yc);
	}

	private void prePress3(Job job, float xc, float yc) throws IOException {
		gen12("PROOF TO:", xc, yc);
		addHorizontal(1, 298f, 660f, yc + 2, Color.LIGHT_GRAY);
		xc += 72;
		gen12Bold(job.getProofTo(), xc, yc);
		xc += 132;
		gen12("DATE:", xc, yc);
		xc += 45;
		if (job.getProofToDate() != null)
			gen12Bold(SDF.get().format(job.getProofToDate()), xc, yc);
		yc += 19;
		xc -= (72 + 132 + 45);
		gen("Notes:", xc, yc);
		splitUpText(job.getPrePressNotes(), 60, xc, yc + 12, 10, 9, PDType1Font.COURIER_BOLD, 10f);
	}

	private void stockTable(Job job, float yc) throws IOException {
		addHorizontal(2f, 50f, 660f, yc, Color.LIGHT_GRAY);
		addVertical(1, 50, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 1, yc + 1, Color.LIGHT_GRAY);
		// 
		int[][] columns = new int[][] { { 50 }, { 85 }, { 162 }, { 196 }, { 339 }, { 361 }, { 534 }, { 609 }, { 629 }, { 660 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"Sheets",
							"              Size",
							"    Wt.",
							"                    Grade/Color",
							" ",
							"               On Hand or Ordered From",
							"  Press Sheet Size",
							"Out",
							"   Up " });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		addText("Cover/", PDType1Font.TIMES_ITALIC, 6, 341, yc + 5);
		addText("  Text", PDType1Font.TIMES_ITALIC, 6, 341, yc + 13);
		//
		LinkedList<String[]> stockLines = new LinkedList<String[]>();
		for (int i = 0; i < stockLinesCount - 1; i++) {
			if (job.getStockItems().size() > stockLinesCount - 1 && i == stockLinesCount - 2) {
				break;
			} else if (job.getStockItems().size() > i) {
				StockItem item = job.getStockItems().get(i);
				stockLines.add(new String[] {
						item.getSheets(),
							item.getSize(),
							item.getWeight(),
							item.getGradeColor(),
							item.getCoverText().equals("Cover") ? "CVR" : item.getCoverText().equals("Text") ? "TXT" : item.getCoverText(),
							item.getOnHandOrOrderFrom() + " " + item.getPpm(),
							item.getPressSheetSize(),
							item.getPsOut(),
							item.getNumberUp() });
			} else {
				stockLines.add(new String[] { "", "", "", "", "", "", "", "", "" });
			}
		}

		String stockTotal = new DecimalFormat("$###,###.00").format(job.retrieveStockTotal());
		if (job.getOrderedDate() != null)
			stockTotal = stockTotal + " " + new SimpleDateFormat("M/d/yy").format(job.getOrderedDate());
		stockLines.add(new String[] { "", "", "", "", "", stockTotal, "", "", "" });
		columns = new int[][] { { 50, 6 }, { 85, 15 }, { 162, 6 }, { 196, 29 }, { 339, 4 }, { 361, 35 }, { 534, 15 }, { 611, 3 }, { 629, 5 }, { 660 } };
		addTable(columns, stockLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
		if (job.getStockItems().size() > stockLinesCount - 1) {
			addText("                          ****** " + "ADDITIONAL LINES ON ATTACHMENT" + " ******                 ", PDType1Font.COURIER_BOLD_OBLIQUE, 10f, 50f,
					yc + 109, Color.BLACK);
			addVertical(1f, 660, yc + 100, yc + 116, Color.LIGHT_GRAY);
		}
	}

	private void page2StockTable(Job job, float yc) throws IOException {
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(0, pageHeight - yc, 610, 8f);
		addHorizontal(2, 1, 611, yc, Color.LIGHT_GRAY);
		addText("STOCK", PDType1Font.COURIER_BOLD, 11f, 300f, yc - .5f, Color.WHITE);
		int[][] columns = new int[][] { { 1 }, { 36 }, { 113 }, { 147 }, { 290 }, { 312 }, { 485 }, { 562 }, { 580 }, { 611 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"Sheets",
							"              Size",
							"    Wt.",
							"                    Grade/Color",
							" ",
							"               On Hand or Ordered From",
							"  Press Sheet Size",
							"Out",
							"   Up " });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		addText("Cover/", PDType1Font.TIMES_ITALIC, 6, 292, yc + 5);
		addText("  Text", PDType1Font.TIMES_ITALIC, 6, 292, yc + 13);
		//
		LinkedList<String[]> stockLines = new LinkedList<String[]>();
		for (int i = stockLinesCount - 2; i < job.getStockItems().size(); i++) {
			StockItem item = job.getStockItems().get(i);
			stockLines.add(new String[] {
					item.getSheets(),
						item.getSize(),
						item.getWeight(),
						item.getGradeColor(),
						item.getCoverText().equals("Cover") ? "CVR" : item.getCoverText().equals("Text") ? "TXT" : "ENV",
						item.getOnHandOrOrderFrom() + " " + item.getPpm(),
						item.getPressSheetSize(),
						item.getPsOut(),
						item.getNumberUp() });
		}
		addTable(columns, stockLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
	}

	private void pressTable(Job job, float yc) throws IOException {
		addHorizontal(2f, 50f, 660f, yc, Color.LIGHT_GRAY);
		addVertical(1, 50, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 1, yc + 1, Color.LIGHT_GRAY);
		// 
		int[][] columns = new int[][] { { 50 }, { 121 }, { 148 }, { 217 }, { 253 }, { 435 }, { 477 }, { 517 }, { 572 }, { 627 }, { 660 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"          Form#",
							"Press",
							"Type of Form",
							"Colors",
							"                     Color of Ink",
							"",
							" Overs",
							"       Total",
							"Final Count",
							"Initials" });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		addText("  Basic #", PDType1Font.TIMES_ITALIC, 6, 442, yc + 5);
		addText("of Sheets", PDType1Font.TIMES_ITALIC, 6, 442, yc + 13);
		//
		LinkedList<String[]> pressLines = new LinkedList<String[]>();
		for (int i = 0; i < pressLinesCount; i++) {
			if (job.getPressItems().size() > pressLinesCount && i == pressLinesCount - 1) {
				break;
			} else if (job.getPressItems().size() > i) {
				PressItem item = job.getPressItems().get(i);
				pressLines.add(new String[] {
						item.getFormNumber(),
							item.getPress(),
							item.getForm(),
							item.getPressColor(),
							item.getColorOfInk(),
							item.getBasicNoOfSheets(),
							item.getOvers(),
							item.getFinalCount(),
							item.getInitials() });
			} else {
				pressLines.add(new String[] { "", "", "", "", "", "", "", "", "", "" });
			}
		}
		columns = new int[][] {
				{ 50, 14 },
					{ 121, 4 },
					{ 148, 13 },
					{ 217, 6 },
					{ 253, 37 },
					{ 435, 8 },
					{ 477, 7 },
					{ 517, 10 },
					{ 572, 10 },
					{ 627, 5 },
					{ 660 } };
		addTable(columns, pressLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
		if (job.getPressItems().size() > pressLinesCount) {
			addText("                          ****** " + "ADDITIONAL LINES ON ATTACHMENT" + " ******                 ", PDType1Font.COURIER_BOLD_OBLIQUE, 10f, 50f,
					yc + 109, Color.BLACK);
			addVertical(1f, 660, yc + 100, yc + 116, Color.LIGHT_GRAY);
		}
	}

	private void page2PressTable(Job job, float yc) throws IOException {
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(0, pageHeight - yc, 610, 8f);
		addHorizontal(2, 1, 611, yc, Color.LIGHT_GRAY);
		addText("PRESS ROOM", PDType1Font.COURIER_BOLD, 11f, 280f, yc - .5f, Color.WHITE);
		// 
		int[][] columns = new int[][] { { 1 }, { 72 }, { 99 }, { 168 }, { 204 }, { 386 }, { 428 }, { 466 }, { 523 }, { 578 }, { 611 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"          Form#",
							"Press",
							"Type of Form",
							"Colors",
							"                     Color of Ink",
							"",
							" Overs",
							"       Total",
							"Final Count",
							"Initials" });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		addText("  Basic #", PDType1Font.TIMES_ITALIC, 6, 393, yc + 5);
		addText("of Sheets", PDType1Font.TIMES_ITALIC, 6, 393, yc + 13);
		//
		LinkedList<String[]> pressLines = new LinkedList<String[]>();
		for (int i = pressLinesCount - 1; i < job.getPressItems().size(); i++) {
			PressItem item = job.getPressItems().get(i);
			pressLines.add(new String[] {
					item.getFormNumber(),
						item.getPress(),
						item.getForm(),
						item.getPressColor(),
						item.getColorOfInk(),
						item.getBasicNoOfSheets(),
						item.getOvers(),
						item.getFinalCount(),
						item.getInitials() });
		}
		columns = new int[][] { { 1, 14 }, { 72, 4 }, { 99, 13 }, { 168, 6 }, { 204, 37 }, { 386, 8 }, { 428, 7 }, { 466, 10 }, { 523, 10 }, { 578, 5 }, { 611 } };
		addTable(columns, pressLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
	}

	private void outsideServicesTable(Job job, float yc) throws IOException {
		addHorizontal(2f, 50f, 660f, yc, Color.LIGHT_GRAY);
		addVertical(1, 50, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 1, yc + 1, Color.LIGHT_GRAY);
		// 
		int[][] columns = new int[][] { { 50 }, { 157 }, { 410 }, { 475 }, { 587 }, { 660 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"            Vendor",
							"                                                Item",
							"      PO#",
							"             Reference",
							"        Price" });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		//
		LinkedList<String[]> osLines = new LinkedList<String[]>();
		for (int i = 0; i < osLinesCount; i++) {
			if (job.getOsItems().size() > osLinesCount && i == osLinesCount - 1) {
				break;
			} else if (job.getOsItems().size() > i) {
				OSItem item = job.getOsItems().get(i);
				String text = "";
				try {
					double price = Double.parseDouble(item.getPrice());
					text = DF.get().format(price);
				} catch (Exception e) {
				}
				//Right-justify price
				while ((PDType1Font.COURIER_BOLD.getStringWidth(text) / 1000 * 8f) < 65)
					text = " " + text;
				osLines.add(
						new String[] { item.getVendor(), item.getItem(), item.getVendor().equals("In House") ? "" : item.getPoNumber(), item.getReference(), text });
			} else {
				osLines.add(new String[] { "", "", "", "", "" });
			}
		}
		columns = new int[][] { { 50, 21 }, { 157, 31 }, { 410, 10 }, { 475, 45 }, { 587, 14 }, { 660 } };
		addTable(columns, osLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
		if (job.getOsItems().size() > osLinesCount) {
			addText("                          ****** " + "ADDITIONAL LINES ON ATTACHMENT" + " ******                 ", PDType1Font.COURIER_BOLD_OBLIQUE, 10f, 50f,
					yc + 60, Color.BLACK);
			addVertical(1f, 660, yc + 51, yc + 67, Color.LIGHT_GRAY);
		}
	}

	private void page2OutsideServicesTable(Job job, float yc) throws IOException {
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(0, pageHeight - yc, 610, 8f);
		addHorizontal(2, 1, 611, yc, Color.LIGHT_GRAY);
		addText("OUTSIDE SERVICES / MISCELLANEOUS", PDType1Font.COURIER_BOLD, 11f, 200F, yc - .5f, Color.WHITE);// 
		// 
		int[][] columns = new int[][] { { 1 }, { 108 }, { 361 }, { 426 }, { 538 }, { 611 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] {
						"            Vendor",
							"                                                Item",
							"      PO#",
							"             Reference",
							"        Price" });
			}
		}, PDType1Font.TIMES_ITALIC, 10f, yc, 12f, Color.LIGHT_GRAY, true);
		//
		LinkedList<String[]> osLines = new LinkedList<String[]>();
		for (int i = osLinesCount - 1; i < job.getOsItems().size(); i++) {
			OSItem item = job.getOsItems().get(i);
			String text = DF.get().format(item.getPrice());
			//Right-justify price
			while ((PDType1Font.COURIER_BOLD.getStringWidth(text) / 1000 * 8f) < 65)
				text = " " + text;
			osLines.add(new String[] {
					item.getVendor(),
						item.getItem(),
						item.getVendor().equals("In House") ? "" : item.getPoNumber() + "",
						item.getReference(),
						text });
		}
		columns = new int[][] { { 1, 21 }, { 108, 31 }, { 361, 10 }, { 426, 45 }, { 538, 14 }, { 611 } };
		addTable(columns, osLines, PDType1Font.COURIER_BOLD, 8f, yc + 14, 12f, Color.LIGHT_GRAY, true);
	}

	private void binderyTable(Job job, float yc) throws IOException {
		float xc = 50f;
		addHorizontal(2f, 50f, 660f, yc, Color.LIGHT_GRAY);
		addVertical(1, xc, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addVertical(1, 660, yc - 1, yc + 1, Color.LIGHT_GRAY);
		addBox(xc, yc, xc + 130, yc + 86, Color.LIGHT_GRAY);
		LinkedList<String[]> lines = new LinkedList<String[]>();
		if (job.getTrimTo1() != null && job.getTrimTo1().trim().length() > 0) {
			lines.add(new String[] { "Trim To " + job.getTrimTo1() + " x " + job.getTrimTo2() });
		}
		if (job.getFoldTo1() != null && job.getFoldTo1().trim().length() > 0) {
			lines.add(new String[] { "Fold To " + job.getFoldTo1() + " x " + job.getFoldTo2() });
		}
		if (job.getShrinkIn() != null && job.getShrinkIn().trim().length() > 0) {
			lines.add(new String[] { "Shrink in " + job.getShrinkIn() + "'s" });
		}
		int[][] columns = new int[][] { { 50, 26 }, { 180 } };
		addTable(columns, lines, PDType1Font.COURIER_BOLD, 8f, yc, 12f, Color.LIGHT_GRAY, false);
		//
		xc += 130f;
		lines = new LinkedList<String[]>() {
			{
				add(new String[] { "   Score" });
				add(new String[] { "   Perf" });
				add(new String[] { "   Drill" });
				add(new String[] { "   Saddlestitch" });
				add(new String[] { "   Pocket" });
				add(new String[] { "   Letterpress" });
				add(new String[] { "" });
			}
		};
		columns = new int[][] { { 180, 18 }, { 270 } };
		addTable(columns, lines, PDType1Font.COURIER_OBLIQUE, 8f, yc, 12f, Color.LIGHT_GRAY, true);
		checkBox(xc + 2, yc + 14, job.isScore());
		checkBox(xc + 2, yc + 26, job.isPerf());
		checkBox(xc + 2, yc + 38, job.isDrill());
		checkBox(xc + 2, yc + 50, job.isSaddlestitch());
		checkBox(xc + 2, yc + 62, job.isPocket());
		checkBox(xc + 2, yc + 74, job.isLetterpress());
		if (job.isDrill() && job.getDrillSize().trim().length() > 0) {
			addText("(" + job.getDrillSize() + ")", PDType1Font.COURIER_OBLIQUE, 8f, xc + 44, yc + 34, Color.BLACK);
		}
		addBox(270, yc, 660f, yc + 86, Color.LIGHT_GRAY);
		addText("Notes:", PDType1Font.COURIER_OBLIQUE, 8f, 274, yc + 10, Color.BLACK);
		splitUpText(job.getBinderyNotes(), 64, 274, yc + 22, 10f, 7, PDType1Font.COURIER_BOLD, 10f);
	}

	private void deliveryTable(final Job job, float yc) throws IOException {
		int[][] columns = new int[][] { { 26 }, { 343 } };
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] { "Delivery Ticket # " + job.getDeliveryTicketNumber() });
				add(new String[] { "Quantity Shipped: " + job.getQuantityShipped() });
				add(new String[] { "# of Boxes: " + job.getNumberOfBoxes() });
				add(new String[] { "Quantity per Box: " + job.getQuantityPerBox() });
				add(new String[] { "# of Samples: " + job.getNumberOfSamples() });
				add(new String[] { "Samples sent to: " + job.getSamplesTo() });
				add(new String[] { "" });
			}
		}, PDType1Font.TIMES_BOLD, 10, yc, 14f, Color.LIGHT_GRAY, true);
	}

	private void shippingTable(final Job job, float yc) throws IOException {
		int[][] columns = new int[][] { { 343 }, { 660 } };
		/*
		 * Parse shippping address
		 */
		final String sa[] = new String[] { "", "", "" };
		if (job.getShippingAddress() != null) {
			BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(job.getShippingAddress().getBytes())));
			try {
				String addr = br.readLine();
				if (addr != null) {
					sa[0] = addr;
					addr = br.readLine();
					if (addr != null) {
						sa[1] = "                  " + addr;
						addr = br.readLine();
						if (addr != null) {
							sa[2] = "                  " + addr;
						}
					}
				}
			} catch (IOException e1) {
			}
		}
		addTable(columns, new LinkedList<String[]>() {
			{
				add(new String[] { "Ship To: " + job.getShipTo() });
				add(new String[] { "Address:   " + sa[0] });
				add(new String[] { sa[1] });
				add(new String[] { sa[2] });
				add(new String[] { "Ship Via: " + job.getShipVia() });
				add(new String[] { "Special Instructions: " + job.getShippingNotes() });
				add(new String[] { "Chargeable Revisions:" + job.getChargeableRevisions() });
			}
		}, PDType1Font.TIMES_BOLD, 10f, yc, 14f, Color.LIGHT_GRAY, true);
		addText("        Price: " + DF.get().format(job.getPrice()), PDType1Font.TIMES_BOLD, 10f, 540, yc + 96);
	}
}
