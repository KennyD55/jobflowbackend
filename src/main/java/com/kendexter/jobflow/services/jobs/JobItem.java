package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;

@SuppressWarnings("serial")
public class JobItem implements Serializable {
	private int jobNumber;
	private int seq;
	private String quantity;
	private String description;
	private String flatSize;
	private String finalSize;

	public boolean empty() {
		return ((quantity == null || quantity.trim().length() == 0)
				&& (description == null || description.trim().length() == 0)
				&& (flatSize == null || flatSize.trim().length() == 0) && (finalSize == null || finalSize.trim()
				.length() == 0));
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getQuantity() {
		return quantity == null ? "" : quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description == null ? "" : description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFlatSize() {
		return flatSize == null ? "" : flatSize;
	}

	public void setFlatSize(String flatSize) {
		this.flatSize = flatSize;
	}

	public String getFinalSize() {
		return finalSize == null ? "" : finalSize;
	}

	public void setFinalSize(String finalSize) {
		this.finalSize = finalSize;
	}
}
