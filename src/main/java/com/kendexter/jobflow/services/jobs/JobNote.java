package com.kendexter.jobflow.services.jobs;

import java.sql.Timestamp;

public class JobNote {
	private int id;
	private int jobNumber;
	private String userName;
	private Timestamp ts;
	private String comment;

	public JobNote() {
	}

	public JobNote(int id, int jobNumber, String userName, Timestamp ts, String comment) {
		super();
		this.id = id;
		this.jobNumber = jobNumber;
		this.userName = userName;
		this.ts = ts;
		this.comment = comment;
	}

	/* JSON */
	public int getRows() {
		// Needed to format textarea
		String rows[] = comment.split("\\n");
		int rowCount = rows.length;
		// Add 1 for each especially long row
		for (String row : rows) {
			int length = row.length();
			while (length > 85) {
				rowCount++;
				length -= 85;
			}
		}
		return rowCount;
	}

	/******/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
