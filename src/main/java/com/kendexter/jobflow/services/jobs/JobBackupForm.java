package com.kendexter.jobflow.services.jobs;

import java.sql.Date;

public class JobBackupForm {
	private int jobNumber;
	private int digitalOffset;
	private String customerName;
	private String description;
	private Date dateDue;
	private String status;
	private boolean flaggedForBackup;

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public int getDigitalOffset() {
		return digitalOffset;
	}

	public void setDigitalOffset(int digitalOffset) {
		this.digitalOffset = digitalOffset;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDue() {
		return dateDue;
	}

	public void setDateDue(Date dateDue) {
		this.dateDue = dateDue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isFlaggedForBackup() {
		return flaggedForBackup;
	}

	public void setFlaggedForBackup(boolean flaggedForBackup) {
		this.flaggedForBackup = flaggedForBackup;
	}
}
