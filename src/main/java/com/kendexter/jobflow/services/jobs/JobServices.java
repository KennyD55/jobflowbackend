package com.kendexter.jobflow.services.jobs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.kendexter.Utilities;
import com.kendexter.jobflow.beans.TokenAttribute;
import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.filters.AuthenticationFilter;
import com.kendexter.jobflow.services.customers.CustomersDAO;
import com.kendexter.jobflow.services.users.User;

@Path("jobs")
public class JobServices {
	private static Logger logger = Logger.getLogger(JobServices.class);
	private @Context ServletContext context;
	private static Object mutex = new Object();

	@Path("jobList")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobList(@QueryParam("customer") String customer, @QueryParam("includeArchived") boolean includeArchived,
			@QueryParam("jobNumber") String jobNumber, @QueryParam("description") String description, @Context HttpServletRequest request) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<JobSummary> jobs = null;
			User user = (User) request.getAttribute("user");
			Collection<String> functions = user.getFunctions();
			if (functions.contains(AppContext.MAINTAIN_JOBS)) {
				jobs = JobsDAO.getJobList(connection, customer, includeArchived, jobNumber, description);
			} else {

				jobs = JobsDAO.getJobList(connection, user.getName(), customer, includeArchived, jobNumber, description);
			}
			return Response.ok().entity(jobs).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJob")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJob(@QueryParam("jobNumber") String jobNumber) {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				Job job = JobsDAO.getJob(connection, jobNumber);
				if (job == null) {
					return Response.ok().entity("{\"error\":\"Job " + jobNumber + " not found\"}").build();
				}
				return Response.ok().entity(job).build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("getJobSummary")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobSummary(@QueryParam("jobNumber") String jobNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			JobSummary job = JobsDAO.getJobSummary(connection, jobNumber);
			if (job == null) {
				return Response.ok().entity("{\"error\":\"Job" + jobNumber + " not found\"}").build();
			}
			return Response.ok().entity(job).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJobNotes")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobNotes(@QueryParam("jobNumber") int jobNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			LinkedList<JobNote> jobNotes = JobsDAO.getJobNotes(connection, jobNumber);
			return Response.ok().entity(jobNotes).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("viewJob")
	@GET
	@PermitAll
	public Response viewJob(@QueryParam("jobNumber") String jobNumber, @QueryParam("onetimeToken") String onetimeToken, @Context HttpServletRequest request) {
		try {
			TokenAttribute ta = AuthenticationFilter.getOneTimeToken(context, onetimeToken); // Verifies and removes one-time token
			if (ta == null) {
				ResponseBuilder rb = Response.ok("<html><head></head><body>Page has expired</body><html>");
				rb.type(MediaType.TEXT_HTML);
				return rb.build();
			}
			User user = ta.getUser();

			if (!user.hasAuthority(AppContext.MAINTAIN_JOBS) && !user.hasAuthority(AppContext.MAINTAIN_MY_CUSTOMERS)) {
				ResponseBuilder rb = Response
						.ok("<html><head></head><body style=\"background-color:black;font-size:18px;color:red;text-align:center\">Not Authorized</body><html>");
				rb.type(MediaType.TEXT_HTML);
				return rb.build();
			}

			if (!user.hasAuthority(AppContext.MAINTAIN_JOBS)) {
				Connection connection = null;
				try {
					connection = AppContext.jobflowDataSource.getConnection();
					JobSummary job = JobsDAO.getJobSummary(connection, jobNumber);
					if (!job.getSalesrep().equals(user.getName())) {
						ResponseBuilder rb = Response.ok(
								"<html><head></head><body style=\"background-color:black;font-size:18px;color:red;text-align:center\">Not Authorized</body><html>");
						rb.type(MediaType.TEXT_HTML);
						return rb.build();
					}
				} finally {
					connection.close();
				}
			}
			PDDocument document = new JobTicketPDF(request).generatePDF();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			document.save(baos);
			document.close();
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ResponseBuilder rb = Response.ok((Object) bais);
			rb.type("application/pdf");
			return rb.build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("");
		}
	}

	@Path("confirmJobNumber")
	@GET
	@PermitAll
	@Produces(MediaType.TEXT_PLAIN)
	public Response confirmJobNumber(@QueryParam("jobNumber") int jobNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM JOBS WHERE JOB_NUMBER = ?");
			select.setInt(1, jobNumber);
			ResultSet rs = select.executeQuery();
			rs.next();
			int count = rs.getInt(1);
			rs.close();
			select.close();
			if (count == 0)
				return Response.ok().entity("not found").build();
			else
				return Response.ok().entity("found").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("enterJob")
	@POST
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized Response enterJob(Job job, @Context HttpServletRequest request) {
		User user = (User) request.getAttribute("user");
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			/*
			 *   Check that the job hasn't been modified
			 */
			if (job.getJobNumber() != 0) {  // Job being entered
				Job oldJob = JobsDAO.getJob(connection, job.getJobNumber());
				System.out.println(getClass() + " compareResult=" + Utilities.compareBeans(oldJob, job));

				PreparedStatement select = connection.prepareStatement("SELECT LAST_UPDATE, WHO_MODIFIED FROM JOBS WHERE JOB_NUMBER = ?");
				select.setInt(1, job.getJobNumber());
				ResultSet rs = select.executeQuery();
				if (!rs.next()) {
					rs.close();
					select.close();
					return Response.ok().entity("{\"deleted\": \"Job " + job.getJobNumber() + " has been deleted.\"}").build();
				}
				Timestamp lastUpdate = rs.getTimestamp("LAST_UPDATE");
				String whoModified = rs.getString("WHO_MODIFIED");
				rs.close();
				select.close();
				if (lastUpdate.compareTo(job.getLastUpdate()) != 0) {
					return Response.ok()
							.entity("{\"updated\": \"Job " + job.getJobNumber() + " has been updated by user " + whoModified + ".  Please re-enter your changes.\"}")
							.build();
				}
			}

			JobsDAO.upsertJob(connection, job, user.getName());

			CustomersDAO.upsertContact(connection, job.getAccountNumber(), job.getContactName(), job.getContactPhone(), job.getContactEmail());

			CustomersDAO.upsertPressContact(connection, job.getAccountNumber(), job.getPressCheckContactName(), job.getPressCheckContactPhone());
			connection.commit();
			return Response.ok().entity(job).build();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJobsByStatus")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.PROMOTE })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobsByStatus(@QueryParam("status") String status) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<JobSummary> jobs = JobsDAO.getJobsByStatus(connection, status);
			return Response.ok().entity(jobs).build();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJobStatus")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.PROMOTE })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobStatus(@QueryParam("jobNumber") String jobNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT STATUS FROM JOBS WHERE JOB_NUMBER = ? AND ARCHIVED = 0");
			select.setString(1, jobNumber);
			ResultSet rs = select.executeQuery();
			if (!rs.next()) {
				rs.close();
				select.close();
				return Response.ok().entity("{\"notfound\":\"Job " + jobNumber + " not found or has been archived\"}").build();
			}
			Collection<JobSummary> jobs = JobsDAO.getJobsByStatus(connection, rs.getString(1));
			rs.close();
			select.close();
			return Response.ok().entity(jobs).build();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJobHistory")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_JOBS, AppContext.MAINTAIN_MY_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobHistory(@QueryParam("jobNumber") int jobNumber) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			LinkedList<JobHistory> histories = JobsDAO.getJobHistory(connection, jobNumber);
			return Response.ok().entity(histories).build();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getJobsReadyForBackup")
	@GET
	@RolesAllowed({ AppContext.BACKUP_JOBS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobsReadyForBackup() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Job> getJobsReadyForBackup = JobsDAO.getJobsReadyForBackup(connection);
			return Response.ok().entity(getJobsReadyForBackup).build();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("backupJobs")
	@POST
	@RolesAllowed({ AppContext.BACKUP_JOBS })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response backupJobs(Collection<JobBackupForm> jobs) {
		synchronized (mutex) {

			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement update = connection.prepareStatement("UPDATE JOBS SET BACKUP_FLAG = 2 WHERE JOB_NUMBER = ?");
				for (JobBackupForm job : jobs) {
					if (job.isFlaggedForBackup()) {
						update.setInt(1, job.getJobNumber());
						update.executeUpdate();
					}
				}
				update.close();
				connection.commit();
				return getJobsReadyForBackup();
			} catch (Exception e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
				}
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("deleteDepartmentFromNotes")
	@POST
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteDepartmentFromNotes(@QueryParam("departmentName") String departmentName, @QueryParam("jobNumber") int jobNumber) {
		synchronized (mutex) {

			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement delete = connection
						.prepareStatement("DELETE FROM JOB_NOTES_DEPARTMENTS WHERE DEPARTMENT_ID = (SELECT ID FROM DEPARTMENTS WHERE NAME = ?) AND JOB_NUMBER = ?");
				delete.setString(1, departmentName);
				delete.setInt(2, jobNumber);
				delete.executeUpdate();
				delete.close();
				connection.commit();
//				updateScheduleJobNotes(connection, jobNumber);
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
				}
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("insertDepartmentToNotes")
	@POST
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertDepartmentToNotes(@QueryParam("departmentName") String departmentName, @QueryParam("jobNumber") int jobNumber) {
		synchronized (mutex) {

			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement select = connection.prepareStatement("SELECT ID FROM DEPARTMENTS WHERE NAME = ?");
				select.setString(1, departmentName);
				ResultSet rs = select.executeQuery();
				rs.next();
				int departmentID = rs.getInt(1);
				rs.close();
				select.close();

				PreparedStatement insert = connection.prepareStatement("INSERT INTO JOB_NOTES_DEPARTMENTS (DEPARTMENT_ID, JOB_NUMBER) VALUES (?, ?)");
				insert.setInt(1, departmentID);
				insert.setInt(2, jobNumber);
				insert.executeUpdate();
				insert.close();
				connection.commit();
//				updateScheduleJobNotes(connection, jobNumber);
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
				}
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("insertJobNote")
	@POST
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertJobNote(@QueryParam("jobNumber") int jobNumber, @QueryParam("comment") String comment, @Context HttpServletRequest request) {
		synchronized (mutex) {

			User user = (User) request.getAttribute("user");
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();

				PreparedStatement insert = connection.prepareStatement("INSERT INTO JOB_NOTES (JOB_NUMBER, USER_NAME, COMMENT) VALUES(?, ?, ?)");
				insert.setInt(1, jobNumber);
				insert.setString(2, user.getName());
				insert.setString(3, comment);
				insert.executeUpdate();
				insert.close();
				connection.commit();
//				updateScheduleJobNotes(connection, jobNumber);
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
				}
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

//	private void updateScheduleJobNotes(Connection connection, int jobNumber) throws Exception {
//		HashMap<String, Schedule> scheduleMap = AppContext.scheduleMap;
//		Set<String> pressNames = scheduleMap.keySet();
//		for (String pressName : pressNames) {
//			Schedule schedule = scheduleMap.get(pressName);
//			for (Task task : schedule.getTasks()) {
//				if (task.getJobNumber() == jobNumber) {
//					JobsDAO.loadJobNoteFlags(connection, task);
//				}
//			}
//
//		}
//	}
}