package com.kendexter.jobflow.services.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

@Path("job")
public class UpdateJobStatusService {
	private static Logger logger = Logger.getLogger(UpdateJobStatusService.class);

	@Path("updateJobStatus")
	@PermitAll
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized Response updateJobStatus(@QueryParam("jobNumber") int jobNumber, @QueryParam("status") String status, @QueryParam("comment") String comment,
			@Context HttpServletRequest request) {
		Connection connection = null;
		try {
			User user = (User) request.getAttribute("user");
			connection = AppContext.jobflowDataSource.getConnection();
			/*
			 * Update job status
			 */
			PreparedStatement update = connection.prepareCall("UPDATE JOBS SET STATUS = ? WHERE JOB_NUMBER = ?");
			update.setString(1, status);
			update.setInt(2, jobNumber);
			update.executeUpdate();
			update.close();
			
			/*
			 * Update job history
			 */
			JobsDAO.insertJobHistory(connection, jobNumber, comment, status, user.getName());

			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
