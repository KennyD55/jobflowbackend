package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;

@SuppressWarnings("serial")
public class OSItem implements Serializable {
	private int jobNumber;
	private int seq;
	private String vendor;
	private String item;
	private String poNumber;
	private String reference;
	private String price;

	public boolean empty() {
		return ((vendor == null || vendor.trim().length() == 0) && (item == null || item.trim().length() == 0)
				&& (poNumber == null || poNumber.trim().length() == 0)
				&& (reference == null || reference.trim().length() == 0) && (price == null || price.trim().length() == 0));
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getVendor() {
		return vendor == null ? "" : vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getItem() {
		return item == null ? "" : item;
	}

	public void setItem(String osItem) {
		this.item = osItem;
	}

	public String getPoNumber() {
		return poNumber == null ? "" : poNumber;
	}

	public void setPoNumber(String osPONumber) {
		this.poNumber = osPONumber;
	}

	public String getReference() {
		return reference == null ? "" : reference;
	}

	public void setReference(String osReference) {
		this.reference = osReference;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String osPrice) {
		this.price = osPrice;
	}

	/*  Deletegate methods for XML conversion */
	public void setOsPrice(double price) {
		this.price = price+"";
	}
	public void setOsContact(String contact) {
//		this.contact = contact;
	}
	public void setOsItem(String item) {
		this.item = item;
	}
	public void setOsPhone(String phone) {
//		this.phone = phone;
	}
	public void setOsPONumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public void setOsReference(String reference) {
		this.reference = reference;
	}
	/*  END Delegate methods */

//	public void load(PurchaseOrderForm po) {
//		setOsPONumber(po.getPoNumber() + "");
//		setOsPrice(po.getTotal());
//		setVendor((po.getAbbr() != null && po.getAbbr().length() > 0) ? po.getAbbr() : po.getVendorName());
//		setOsReference(po.getReference());
//		for (int k = 0; k < po.getItems().size(); k++) {
//			if (po.getItems().get(k).getRate() != null && po.getItems().get(k).getRate().length() > 0) {
//				setOsItem(po.getItems().get(k).getItem());
//				break;
//			}
//		}
//	}
}
