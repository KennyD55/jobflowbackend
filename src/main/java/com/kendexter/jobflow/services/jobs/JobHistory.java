package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class JobHistory implements Serializable {
	private int id;
	private int jobNumber;
	private String name;
	private String status;
	private String comment;
	private Timestamp ts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getTs() {
		return this.ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}
}
