package com.kendexter.jobflow.services.jobs;

import java.io.Serializable;

@SuppressWarnings("serial")
public class StockItem implements Serializable {
	private int jobNumber;
	private int seq;
	private String sheets;
	private String size;
	private String weight;
	private String gradeColor;
	private String coverText;
	private String onHandOrOrderFrom;
	private String pressSheetSize;
	private String psOut;
	private String numberUp;
	private String ppm;
	private String pb;
	private boolean lot;

	public boolean empty() {
		return ((sheets == null || sheets.trim().length() == 0) && (size == null || size.trim().length() == 0)
				&& (weight == null || weight.trim().length() == 0)
				&& (gradeColor == null || gradeColor.trim().length() == 0)
				&& (onHandOrOrderFrom == null || onHandOrOrderFrom.trim().length() == 0)
				&& (pressSheetSize == null || pressSheetSize.trim().length() == 0)
				&& (psOut == null || psOut.trim().length() == 0) && (numberUp == null || numberUp.trim().length() == 0));
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getCoverText() {
		return coverText == null ? "" : coverText;
	}

	public String getGradeColor() {
		return gradeColor == null ? "" : gradeColor;
	}

	public String getNumberUp() {
		return numberUp == null ? "" : numberUp;
	}

	public String getOnHandOrOrderFrom() {
		return onHandOrOrderFrom == null ? "" : onHandOrOrderFrom;
	}

	public String getPressSheetSize() {
		return pressSheetSize == null ? "" : pressSheetSize;
	}

	public String getPsOut() {
		return psOut == null ? "" : psOut;
	}

	public String getSheets() {
		return sheets == null ? "" : sheets;
	}

	public String getSize() {
		return size == null ? "" : size;
	}

	public String getWeight() {
		return weight == null ? "" : weight;
	}

	public String getPpm() {
		return ppm == null ? "" : ppm;
	}

	public void setPpm(String ppm) {
		this.ppm = ppm;
	}

	public void setCoverText(String coverText) {
		this.coverText = coverText;
	}

	public void setGradeColor(String gradeColor) {
		this.gradeColor = gradeColor;
	}

	public void setNumberUp(String numberUp) {
		this.numberUp = numberUp;
	}

	public void setOnHandOrOrderFrom(String onHandOrOrderFrom) {
		this.onHandOrOrderFrom = onHandOrOrderFrom;
	}

	public void setPressSheetSize(String pressSheetSize) {
		this.pressSheetSize = pressSheetSize;
	}

	public void setPsOut(String psOut) {
		this.psOut = psOut;
	}

	public void setSheets(String sheets) {
		this.sheets = sheets;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getPb() {
		return pb == null ? "" : pb;
	}

	public void setPb(String pb) {
		this.pb = pb;
	}

	public String retrieveUnitPrice() {
		if (ppm == null || ppm.trim().length() == 0)
			return "";
		String unitPrice = getPpm() + "/";
		if (getPb().trim().equals("1000"))
			unitPrice += "m";
		else if (getPb().trim().equals("100"))
			unitPrice += "c";
		else
			unitPrice += getPb().trim();
		return unitPrice;
	}

	public boolean isLot() {
		return lot;
	}

	public void setLot(boolean lot) {
		this.lot = lot;
	}
}
