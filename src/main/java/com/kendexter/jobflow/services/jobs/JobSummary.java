package com.kendexter.jobflow.services.jobs;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

public class JobSummary {
	private int jobNumber;
	private String customerName;
	private Timestamp dateSubmitted;
	private String whoSubmitted;
	private String salesrep;
	private String status;
	private double price;
	private String description;
	private Timestamp jeopardyTime;
	private boolean hot;
	private int digitalOffset;
	private boolean archived;
	private boolean printToday;
	private boolean backupFlag;
	private int mailed;
	private Date dateDue;
	private Timestamp lastUpdate;
	private Date dropDate;
	private boolean stockPrint;
	private ArrayList<JobNoteFlag> jobNoteFlags;

	public JobSummary() {
		setJobNoteFlags(new ArrayList<JobNoteFlag>());
	}

	public Date getDateDue() {
		return dateDue;
	}

	public void setDateDue(Date dateDue) {
		this.dateDue = dateDue;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Timestamp getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Timestamp dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getWhoSubmitted() {
		return whoSubmitted;
	}

	public void setWhoSubmitted(String whoSubmitted) {
		this.whoSubmitted = whoSubmitted;
	}

	public String getSalesrep() {
		return salesrep;
	}

	public void setSalesrep(String salesrep) {
		this.salesrep = salesrep;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getJeopardyTime() {
		return jeopardyTime;
	}

	public void setJeopardyTime(Timestamp jeopardyTime) {
		this.jeopardyTime = jeopardyTime;
	}

	public boolean isHot() {
		return hot;
	}

	public void setHot(boolean hot) {
		this.hot = hot;
	}

	public int getDigitalOffset() {
		return digitalOffset;
	}

	public void setDigitalOffset(int digitalOffset) {
		this.digitalOffset = digitalOffset;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public boolean isPrintToday() {
		return printToday;
	}

	public void setPrintToday(boolean printToday) {
		this.printToday = printToday;
	}

	public boolean isBackupFlag() {
		return backupFlag;
	}

	public void setBackupFlag(boolean backupFlag) {
		this.backupFlag = backupFlag;
	}

	public int getMailed() {
		return mailed;
	}

	public void setMailed(int mailed) {
		this.mailed = mailed;
	}

	public Date getDropDate() {
		return dropDate;
	}

	public void setDropDate(Date dropDate) {
		this.dropDate = dropDate;
	}

	public boolean isStockPrint() {
		return stockPrint;
	}

	public void setStockPrint(boolean stockPrint) {
		this.stockPrint = stockPrint;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public ArrayList<JobNoteFlag> getJobNoteFlags() {
		return jobNoteFlags;
	}

	public void setJobNoteFlags(ArrayList<JobNoteFlag> jobNoteFlags) {
		this.jobNoteFlags = jobNoteFlags;
	}

	public void addJobNoteFlag(JobNoteFlag jobNoteFlag) {
		this.jobNoteFlags.add(jobNoteFlag);
	}
}
