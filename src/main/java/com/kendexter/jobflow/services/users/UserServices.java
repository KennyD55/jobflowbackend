package com.kendexter.jobflow.services.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("users")
public class UserServices {
	private static Logger logger = Logger.getLogger(UserServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getUser")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@QueryParam("userID") String userID) {
		try {
			User user = retrieveUser(userID);
			return user != null ? Response.ok().entity(user).build() : Response.ok().entity("{\"error\":\"No user found\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}

	@Path("verifyUserName")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkUserName(@QueryParam("id") int id, @QueryParam("userName") String userName) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = conn.prepareStatement("SELECT COUNT(*) FROM USERS WHERE NAME = ? AND ID != ?");
			select.setString(1, userName);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null : "User name " + userName + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deleteUser")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@QueryParam("userID") int id) {
		Connection conn = null;
		try {
			User thisUser = AppContext.getUser(context, requestContext);

			if (thisUser.getId() == id) {
				return Response.ok().entity("{\"error\":\"You cannot delete yourself\"}").build();
			}
			conn = AppContext.jobflowDataSource.getConnection();
			UsersDAO.deleteUser(conn, id);
			conn.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyUserID")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkUserID(@QueryParam("id") int id, @QueryParam("userID") String userID) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = conn.prepareStatement("SELECT COUNT(*) FROM USERS WHERE USER_ID = ? AND ID != ?");
			select.setString(1, userID);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null : "User id " + userID + " already used.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("Unexpected system error").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("userList")
	@GET
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserList() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			LinkedList<User> users = UsersDAO.getUserList(connection);
			return Response.ok().entity(users).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertUser")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response userUpdate(UserForm user) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			int count = UsersDAO.updateUser(conn, user);
			conn.commit();
			return Response.ok().entity("{\"count\": \"" + count + "\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getUserForm")
	@GET
	@RolesAllowed({ AppContext.ADMINISTRATION })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserForm(@QueryParam("userID") String userID) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			UserForm userForm = UsersDAO.getUserForm(connection, userID);
			return Response.ok().entity(userForm).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getUsersAssignedAsSalesRep")
	@GET
	@RolesAllowed({ AppContext.MAINTAIN_ALL_CUSTOMERS })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersAssignedAsSalesRep() {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			Collection<String> userNames = UsersDAO.getUsersAssignedAsSalesRep(conn);
			conn.commit();
			return Response.ok().entity(userNames).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static User retrieveUser(String userID) throws Exception {
		User user = null;
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			user = UsersDAO.retrieveUser(connection, userID);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return user;
	}

	public static Collection<Integer> retrieveMonitoredJobs(String userID) throws Exception {
		Collection<Integer> monitoredJobs = null;
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			monitoredJobs = UsersDAO.getMonitoredJobs(connection, userID);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return monitoredJobs;
	}

	@Path("getUserProfile")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserProfile(@Context HttpServletRequest request) {
		try {
			User user = (User) request.getAttribute("user");
			Profile profile = new Profile();
			profile.setUserID(user.getUserID());
			profile.setPassword(user.getPass());
			profile.setPhone(user.getPhone());
			profile.setEmail(user.getEmail());
			profile.setInitials(user.getInitials());
			profile.setMarkJobsWithPurchaseOrders(user.isMarkJobsWithPos());
			profile.setMarkJobsWithSalesRep(user.isMarkJobsWithSalesRep());
			profile.setUseSmallControlTags(user.isSmallTags());
			profile.setDateDisplay(user.getDateFormat());
			return Response.ok().entity(profile).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		}
	}

	@Path("updateUserProfile")
	@POST
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUserProfile(Profile profile, @Context HttpServletRequest request, @Context ContainerRequestContext requestContext) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			User user = (User) request.getAttribute("user");
			if (!user.getUserID().equals(profile.getUserID())) {
				ResponseBuilder ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN).entity("You cannot access this resource");
				requestContext.abortWith(ACCESS_FORBIDDEN.build());
			}
			UsersDAO.updateProfile(conn, profile);
			user.setPass(profile.getPassword());
			user.setPhone(profile.getPhone());
			user.setEmail(profile.getEmail());
			user.setInitials(profile.getInitials());
			user.setMarkJobsWithPos(profile.isMarkJobsWithPurchaseOrders());
			user.setMarkJobsWithSalesRep(profile.isMarkJobsWithSalesRep());
			user.setSmallTags(profile.isUseSmallControlTags());
			user.setDateFormat(profile.getDateDisplay());
			conn.commit();
			return Response.ok().entity(profile).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
