package com.kendexter.jobflow.services.users;

import java.io.Serializable;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class UserForm implements Serializable {
	private int id;
	private String userID;
	private String name;
	private String pass;
	private String initials;
	private String color;
	private LinkedList<JobTitle> jobTitles;

	public UserForm() {
		setJobTitles(new LinkedList<JobTitle>());
	}

	public UserForm(int id, String userID, String name, String pass, String initials, String color) {
		this();
		this.id = id;
		this.userID = userID;
		this.name = name;
		this.pass = pass;
		this.initials = initials;
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public LinkedList<JobTitle> getJobTitles() {
		return jobTitles;
	}

	public void setJobTitles(LinkedList<JobTitle> jobTitles) {
		this.jobTitles = jobTitles;
	}
	
	public void addJobTitle(JobTitle jobTitle) {
		this.jobTitles.add(jobTitle);
	}
}
