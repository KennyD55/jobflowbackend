package com.kendexter.jobflow.services.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.admin.authority.AuthorityDAO;
import com.kendexter.jobflow.services.admin.authority.Role;

public class UsersDAO {
	private static Logger logger = Logger.getLogger(UsersDAO.class);

	private UsersDAO() {
	}

	public static User retrieveUser(Connection connection, String userID) throws Exception {

		PreparedStatement select = connection.prepareStatement(
				"SELECT ID, USER_ID, NAME, PASS, EMAIL, DATE_FORMAT, POP_UPS, PHONE, UNREAD_MESSAGES, LOG_BEGIN_DATE, ROWS_PER_PAGE, INITIALS, MARK_JOBS_WITH_POS, COLOR, MARK_JOBS_WITH_SALES_REP, SMALL_TAGS, SHOW_SCHEDULE_CHANGES"
						+ " FROM USERS WHERE USER_ID = ?");
		select.setString(1, userID);
		ResultSet rs = select.executeQuery();
		if (!rs.next())
			return null;
		User user = loadUserFromResultSet(rs);
		rs.close();
		select.close();

		select = connection
				.prepareStatement("SELECT DISTINCT FUNCTION FROM FUNCTIONS_AUTHORIZED A, USER_ROLES B WHERE A.ROLE = B.ROLE AND B.USER_ID = ? ORDER BY FUNCTION");
		select.setString(1, userID);
		rs = select.executeQuery();
		while (rs.next())
			user.addFunction(rs.getString("FUNCTION"));
		rs.close();
		select.close();

		select = connection.prepareStatement("SELECT ROLE FROM USER_ROLES WHERE USER_ID = ? ORDER BY ROLE");
		select.setString(1, userID);
		rs = select.executeQuery();
		while (rs.next())
			user.addRole(rs.getString("ROLE"));
		rs.close();
		select.close();

		return user;
	}

	public static synchronized int updateUser(Connection conn, UserForm user) throws Exception {

		PreparedStatement upsert = conn.prepareStatement("INSERT INTO USERS (ID, USER_ID, NAME, PASS, INITIALS, COLOR)"
				+ " VALUES (?, ? , ?, ?, ?, ?)"
				+ "  ON DUPLICATE KEY UPDATE USER_ID =  ?, NAME = ?, PASS = ?, INITIALS =?, COLOR = ?");
		int p = 0;
		upsert.setInt(++p, user.getId());
		upsert.setString(++p, user.getUserID());
		upsert.setString(++p, user.getName());
		upsert.setString(++p, user.getPass());
		upsert.setString(++p, user.getInitials());
		upsert.setString(++p, user.getColor());
		upsert.setString(++p, user.getUserID());
		upsert.setString(++p, user.getName());
		upsert.setString(++p, user.getPass());
		upsert.setString(++p, user.getInitials());
		upsert.setString(++p, user.getColor());
		int count = upsert.executeUpdate();
		PreparedStatement delete = conn.prepareStatement("DELETE FROM USER_TITLES WHERE USER_ID = ?");
		delete.setString(1, user.getUserID());
		delete.executeUpdate();
		PreparedStatement insert = conn.prepareStatement("INSERT INTO USER_TITLES (USER_ID, JOB_TITLE) VALUES (?, ?)");
		for (JobTitle jobTitle : user.getJobTitles()) {
			if (jobTitle.isAuthorized()) {
				insert.setString(1, user.getUserID());
				insert.setString(2, jobTitle.getRole());
				insert.executeUpdate();
			}
		}
		upsert.close();
		delete.close();
		insert.close();
		return count;
	}

	private static final String SELECTUSERLIST = "SELECT USER_ID FROM USERS ORDER BY USER_ID";

	public static LinkedList<User> getUserList(Connection connection) throws Exception {
		LinkedList<User> users = new LinkedList<User>();
		ResultSet rs = null;
		PreparedStatement st = null;
		try {
			st = connection.prepareStatement(SELECTUSERLIST);
			rs = st.executeQuery();
			while (rs.next()) {
				User user = retrieveUser(connection, rs.getString("USER_ID"));
				users.add(user);
			}
			return users;
		} finally {
			try {
				rs.close();
				st.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	private static User loadUserFromResultSet(ResultSet rs) throws Exception {
		User user = new User();
		user.setId(rs.getInt("ID"));
		user.setUserID(rs.getString("USER_ID"));
		user.setName(rs.getString("NAME"));
		user.setInitials(rs.getString("INITIALS"));
		user.setShowScheduleChanges(rs.getInt("SHOW_SCHEDULE_CHANGES") == 1);
		user.setDateFormat(rs.getString("DATE_FORMAT"));
		user.setLogBeginDate(rs.getDate("LOG_BEGIN_DATE"));
		user.setUnreadMessages(rs.getInt("UNREAD_MESSAGES") == 1);
		user.setRowsPerPage(rs.getInt("ROWS_PER_PAGE"));
		user.setPhone(rs.getString("PHONE"));
		user.setMarkJobsWithPos(rs.getInt("MARK_JOBS_WITH_POS") == 1);
		user.setEmail(rs.getString("EMAIL"));
		user.setSmallTags(rs.getInt("SMALL_TAGS") == 1);
		user.setMarkJobsWithSalesRep(rs.getInt("MARK_JOBS_WITH_SALES_REP") == 1);
		user.setPass(rs.getString("PASS"));
		user.setColor(rs.getString("COLOR"));
		return user;
	}

	private static final String SELECTUSERJOBTITLES = "SELECT JOB_TITLE FROM USER_TITLES WHERE USER_ID = ? ORDER BY JOB_TITLE";

	public static LinkedList<String> getJobTitles(Connection connection, String userID) throws Exception {
		LinkedList<String> jobTitles = new LinkedList<String>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement(SELECTUSERJOBTITLES);
			select.setString(1, userID);
			rs = select.executeQuery();
			while (rs.next())
				jobTitles.add(rs.getString("JOB_TITLE"));
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return jobTitles;
	}

	public static HashSet<String> getRoles(Connection connection, String userID) throws Exception {
		HashSet<String> roles = new HashSet<String>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement("SELECT DISTINCT ROLE FROM USER_GROUPS, JOB_TITLES, USER_TITLES"
					+ " WHERE JOB_TITLES.JOB_TITLE = USER_TITLES.JOB_TITLE"
					+ " AND JOB_TITLES.JOB_TITLE = USER_GROUPS.JOB_TITLE"
					+ " AND USER_TITLES.USER_ID = ?"
					+ " ORDER BY ROLE");
			select.setString(1, userID);
			rs = select.executeQuery();
			while (rs.next())
				roles.add(rs.getString(1));
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return roles;
	}

	private static final String SELECTMONITOREDJOBS = "SELECT JOB_NUMBER FROM MONITORED_JOBS WHERE USER_ID = ?";

	public static Collection<Integer> getMonitoredJobs(Connection connection, String userID) throws Exception {
		LinkedList<Integer> monitoredJobs = new LinkedList<Integer>();
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement(SELECTMONITOREDJOBS);
			select.setString(1, userID);
			rs = select.executeQuery();
			while (rs.next())
				monitoredJobs.add(rs.getInt(1));
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return monitoredJobs;
	}

	public static Collection<String> getUsersAssignedAsSalesRep(Connection connection) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			Collection<String> userNames = new LinkedList<String>();

			select = connection.prepareStatement("SELECT DISTINCT SALESREP FROM CUSTOMERS WHERE SALESREP IS NOT NULL ORDER BY SALESREP");

			rs = select.executeQuery();
			while (rs.next())
				userNames.add(rs.getString(1));
			rs.close();
			select.close();
			return userNames;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static int deleteUser(Connection connection, int id) throws Exception {
		PreparedStatement delete = null;
		try {
			delete = connection.prepareStatement("DELETE FROM USERS WHERE ID = ?");
			delete.setInt(1, id);
			return delete.executeUpdate();
		} finally {
			try {
				delete.close();
			} catch (Exception e) {
			}
		}
	}

	public static User getUser(Connection connection, int id) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		try {
			select = connection.prepareStatement(
					"SELECT ID, USER_ID, NAME, PASS, EMAIL, DATE_FORMAT, POP_UPS, PHONE, UNREAD_MESSAGES, LOG_BEGIN_DATE, ROWS_PER_PAGE, INITIALS, MARK_JOBS_WITH_POS, COLOR, MARK_JOBS_WITH_SALES_REP, SMALL_TAGS, SHOW_SCHEDULE_CHANGES"
							+ " FROM USERS WHERE ID = ?");
			rs = select.executeQuery();
			if (!rs.next())
				throw new RuntimeException("User for id " + id + " not found");
			User user = loadUserFromResultSet(rs);
			rs.close();
			select.close();
			return user;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static UserForm getUserForm(Connection connection, String userID) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT ID, USER_ID, NAME, PASS, INITIALS, COLOR FROM USERS WHERE USER_ID = ?");
		select.setString(1, userID);
		ResultSet rs = select.executeQuery();
		if (rs.next()) {
			UserForm userForm = new UserForm(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
			rs.close();
			select.close();

			Collection<Role> allRoles = AuthorityDAO.getRoles(connection);
			Collection<String> roleNames = new LinkedList<String>();
			for (Role role : allRoles) {
				roleNames.add(role.getRole());
			}

			Collection<String> thisUserRoles = new LinkedList<String>();
			select = connection.prepareStatement("SELECT ROLE FROM USER_ROLES WHERE USER_ID = ?");
			select.setString(1, userID);
			rs = select.executeQuery();
			while (rs.next())
				thisUserRoles.add(rs.getString(1));
			rs.close();
			select.close();

			for (String role : roleNames) {
				userForm.addJobTitle(new JobTitle(role, thisUserRoles.contains(role)));
			}

			return userForm;
		}
		return null;
	}

	public static void updateProfile(Connection connection, Profile profile) throws Exception {
		PreparedStatement update = connection.prepareStatement("UPDATE USERS SET PASS = ?, EMAIL = ?, DATE_FORMAT = ?, PHONE = ?, INITIALS = ?,"
				+ " MARK_JOBS_WITH_POS = ?, MARK_JOBS_WITH_SALES_REP = ?, SMALL_TAGS = ? WHERE USER_ID = ?");
		int p = 0;
		update.setString(++p, profile.getPassword());
		update.setString(++p, profile.getEmail());
		update.setString(++p, profile.getDateDisplay());
		update.setString(++p, profile.getPhone());
		update.setString(++p, profile.getInitials());
		update.setInt(++p, profile.isMarkJobsWithPurchaseOrders() ? 1 : 0);
		update.setInt(++p, profile.isMarkJobsWithSalesRep() ? 1 : 0);
		update.setInt(++p, profile.isUseSmallControlTags() ? 1 : 0);
		update.setString(++p, profile.getUserID());
		update.executeUpdate();
		update.close();
	}
}
