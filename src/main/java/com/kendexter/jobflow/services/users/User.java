package com.kendexter.jobflow.services.users;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class User implements Serializable {
	private int id;
	private String userID;
	private String name;
	private String pass;
	private String email;
	private String dateFormat;
	private String phone;
	private boolean unreadMessages;
	private Date logBeginDate;
	private int rowsPerPage;
	private String initials;
	private boolean markJobsWithPos;
	private boolean markJobsWithSalesRep;
	private String color;
	private boolean smallTags;
	private boolean showScheduleChanges;
	private Collection<String> functions;
	private Collection<String> roles;
	
	public User() {
		setFunctions(new LinkedList<String>());
		setRoles(new LinkedList<String>());
	}
	
	public boolean hasAuthority(String function) {
		return this.getFunctions().contains(function);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isUnreadMessages() {
		return unreadMessages;
	}

	public void setUnreadMessages(boolean unreadMessages) {
		this.unreadMessages = unreadMessages;
	}

	public Date getLogBeginDate() {
		return logBeginDate;
	}

	public void setLogBeginDate(Date logBeginDate) {
		this.logBeginDate = logBeginDate;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public boolean isMarkJobsWithPos() {
		return markJobsWithPos;
	}

	public void setMarkJobsWithPos(boolean markJobsWithPos) {
		this.markJobsWithPos = markJobsWithPos;
	}

	public boolean isMarkJobsWithSalesRep() {
		return markJobsWithSalesRep;
	}

	public void setMarkJobsWithSalesRep(boolean markJobsWithSalesRep) {
		this.markJobsWithSalesRep = markJobsWithSalesRep;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isSmallTags() {
		return smallTags;
	}

	public void setSmallTags(boolean smallTags) {
		this.smallTags = smallTags;
	}

	public boolean isShowScheduleChanges() {
		return showScheduleChanges;
	}

	public void setShowScheduleChanges(boolean showScheduleChanges) {
		this.showScheduleChanges = showScheduleChanges;
	}

	public Collection<String> getFunctions() {
		return functions;
	}

	public void setFunctions(Collection<String> functions) {
		this.functions = functions;
	}
	
	public void addFunction(String function) {
		this.functions.add(function);
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public void addRole(String role) {
		this.roles.add(role);
		
	}
}
