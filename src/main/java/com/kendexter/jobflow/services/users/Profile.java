package com.kendexter.jobflow.services.users;

public class Profile {
	private String userID;
	private String password;
	private String email;
	private String phone;
	private String initials;
	private boolean markJobsWithPurchaseOrders;
	private boolean markJobsWithSalesRep;
	private boolean useSmallControlTags;
	private String dateDisplay;

	public boolean isUseSmallControlTags() {
		return useSmallControlTags;
	}

	public void setUseSmallControlTags(boolean useSmallControlTags) {
		this.useSmallControlTags = useSmallControlTags;
	}


	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public boolean isMarkJobsWithPurchaseOrders() {
		return markJobsWithPurchaseOrders;
	}

	public void setMarkJobsWithPurchaseOrders(boolean markJobsWithPurchaseOrders) {
		this.markJobsWithPurchaseOrders = markJobsWithPurchaseOrders;
	}

	public boolean isMarkJobsWithSalesRep() {
		return markJobsWithSalesRep;
	}

	public void setMarkJobsWithSalesRep(boolean markJobsWithSalesRep) {
		this.markJobsWithSalesRep = markJobsWithSalesRep;
	}

	public String getDateDisplay() {
		return dateDisplay;
	}

	public void setDateDisplay(String dateDisplay) {
		this.dateDisplay = dateDisplay;
	}	
}
