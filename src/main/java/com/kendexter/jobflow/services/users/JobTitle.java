package com.kendexter.jobflow.services.users;

public class JobTitle {
	private String role;
	private boolean authorized;

	public JobTitle() {
		this("");
	}

	public JobTitle(String role) {
		this(role, false);
	}

	public JobTitle(String role, boolean authorized) {
		this.setRole(role);
		this.setAuthorized(authorized);
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isAuthorized() {
		return authorized;
	}

	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}
}
