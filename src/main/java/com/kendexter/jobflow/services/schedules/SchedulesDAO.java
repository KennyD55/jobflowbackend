package com.kendexter.jobflow.services.schedules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.jobs.JobsDAO;
import com.kendexter.jobflow.services.users.User;

public class SchedulesDAO {

	private SchedulesDAO() {
	}

	public static Schedule getSchedule(Connection connection, String pressName) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT SCHEDULE FROM PRESSES WHERE NAME = ?");
		select.setString(1, pressName);
		ResultSet rs = select.executeQuery();
		if (!rs.next()) {
			rs.close();
			select.close();
			return null;
		}
		Schedule schedule = AppContext.MAPPER.get().readValue(rs.getString("SCHEDULE"), Schedule.class);
		for (Task task : schedule.getScheduledTasks()) {
			if (task.getJobNumber() != 0) {
				task.setJob(JobsDAO.getJobSummary(connection, task.getJobNumber()));
			}
		}
		for (Task task : schedule.getUnScheduledTasks()) {
			if (task.getJobNumber() != 0) {
				task.setJob(JobsDAO.getJobSummary(connection, task.getJobNumber()));
			}
		}
		for (Task task : schedule.getCompletedTasks()) {
			if (task.getJobNumber() != 0) {
				task.setJob(JobsDAO.getJobSummary(connection, task.getJobNumber()));
			}
		}
		for (Task task : schedule.getPreScheduledTasks()) {
			if (task.getJobNumber() != 0) {
				task.setJob(JobsDAO.getJobSummary(connection, task.getJobNumber()));
			}
		}
		rs.close();
		select.close();
		return schedule;
	}
	
	public static void persistSchedule(Connection connection, Schedule schedule, User user) throws Exception {
		PreparedStatement update = connection.prepareStatement("UPDATE PRESSES SET SCHEDULE = ? WHERE NAME = ?");
		schedule.setLastUpdate(new Timestamp(System.currentTimeMillis()));
		schedule.setUpdatedBy(user.getName());
		update.setString(1, AppContext.MAPPER.get().writeValueAsString(schedule));
		update.setString(2, schedule.getPressName());
		update.executeUpdate();
	}
}
