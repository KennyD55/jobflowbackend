package com.kendexter.jobflow.services.schedules;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Schedule {
	private String pressName;
	private boolean autoPromote;
	private String description;
	private Timestamp startTime;
	private Timestamp stopPressStartTime;
	private Timestamp stopPressEndTime;
	private String stopPressComments;
	private Timestamp lastUpdate;
	private String updatedBy;
	private ArrayList<Task> scheduledTasks;
	private ArrayList<Task> preScheduledTasks;
	private ArrayList<Task> unScheduledTasks;
	private ArrayList<Task> completedTasks;

	public Schedule() {
		this.scheduledTasks = new ArrayList<Task>();
		this.preScheduledTasks = new ArrayList<Task>();
		this.unScheduledTasks = new ArrayList<Task>();
		this.completedTasks = new ArrayList<Task>();
	}
	
	public String getPressName() {
		return pressName;
	}

	public void setPressName(String pressName) {
		this.pressName = pressName;
	}

	public boolean isAutoPromote() {
		return autoPromote;
	}

	public void setAutoPromote(boolean autoPromote) {
		this.autoPromote = autoPromote;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTimeInMillis) {
		this.startTime = startTimeInMillis;
	}

	public Timestamp getStopPressStartTime() {
		return stopPressStartTime;
	}

	public void setStopPressStartTime(Timestamp stopPressStartTime) {
		this.stopPressStartTime = stopPressStartTime;
	}

	public Timestamp getStopPressEndTime() {
		return stopPressEndTime;
	}

	public void setStopPressEndTime(Timestamp stopPressEndTime) {
		this.stopPressEndTime = stopPressEndTime;
	}

	public String getStopPressComments() {
		return stopPressComments;
	}

	public void setStopPressComments(String stopPressComments) {
		this.stopPressComments = stopPressComments;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public ArrayList<Task> getScheduledTasks() {
		return scheduledTasks;
	}

	public void addScheduledTask(Task task) {
		this.scheduledTasks.add(task);
	}
	
	public void setScheduledTasks(ArrayList<Task> scheduledTasks) {
		this.scheduledTasks = scheduledTasks;
	}

	public ArrayList<Task> getPreScheduledTasks() {
		return preScheduledTasks;
	}
	
	public void setPreScheduledTasks(ArrayList<Task> preScheduledTasks) {
		this.preScheduledTasks = preScheduledTasks;
	}

	public void addPreScheduledTask(Task task) {
		this.preScheduledTasks.add(task);
	}

	public ArrayList<Task> getUnScheduledTasks() {
		return unScheduledTasks;
	}

	public void setUnScheduledTasks(ArrayList<Task> unScheduledTasks) {
		this.unScheduledTasks = unScheduledTasks;
	}

	public void addUnScheduledTask(Task task) {
		this.unScheduledTasks.add(task);
	}
	
	public ArrayList<Task> getCompletedTasks() {
		return completedTasks;
	}

	public void setCompletedTasks(ArrayList<Task> completedTasks) {
		this.completedTasks = completedTasks;
	}
	
	public void addCompletedTask(Task task) {
		this.completedTasks.add(task);
	}
}