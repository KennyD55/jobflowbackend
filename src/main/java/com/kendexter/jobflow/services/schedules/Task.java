package com.kendexter.jobflow.services.schedules;

import java.sql.Timestamp;

import com.kendexter.Utilities;
import com.kendexter.jobflow.services.jobs.JobSummary;

public class Task implements Comparable<Task> {

	private int runTime;
	private int actualRuntime;
	private int jobNumber;
	private long taskID;
	// Status
	private String status;
	public static final String UNSCHEDULED = "UNSCHEDULED";
	public static final String PRESCHEDULED = "PRESCHEDULED";
	public static final String SCHEDULED = "SCHEDULED";
	public static final String COMPLETED = "COMPLETED";
	// Type
	private String type;
	public static final String JOBTASK = "JOBTASK";
	public static final String IDLETASK = "IDLETASK";
	// Idle Type
	private String idleType;
	public static final String SCHEDULEDIDLE = "SCHEDULEDIDLE";
	public static final String STOPPRESS = "STOPPRESS";
	public static final String HOLDIDLE = "HOLDIDLE";
	//
	private boolean dropped;
	private boolean continuation;
	public boolean unfinished;
	private String pressCheck;
	public static final String PRESSCHECKTRUE = "PRESSCHECKTRUE";
	public static final String PRESSCHECKFALSE = "PRESSCHECKFALSE";
	public String pressName; 
	private String checkTime;
	private boolean interrupted;
	private long baseTaskID;
	private int baseRunTime;
	private int seq;
	private boolean parsed;
	private boolean archived;
	private long pressItemId;
	private Timestamp startTime;
	private Timestamp endTime;
	private JobSummary job;

	public Task() {
	}

	public boolean equals(Object object) {
		Task that = (Task) object;
		return this.taskID == that.taskID;
	}

	public int compareTo(Task that) {
		return this.taskID == that.taskID ? 0 : (int) (this.taskID - that.taskID);
	}

	public Object clone() {
		Task cloneTask = new Task();
		Utilities.deepClone(this, cloneTask);
		return cloneTask;
	}

	public int getRunTime() {
		return runTime;
	}

	public void setRunTime(int runTime) {
		this.runTime = runTime;
	}

	public int getActualRuntime() {
		return actualRuntime;
	}

	public void setActualRuntime(int actualRuntime) {
		this.actualRuntime = actualRuntime;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public long getTaskID() {
		return taskID;
	}

	public void setTaskID(long taskID) {
		this.taskID = taskID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdleType() {
		return idleType;
	}

	public void setIdleType(String idleType) {
		this.idleType = idleType;
	}

	public boolean isDropped() {
		return dropped;
	}

	public void setDropped(boolean dropped) {
		this.dropped = dropped;
	}

	public boolean isContinuation() {
		return continuation;
	}

	public void setContinuation(boolean continuation) {
		this.continuation = continuation;
	}

	public boolean isUnfinished() {
		return unfinished;
	}

	public void setUnfinished(boolean unfinished) {
		this.unfinished = unfinished;
	}

	public String getPressCheck() {
		return pressCheck;
	}

	public void setPressCheck(String pressCheck) {
		this.pressCheck = pressCheck;
	}

	public String getPressName() {
		return pressName;
	}

	public void setPressName(String pressName) {
		this.pressName = pressName;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public boolean isInterrupted() {
		return interrupted;
	}

	public void setInterrupted(boolean interrupted) {
		this.interrupted = interrupted;
	}

	public long getBaseTaskID() {
		return baseTaskID;
	}

	public void setBaseTaskID(long baseTaskID) {
		this.baseTaskID = baseTaskID;
	}

	public int getBaseRunTime() {
		return baseRunTime;
	}

	public void setBaseRunTime(int baseRunTime) {
		this.baseRunTime = baseRunTime;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public boolean isParsed() {
		return parsed;
	}

	public void setParsed(boolean parsed) {
		this.parsed = parsed;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public long getPressItemId() {
		return pressItemId;
	}

	public void setPressItemId(long pressItemId) {
		this.pressItemId = pressItemId;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public JobSummary getJob() {
		return job;
	}

	public void setJob(JobSummary job) {
		this.job = job;
	}
}
