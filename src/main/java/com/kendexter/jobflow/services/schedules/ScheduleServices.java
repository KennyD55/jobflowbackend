package com.kendexter.jobflow.services.schedules;

import java.sql.Connection;
import java.util.ArrayList;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

@Path("schedules")
public class ScheduleServices {
	private static Logger logger = Logger.getLogger(ScheduleServices.class);
	private @Context ServletContext context;
	private static Object mutex = new Object();

	@Path("getSchedule")
	@GET
	@RolesAllowed({ AppContext.SCHEDULE_LEVEL_II, AppContext.SCHEDULE_LEVEL_III })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSchedule(@QueryParam("pressName") String pressName) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Object retObject = getSchedule(connection, pressName, null);
			if (retObject instanceof Response)
				return (Response) retObject;

			Schedule schedule = (Schedule) retObject;
			return Response.ok().entity(schedule).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("processSchedule")
	@POST
	@RolesAllowed({ AppContext.SCHEDULE_LEVEL_II, AppContext.SCHEDULE_LEVEL_III })
	@Produces(MediaType.APPLICATION_JSON)
	public Response processSchedule(@QueryParam("pressName") String pressName, @QueryParam("taskID") long taskID, @QueryParam("lastUpdate") long lastUpdate,
			@QueryParam("command") String command, @Context HttpServletRequest request) {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				Object retObject = getSchedule(connection, pressName, lastUpdate);
				if (retObject instanceof Response)
					return (Response) retObject;

				Schedule schedule = (Schedule) retObject;

				if ("moveUp".equals(command)) {
					moveUp(schedule, taskID);
				} else if ("moveDown".equals(command)) {
					moveDown(schedule, taskID);
				} else if ("unschedule".equals(command)) {
					unschedule(schedule, taskID);
				} else if ("schedule".equals(command)) {
					schedule(schedule, taskID);
				} else {
					throw new Exception("Command " + command + " not understood");
				}

				SchedulesDAO.persistSchedule(connection, schedule, ((User) request.getAttribute("user")));
				connection.commit();
				return Response.ok().entity(schedule).build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}

	}

	public void moveUp(Schedule schedule, long taskID) throws Exception {
		synchronized (mutex) {
			ArrayList<Task> tasks = schedule.getScheduledTasks();
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				if (task.getTaskID() == taskID) {
					tasks.remove(i);
					tasks.add(i - 1, task);
					break;
				}
			}
		}
	}

	public void moveDown(Schedule schedule, long taskID) {
		ArrayList<Task> tasks = schedule.getScheduledTasks();
		for (int i = 0; i < tasks.size(); i++) {
			Task task = tasks.get(i);
			if (task.getTaskID() == taskID) {
				tasks.remove(i);
				tasks.add(i + 1, task);
				break;
			}
		}

	}

	private void unschedule(Schedule schedule, long taskID) throws Exception {
		ArrayList<Task> tasks = schedule.getScheduledTasks();
		for (int i = 0; i < tasks.size(); i++) {
			Task task = tasks.get(i);
			if (task.getTaskID() == taskID) {
				tasks.remove(i);
				schedule.getUnScheduledTasks().add(task);
				break;
			}
		}
	}

	private void schedule(Schedule schedule, long taskID) throws Exception {
		ArrayList<Task> tasks = schedule.getUnScheduledTasks();
		for (int i = 0; i < tasks.size(); i++) {
			Task task = tasks.get(i);
			if (task.getTaskID() == taskID) {
				tasks.remove(i);
				schedule.getScheduledTasks().add(task);
				break;
			}
		}
	}

	Object getSchedule(Connection connection, String pressName, Long lastUpdate) throws Exception {
		Schedule schedule = SchedulesDAO.getSchedule(connection, pressName);
		if (schedule == null) {
			return Response.serverError().entity("{\"error\":\"Schedule not found\"}").build();
		}
		if (lastUpdate != null && lastUpdate != schedule.getLastUpdate().getTime()) {
			return Response.ok()
					.entity("{\"error\":\"Schedule previously updated\", \"updatedBy\": \"" + StringEscapeUtils.escapeJson(schedule.getUpdatedBy()) + "\"}").build();
		}
		return schedule;
	}
}