package com.kendexter.jobflow.services.admin.authority;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;

public class AuthorityDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(AuthorityDAO.class);

	private AuthorityDAO() {
	}

	public static synchronized int deleteRole(Connection connection, String role) throws Exception {
		PreparedStatement delete = null;
		try {

			delete = connection.prepareStatement("DELETE FROM ROLES WHERE ROLE = ?");
			delete.setString(1, role);
			return delete.executeUpdate();

		} finally {
			try {
				delete.close();
			} catch (Exception e) {
			}
		}
	}

	public static Collection<Role> getRoles(Connection connection) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		Collection<Role> roles = new LinkedList<Role>();
		try {
			select = connection.prepareStatement("SELECT ID, ROLE FROM ROLES ORDER BY ROLE");
			rs = select.executeQuery();
			while (rs.next())
				roles.add(new Role(rs.getInt("ID"), rs.getString("ROLE")));
			rs.close();
			select.close();
			return roles;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static Collection<String> getFunctions(Connection connection, String role) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		Collection<String> roles = new LinkedList<String>();
		try {

			roles = new LinkedList<String>();
			select = connection.prepareStatement("SELECT FUNCTION FROM FUNCTIONS_AUTHORIZED WHERE ROLE = ?");
			select.setString(1, role);
			rs = select.executeQuery();
			while (rs.next())
				roles.add(rs.getString("FUNCTION"));
			rs.close();
			select.close();
			return roles;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static boolean areThereAnyAdminRolesLeft(Connection connection) throws Exception {
		PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM FUNCTIONS_AUTHORIZED WHERE FUNCTION = 'Administration'");
		ResultSet rs = select.executeQuery();
		rs.next();
		boolean ret = rs.getInt(1) > 0;
		rs.close();
		select.close();
		return ret;
	}

	@SuppressWarnings("serial") private static final LinkedHashMap<String, String> ISMETHODMAP = new LinkedHashMap<String, String>() {
		{
			put(AppContext.ADMINISTRATION, "isAdministration");
			put(AppContext.ENTER_JOBS, "isEnterJobs");
			put(AppContext.MAINTAIN_JOBS, "isMaintainJobs");
			put(AppContext.VIEW_JOBS, "isViewJobs");
			put(AppContext.MAINTAIN_ALL_CUSTOMERS, "isMaintainAllCustomers");
			put(AppContext.MAINTAIN_MY_CUSTOMERS, "isMaintainMyCustomers");
			put(AppContext.PRESSMAN, "isPressman");
			put(AppContext.ADD_CUSTOMER, "isAddCustomer");
			put(AppContext.ENTER_JOB_ESTIMATES, "isEnterJobEstimates");
			put(AppContext.COD, "isCod");
			put(AppContext.REPORTS, "isReports");
			put(AppContext.ARCHIVE, "isArchive");
			put(AppContext.PROMOTE, "isPromote");
			put(AppContext.RESET, "isReset");
			put(AppContext.HOT, "isHot");
			put(AppContext.ENTER_PURCHASE_ORDERS, "isEnterPurchaseOrders");
			put(AppContext.APPROVE_PURCHASE_ORDERS, "isApprovePurchaseOrders");
			put(AppContext.STOCK, "isStock");
			put(AppContext.RESET_ALL, "isResetAll");
			put(AppContext.SCHEDULE_LEVEL_I, "isScheduleLevelI");
			put(AppContext.SCHEDULE_LEVEL_II, "isScheduleLevelII");
			put(AppContext.SCHEDULE_LEVEL_III, "isScheduleLevelIII");
			put(AppContext.BACKUP_JOBS, "isBackupJobs");
			put(AppContext.DELETE_JOBS, "isDeleteJobs");
			put(AppContext.RESTRICTED_STATUS, "isRestrictedStatus");
			put(AppContext.VIEW_MAIL_JOBS, "isViewMailJobs");
		}
	};

	public static synchronized void upsertRole(Connection connection, RolesForm form) throws Exception {
		PreparedStatement upsert = connection.prepareStatement("INSERT INTO ROLES (ID, ROLE) VALUES(?, ?) ON DUPLICATE KEY UPDATE ROLE = ?");
		upsert.setInt(1, form.getId());
		upsert.setString(2, form.getRole());
		upsert.setString(3, form.getRole());
		upsert.executeUpdate();
		upsert.close();
		PreparedStatement delete = connection.prepareStatement("DELETE FROM FUNCTIONS_AUTHORIZED WHERE ROLE = ?");
		delete.setString(1, form.getRole());
		delete.executeUpdate();
		delete.close();
		PreparedStatement insert = connection.prepareStatement("INSERT INTO FUNCTIONS_AUTHORIZED (ROLE, FUNCTION) VALUES (?, ?)");
		for (String function : AppContext.ALL_FUNCTIONS) {
			Method method = form.getClass().getMethod(ISMETHODMAP.get(function), new Class[] {});
			boolean authorized = (boolean) method.invoke(form, new Object[] {});
			if (authorized) {
				insert.setString(1, form.getRole());
				insert.setString(2, function);
				insert.executeUpdate();
			}

		}
	}
}
