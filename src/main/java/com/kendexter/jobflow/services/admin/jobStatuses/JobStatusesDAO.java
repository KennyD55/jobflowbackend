package com.kendexter.jobflow.services.admin.jobStatuses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class JobStatusesDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(JobStatusesDAO.class);

	private JobStatusesDAO() {
	}

	public static Collection<JobStatus> getJobStatuses(Connection connection) throws Exception {

		Collection<JobStatus> jobStatuses = new LinkedList<JobStatus>();

		LinkedList<String> activeStatuses = new LinkedList<String>();
		PreparedStatement select = connection.prepareStatement("SELECT DISTINCT STATUS FROM JOBS");
		ResultSet rs = select.executeQuery();
		while (rs.next())
			activeStatuses.add(rs.getString("STATUS"));
		rs.close();
		select.close();

		select = connection
				.prepareStatement("SELECT ID, STATUS, SEQ, DEFAULT_PROMOTION_STATUS, JEOPARDY_HOURS, RESTRICTED, ARCHIVE_DAYS, READY_FOR_BACKUP, AUTO_ARCHIVE"
						+ " FROM JOB_STATUSES ORDER BY SEQ");
		rs = select.executeQuery();
		while (rs.next()) {
			JobStatus jobStatus = new JobStatus();
			jobStatus.setId(rs.getInt("ID"));
			jobStatus.setStatus(rs.getString("STATUS"));
			jobStatus.setSeq(rs.getInt("SEQ"));
			jobStatus.setDefaultPromotionStatus(rs.getString("DEFAULT_PROMOTION_STATUS"));
			jobStatus.setJeopardyHours(rs.getInt("JEOPARDY_HOURS"));
			jobStatus.setRestricted(rs.getInt("RESTRICTED") == 1);
			jobStatus.setArchiveDays(rs.getInt("ARCHIVE_DAYS"));
			jobStatus.setReadyForBackup(rs.getInt("READY_FOR_BACKUP") == 1);
			jobStatus.setAutoArchive(rs.getInt("AUTO_ARCHIVE") == 1);
			jobStatus.setDeleteAble(!activeStatuses.contains(jobStatus.getStatus()));
			jobStatuses.add(jobStatus);
		}
		rs.close();
		select.close();
		return jobStatuses;
	}

	public static synchronized int upsertJobStatus(Connection connection, JobStatus jobStatus) throws Exception {
		PreparedStatement upsert = connection.prepareStatement("REPLACE INTO JOB_STATUSES (ID, STATUS, SEQ, DEFAULT_PROMOTION_STATUS, JEOPARDY_HOURS,"
				+ " RESTRICTED, ARCHIVE_DAYS, READY_FOR_BACKUP, AUTO_ARCHIVE)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		int p = 0;
		/* Insert */
		upsert.setInt(++p, jobStatus.getId());
		upsert.setString(++p, jobStatus.getStatus());
		upsert.setInt(++p, jobStatus.getSeq());
		upsert.setString(++p, jobStatus.getDefaultPromotionStatus());
		upsert.setInt(++p, jobStatus.getJeopardyHours());
		upsert.setInt(++p, jobStatus.isRestricted() ? 1 : 0);
		upsert.setInt(++p, jobStatus.getArchiveDays());
		upsert.setInt(++p, jobStatus.isReadyForBackup() ? 1 : 0);
		upsert.setInt(++p, jobStatus.isAutoArchive() ? 1 : 0);
		int count = upsert.executeUpdate();
		upsert.close();
		return count;
	}

	public static synchronized int deleteJobStatus(Connection connection, int id) throws Exception {
		PreparedStatement delete = connection.prepareStatement("DELETE FROM JOB_STATUSES WHERE ID = ?");
		delete.setInt(1, id);
		int count = delete.executeUpdate();
		delete.close();
		return count;
	}

	public static Collection<JobStatusMonitorProfile> getJobStatusMonitorProfile(Connection connection, String userID) throws Exception {
		Collection<JobStatus> jobStatuses = getJobStatuses(connection);
		Collection<JobStatusMonitorProfile> profiles = new LinkedList<JobStatusMonitorProfile>();
		LinkedList<String> userMonitoredStatuses = new LinkedList<String>();
		PreparedStatement select = connection.prepareStatement("SELECT STATUS FROM MONITOR_PROFILE WHERE USER_ID = ?");
		select.setString(1, userID);
		ResultSet rs = select.executeQuery();
		while (rs.next())
			userMonitoredStatuses.add(rs.getString("STATUS"));
		rs.close();
		select.close();
		for (JobStatus jobStatus : jobStatuses) {
			JobStatusMonitorProfile profile = new JobStatusMonitorProfile();
			profile.setStatus(jobStatus.getStatus());
			profile.setMonitored(userMonitoredStatuses.contains(jobStatus.getStatus()));
			profiles.add(profile);
		}
		return profiles;
	}

	public static synchronized int updateMonitoredStatuses(Connection connection, String userID, Collection<JobStatusMonitorProfile> profiles) throws Exception {
		PreparedStatement delete = connection.prepareStatement("DELETE FROM MONITOR_PROFILE WHERE USER_ID = ?");
		delete.setString(1, userID);
		delete.executeUpdate();
		delete.close();
		int count = 0;
		PreparedStatement insert = connection.prepareStatement("INSERT INTO MONITOR_PROFILE (USER_ID, STATUS) VALUE (?, ?)");
		insert.setString(1, userID);
		for (JobStatusMonitorProfile profile : profiles) {
			if (profile.monitored) {
				insert.setString(2, profile.status);
				count += insert.executeUpdate();
			}
		}
		insert.close();
		return count;
	}
}
