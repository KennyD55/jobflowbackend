package com.kendexter.jobflow.services.admin.stockVendors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class StockVendorServices {
	private static Logger logger = Logger.getLogger(StockVendorServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;
	private static Object mutex = new Object();

	@Path("getStockVendors")
	@GET
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStockVendors() {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement select = connection.prepareStatement("SELECT VENDOR_NAME FROM STOCK_VENDORS ORDER BY VENDOR_NAME");
				LinkedList<StockVendor> vendorNames = new LinkedList<StockVendor>();
				ResultSet rs = select.executeQuery();
				while (rs.next())
					vendorNames.add(new StockVendor(rs.getString("VENDOR_NAME")));
				rs.close();
				select.close();
				return Response.ok().entity(vendorNames).build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("deleteStockVendor")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteStockVendor(StockVendor stockVendor) {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement delete = connection.prepareStatement("DELETE FROM STOCK_VENDORS WHERE VENDOR_NAME = ?");
				delete.setString(1, stockVendor.getVendorName());
				delete.executeUpdate();
				delete.close();
				connection.commit();
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("addStockVendor")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addStockVendor(StockVendor stockVendor) {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement insert = connection.prepareStatement("INSERT INTO STOCK_VENDORS (VENDOR_NAME) VALUES(?)");
				insert.setString(1, stockVendor.getVendorName());
				insert.executeUpdate();
				insert.close();
				connection.commit();
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}
}
