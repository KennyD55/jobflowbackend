package com.kendexter.jobflow.services.admin.departments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class DepartmentsDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(DepartmentsDAO.class);

	private DepartmentsDAO() {
	}

	public static Collection<Department> getDepartments(Connection connection) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		Collection<Department> departments = new LinkedList<Department>();
		try {

			select = connection.prepareStatement("SELECT ID, SEQ, NAME, COLOR FROM DEPARTMENTS ORDER BY SEQ");
			rs = select.executeQuery();
			while (rs.next())
				departments.add(new Department(rs.getInt("ID"), rs.getInt("SEQ"), rs.getString("NAME"), rs.getString("COLOR")));
			rs.close();
			select.close();
			return departments;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static synchronized int upsertDepartment(Connection connection, Department department) throws Exception {
		PreparedStatement upsert = null;
		try {
			upsert = connection.prepareStatement(
					"INSERT INTO DEPARTMENTS (ID, SEQ, NAME, COLOR)" + " VALUES (?, ?, ?, ?)" + " ON DUPLICATE KEY UPDATE SEQ= ?, NAME= ?, COLOR= ?");

			int p = 0;
			/* Insert */
			upsert.setInt(++p, department.getId());
			upsert.setInt(++p, department.getSeq());
			upsert.setString(++p, department.getName());
			upsert.setString(++p, department.getColor());

			/* Update */
			upsert.setInt(++p, department.getSeq());
			upsert.setString(++p, department.getName());
			upsert.setString(++p, department.getColor());
			return upsert.executeUpdate();
		} finally {
			try {
				upsert.close();
			} catch (Exception e) {
			}
		}
	}

	public static synchronized int deleteDepartment(Connection connection, int id) throws Exception {
		PreparedStatement delete = null;
		try {

			delete = connection.prepareStatement("DELETE FROM DEPARTMENTS WHERE ID = ?");
			delete.setInt(1, id);
			return delete.executeUpdate();

		} finally {
			try {
				delete.close();
			} catch (Exception e) {
			}
		}
	}

}
