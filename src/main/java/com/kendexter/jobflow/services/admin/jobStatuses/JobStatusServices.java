package com.kendexter.jobflow.services.admin.jobStatuses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class JobStatusServices {
	private static Logger logger = Logger.getLogger(JobStatusServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getJobStatuses")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJobStatuses() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			return Response.ok().entity(JobStatusesDAO.getJobStatuses(connection)).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertJobStatus")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertJobStatus(JobStatus jobStatus) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			int count = JobStatusesDAO.upsertJobStatus(connection, jobStatus);
			connection.commit();
			return Response.ok().entity("{\"count\": \"" + count + "\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyJobStatus")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkUserName(@QueryParam("id") int id, @QueryParam("status") String status) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM JOB_STATUSES WHERE STATUS = ? AND ID != ?");
			select.setString(1, status);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null : "User name " + status + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deleteJobStatus")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteJobStatus(@QueryParam("id") int id) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			JobStatusesDAO.deleteJobStatus(connection, id);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getMonitoredStatuses")
	@PermitAll
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMonitoredStatuses(@QueryParam("userID") String userID) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<JobStatusMonitorProfile> profiles = JobStatusesDAO.getJobStatusMonitorProfile(connection, userID);
			return Response.ok().entity(profiles).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("updateMonitoredStatuses")
	@PermitAll
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMonitoredStatuses(Collection<JobStatusMonitorProfile> profiles, @QueryParam("userID") String userID) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			JobStatusesDAO.updateMonitoredStatuses(connection, userID, profiles);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
