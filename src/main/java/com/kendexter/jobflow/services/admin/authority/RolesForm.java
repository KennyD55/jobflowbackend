package com.kendexter.jobflow.services.admin.authority;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;

@SuppressWarnings("serial")
public class RolesForm {
	private static Logger logger = Logger.getLogger(RolesForm.class);
	private int id;
	private String role;
	private boolean addCustomer;
	private boolean administration;
	private boolean approvePurchaseOrders;
	private boolean archive;
	private boolean backupJobs;
	private boolean cod;
	private boolean deleteJobs;
	private boolean enterJobEstimates;
	private boolean enterJobs;
	private boolean enterPurchaseOrders;
	private boolean hot;
	private boolean maintainAllCustomers;
	private boolean maintainJobs;
	private boolean maintainMyCustomers;
	private boolean pressman;
	private boolean promote;
	private boolean reports;
	private boolean reset;
	private boolean resetAll;
	private boolean restrictedStatus;
	private boolean scheduleLevelI;
	private boolean scheduleLevelII;
	private boolean scheduleLevelIII;
	private boolean stock;
	private boolean viewJobs;
	private boolean viewMailJobs;
	private static final LinkedHashMap<String, String> SETMETHODMAP = new LinkedHashMap<String, String>() {
		{
			put(AppContext.ADMINISTRATION, "setAdministration");
			put(AppContext.ENTER_JOBS, "setEnterJobs");
			put(AppContext.MAINTAIN_JOBS, "setMaintainJobs");
			put(AppContext.VIEW_JOBS, "setViewJobs");
			put(AppContext.MAINTAIN_ALL_CUSTOMERS, "setMaintainAllCustomers");
			put(AppContext.MAINTAIN_MY_CUSTOMERS, "setMaintainMyCustomers");
			put(AppContext.PRESSMAN, "setPressman");
			put(AppContext.ADD_CUSTOMER, "setAddCustomer");
			put(AppContext.ENTER_JOB_ESTIMATES, "setEnterJobEstimates");
			put(AppContext.COD, "setCod");
			put(AppContext.REPORTS, "setReports");
			put(AppContext.ARCHIVE, "setArchive");
			put(AppContext.PROMOTE, "setPromote");
			put(AppContext.RESET, "setReset");
			put(AppContext.HOT, "setHot");
			put(AppContext.ENTER_PURCHASE_ORDERS, "setEnterPurchaseOrders");
			put(AppContext.APPROVE_PURCHASE_ORDERS, "setApprovePurchaseOrders");
			put(AppContext.STOCK, "setStock");
			put(AppContext.RESET_ALL, "setResetAll");
			put(AppContext.SCHEDULE_LEVEL_I, "setScheduleLevelI");
			put(AppContext.SCHEDULE_LEVEL_II, "setScheduleLevelII");
			put(AppContext.SCHEDULE_LEVEL_III, "setScheduleLevelIII");
			put(AppContext.BACKUP_JOBS, "setBackupJobs");
			put(AppContext.DELETE_JOBS, "setDeleteJobs");
			put(AppContext.RESTRICTED_STATUS, "setRestrictedStatus");
			put(AppContext.VIEW_MAIL_JOBS, "setViewMailJobs");
		}
	};

	public RolesForm() {

	}

	public RolesForm(UserRole userRole) {
		setId(userRole.getId());
		setRole(userRole.getRole());
		for (Function function : userRole.getFunctions()) {
			Method method;
			try {
				method = this.getClass().getMethod(SETMETHODMAP.get(function.getName()), Boolean.TYPE);
				method.invoke(this, function.isAuthorized());
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRole() {
		return role == null ? "" : role.trim();
	}

	public void setRole(String role) {
		this.role = role == null ? "" : role.trim();
	}

	public boolean isAddCustomer() {
		return addCustomer;
	}

	public void setAddCustomer(boolean addCustomer) {
		this.addCustomer = addCustomer;
	}

	public boolean isAdministration() {
		return administration;
	}

	public void setAdministration(boolean administration) {
		this.administration = administration;
	}

	public boolean isApprovePurchaseOrders() {
		return approvePurchaseOrders;
	}

	public void setApprovePurchaseOrders(boolean approvePurchaseOrders) {
		this.approvePurchaseOrders = approvePurchaseOrders;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public boolean isBackupJobs() {
		return backupJobs;
	}

	public void setBackupJobs(boolean backupJobs) {
		this.backupJobs = backupJobs;
	}

	public boolean isCod() {
		return cod;
	}

	public void setCod(boolean cod) {
		this.cod = cod;
	}

	public boolean isDeleteJobs() {
		return deleteJobs;
	}

	public void setDeleteJobs(boolean deleteJobs) {
		this.deleteJobs = deleteJobs;
	}

	public boolean isEnterJobEstimates() {
		return enterJobEstimates;
	}

	public void setEnterJobEstimates(boolean enterJobEstimates) {
		this.enterJobEstimates = enterJobEstimates;
	}

	public boolean isEnterJobs() {
		return enterJobs;
	}

	public void setEnterJobs(boolean enterJobs) {
		this.enterJobs = enterJobs;
	}

	public boolean isEnterPurchaseOrders() {
		return enterPurchaseOrders;
	}

	public void setEnterPurchaseOrders(boolean enterPurchaseOrders) {
		this.enterPurchaseOrders = enterPurchaseOrders;
	}

	public boolean isHot() {
		return hot;
	}

	public void setHot(boolean hot) {
		this.hot = hot;
	}

	public boolean isMaintainAllCustomers() {
		return maintainAllCustomers;
	}

	public void setMaintainAllCustomers(boolean maintainAllCustomers) {
		this.maintainAllCustomers = maintainAllCustomers;
	}

	public boolean isMaintainJobs() {
		return maintainJobs;
	}

	public void setMaintainJobs(boolean maintainJobs) {
		this.maintainJobs = maintainJobs;
	}

	public boolean isMaintainMyCustomers() {
		return maintainMyCustomers;
	}

	public void setMaintainMyCustomers(boolean maintainMyCustomers) {
		this.maintainMyCustomers = maintainMyCustomers;
	}

	public boolean isPressman() {
		return pressman;
	}

	public void setPressman(boolean pressman) {
		this.pressman = pressman;
	}

	public boolean isPromote() {
		return promote;
	}

	public void setPromote(boolean promote) {
		this.promote = promote;
	}

	public boolean isReports() {
		return reports;
	}

	public void setReports(boolean reports) {
		this.reports = reports;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	public boolean isResetAll() {
		return resetAll;
	}

	public void setResetAll(boolean resetAll) {
		this.resetAll = resetAll;
	}

	public boolean isRestrictedStatus() {
		return restrictedStatus;
	}

	public void setRestrictedStatus(boolean restrictedStatus) {
		this.restrictedStatus = restrictedStatus;
	}

	public boolean isScheduleLevelI() {
		return scheduleLevelI;
	}

	public void setScheduleLevelI(boolean scheduleLevelI) {
		this.scheduleLevelI = scheduleLevelI;
	}

	public boolean isScheduleLevelII() {
		return scheduleLevelII;
	}

	public void setScheduleLevelII(boolean scheduleLevelII) {
		this.scheduleLevelII = scheduleLevelII;
	}

	public boolean isScheduleLevelIII() {
		return scheduleLevelIII;
	}

	public void setScheduleLevelIII(boolean scheduleLevelIII) {
		this.scheduleLevelIII = scheduleLevelIII;
	}

	public boolean isStock() {
		return stock;
	}

	public void setStock(boolean stock) {
		this.stock = stock;
	}

	public boolean isViewJobs() {
		return viewJobs;
	}

	public void setViewJobs(boolean viewJobs) {
		this.viewJobs = viewJobs;
	}

	public boolean isViewMailJobs() {
		return viewMailJobs;
	}

	public void setViewMailJobs(boolean viewMailJobs) {
		this.viewMailJobs = viewMailJobs;
	}
}
