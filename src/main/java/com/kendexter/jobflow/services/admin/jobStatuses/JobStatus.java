package com.kendexter.jobflow.services.admin.jobStatuses;

import java.io.Serializable;

@SuppressWarnings("serial")
public class JobStatus implements Serializable {
	private int id;
	private String status;
	private int seq;
	private String defaultPromotionStatus;
	private int jeopardyHours;
	private boolean restricted;
	private int archiveDays;
	private boolean autoArchive;
	private boolean readyForBackup;
	private boolean deleteAble;

	public JobStatus() {

	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public boolean isAutoArchive() {
		return autoArchive;
	}

	public void setAutoArchive(boolean autoArchive) {
		this.autoArchive = autoArchive;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getDefaultPromotionStatus() {
		return this.defaultPromotionStatus;
	}

	public void setDefaultPromotionStatus(String defaultPromotionStatus) {
		this.defaultPromotionStatus = defaultPromotionStatus;
	}

	public int getJeopardyHours() {
		return this.jeopardyHours;
	}

	public void setJeopardyHours(int jeopardyHours) {
		this.jeopardyHours = jeopardyHours;
	}

	public boolean isRestricted() {
		return this.restricted;
	}

	public void setRestricted(boolean restricted) {
		this.restricted = restricted;
	}

	public int getArchiveDays() {
		return this.archiveDays;
	}

	public void setArchiveDays(int archiveDays) {
		this.archiveDays = archiveDays;
	}

	public boolean isReadyForBackup() {
		return this.readyForBackup;
	}

	public void setReadyForBackup(boolean readyForBackup) {
		this.readyForBackup = readyForBackup;
	}

	public boolean isDeleteAble() {
		return deleteAble;
	}

	public void setDeleteAble(boolean deleteAble) {
		this.deleteAble = deleteAble;
	}
}
