package com.kendexter.jobflow.services.admin.parameters;

import java.sql.Connection;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class ParameterServices {
	private static Logger logger = Logger.getLogger(ParameterServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getParameter")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParameter(@QueryParam("name") String name) {
		String value = AppContext.getParameterValue(name);
		String resp = "{\"" + name + "\":\"" + value + "\"}";
		return Response.ok().entity("{\"value\":\"" + StringEscapeUtils.escapeJson(value) + "\"}").build();
	}
	
	@Path("getSetupParameters")
	@GET
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSetupParameters() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			return Response.ok().entity(ParametersDAO.getFormParameters(connection)).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("updateParameters")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateParameters(ParametersForm form) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			int count = ParametersDAO.updateParameters(conn, form);
			conn.commit();
			AppContext.loadParameters();
			return Response.ok().entity("{\"count\": \"" + count + "\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
