package com.kendexter.jobflow.services.admin.jobStatuses;

public class JobStatusMonitorProfile {
	String status;
	boolean monitored;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isMonitored() {
		return monitored;
	}

	public void setMonitored(boolean monitored) {
		this.monitored = monitored;
	}
}
