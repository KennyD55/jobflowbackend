package com.kendexter.jobflow.services.admin.presses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class PressesDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(PressesDAO.class);

	private PressesDAO() {
	}

	public static Collection<Press> getPresses(Connection connection) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		Collection<Press> presses = new LinkedList<Press>();
		try {

			LinkedList<String> activeStatuses = new LinkedList<String>();
			select = connection.prepareStatement("SELECT DISTINCT STATUS FROM JOBS");
			rs = select.executeQuery();
			while (rs.next())
				activeStatuses.add(rs.getString("STATUS"));
			rs.close();
			select.close();

			select = connection.prepareStatement("SELECT ID, NAME, DESCRIPTION, SEQ, AUTO_PROMOTE, RATE, DIGITAL" + " FROM PRESSES ORDER BY SEQ");
			rs = select.executeQuery();
			while (rs.next()) {
				Press press = new Press();
				press.setId(rs.getInt("ID"));
				press.setName(rs.getString("NAME"));
				press.setDescription(rs.getString("DESCRIPTION"));
				press.setSeq(rs.getInt("SEQ"));
				press.setAutoPromote(rs.getInt("AUTO_PROMOTE") == 1);
				press.setRate(rs.getDouble("RATE"));
				press.setDigital(rs.getInt("DIGITAL") == 1);
				presses.add(press);
			}
			return presses;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static synchronized int upsertPress(Connection connection, Press press) throws Exception {
		PreparedStatement upsert = null;
		try {
			upsert = connection.prepareStatement("REPLACE INTO PRESSES(NAME, DESCRIPTION, SEQ, AUTO_PROMOTE, RATE, DIGITAL)" + 
					" VALUES (?, ?, ?, ?, ?, ?)");
			int p = 0;

			upsert.setString(++p, press.getName());
			upsert.setString(++p, press.getDescription());
			upsert.setInt(++p, press.getSeq());
			upsert.setInt(++p, press.isAutoPromote() ? 1 : 0);
			upsert.setDouble(++p, press.getRate());
			upsert.setInt(++p, press.isDigital() ? 1 : 0);

			return upsert.executeUpdate();
		} finally {
			try {
				upsert.close();
			} catch (Exception e) {
			}
		}
	}

	public static synchronized int deletePress(Connection connection, int id) throws Exception {
		PreparedStatement delete = null;
		try {

			delete = connection.prepareStatement("DELETE FROM PRESSES WHERE ID = ?");
			delete.setInt(1, id);
			return delete.executeUpdate();

		} finally {
			try {
				delete.close();
			} catch (Exception e) {
			}
		}
	}

}
