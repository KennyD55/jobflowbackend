package com.kendexter.jobflow.services.admin.presses;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Press implements Serializable {
	private int id;
	private String name;
	private String description;
	private int seq;
	private boolean autoPromote;
	private double rate;
	private boolean digital;
	private String schedule;

	public Press() {
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public boolean isAutoPromote() {
		return this.autoPromote;
	}

	public void setAutoPromote(boolean autoPromote) {
		this.autoPromote = autoPromote;
	}

	public double getRate() {
		return this.rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public boolean isDigital() {
		return this.digital;
	}

	public void setDigital(boolean digital) {
		this.digital = digital;
	}

	public String getSchedule() {
		return this.schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
}
