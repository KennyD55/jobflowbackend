package com.kendexter.jobflow.services.admin.parameters;

public class ParametersForm {
	private String companyCode;
	private String nextJobNumber;
	private String nextAccountNumber;
	private String nextPoNumber;
	private String ceilingLimit;
	private String timeZone;
	private String pressStartTimeH;
	private String pressStartTimeM;
	private String pressStartTimeAMPM;
	private String schedulePromoteStatus;
	private String shipToAddress;
	private String billToAddress;
	private String timeoutInMinutes;

	public ParametersForm() {
	}
	
	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getNextJobNumber() {
		return nextJobNumber;
	}

	public void setNextJobNumber(String nextJobNumber) {
		this.nextJobNumber = nextJobNumber;
	}

	public String getNextAccountNumber() {
		return nextAccountNumber;
	}

	public void setNextAccountNumber(String nextAccountNumber) {
		this.nextAccountNumber = nextAccountNumber;
	}

	public String getNextPoNumber() {
		return nextPoNumber;
	}

	public void setNextPoNumber(String nextPoNumber) {
		this.nextPoNumber = nextPoNumber;
	}

	public String getCeilingLimit() {
		return ceilingLimit;
	}

	public void setCeilingLimit(String ceilingLimit) {
		this.ceilingLimit = ceilingLimit;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getPressStartTimeH() {
		return pressStartTimeH;
	}

	public void setPressStartTimeH(String pressStartTimeH) {
		this.pressStartTimeH = pressStartTimeH;
	}

	public String getPressStartTimeM() {
		return pressStartTimeM;
	}

	public void setPressStartTimeM(String pressStartTimeM) {
		this.pressStartTimeM = pressStartTimeM;
	}

	public String getPressStartTimeAMPM() {
		return pressStartTimeAMPM;
	}

	public void setPressStartTimeAMPM(String pressStartTimeAMPM) {
		this.pressStartTimeAMPM = pressStartTimeAMPM;
	}

	public String getSchedulePromoteStatus() {
		return schedulePromoteStatus;
	}

	public void setSchedulePromoteStatus(String schedulePromoteStatus) {
		this.schedulePromoteStatus = schedulePromoteStatus;
	}

	public String getShipToAddress() {
		return shipToAddress;
	}

	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}

	public String getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}

	public String getTimeoutInMinutes() {
		return timeoutInMinutes;
	}

	public void setTimeoutInMinutes(String timeoutInMinutes) {
		this.timeoutInMinutes = timeoutInMinutes;
	}
}
