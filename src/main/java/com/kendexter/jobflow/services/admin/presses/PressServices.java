package com.kendexter.jobflow.services.admin.presses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class PressServices {
	private static Logger logger = Logger.getLogger(PressServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getPresses")
	@GET
	@RolesAllowed({AppContext.ADMINISTRATION, AppContext.SCHEDULE_LEVEL_II, AppContext.SCHEDULE_LEVEL_III})
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPresses() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Press> presses = PressesDAO.getPresses(connection);
			return Response.ok().entity(presses).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deletePress")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePress(@QueryParam("id") int id) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PressesDAO.deletePress(connection, id);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyPressName")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyPressName(@QueryParam("id") int id, @QueryParam("name") String name) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM PRESSES WHERE NAME = ? AND ID != ?");
			select.setString(1, name);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null
					: "User name " + name + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertPress")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertPress(Press press) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			int count = PressesDAO.upsertPress(connection, press);
			connection.commit();
			return Response.ok().entity("{\"count\": \"" + count + "\"}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
