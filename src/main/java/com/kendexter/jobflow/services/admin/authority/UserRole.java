package com.kendexter.jobflow.services.admin.authority;

import java.util.Collection;
import java.util.LinkedList;

import com.kendexter.jobflow.config.AppContext;

public class UserRole {
	private int id;
	private String role;
	private Collection<Function> functions;;

	public UserRole() {
	}

	public UserRole(Role role) {
		this();
		setId(role.getId());
		setRole(role.getRole());
	}

	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setFunctions(Collection<Function> functions) {
		this.functions = functions;
	}

	public Collection<Function> getFunctions() {
		return this.functions;
	}

	public void addFunction(Function function) {
		this.functions.add(function);
	}

	public void loadFunctions(Collection<String> functions) {
		this.setFunctions(new LinkedList<Function>());
		for (String function : AppContext.ALL_FUNCTIONS) {
			addFunction(new Function(function, functions.contains(function)));
		}
	}
}
