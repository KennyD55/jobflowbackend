package com.kendexter.jobflow.services.admin.departments;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Department implements Serializable {
	private int id;
	private int seq;
	private String name;
	private String color;

	public Department() {
	}
	
	public int getId() {
		return this.id;
	}

	public Department(int id, int seq, String name, String color) {
		this.id = id;
		this.seq = seq;
		this.name = name;
		this.color = color;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
