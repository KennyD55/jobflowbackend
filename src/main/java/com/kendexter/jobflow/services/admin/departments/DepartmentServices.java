package com.kendexter.jobflow.services.admin.departments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class DepartmentServices {
	private static Logger logger = Logger.getLogger(DepartmentServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

	@Path("getDepartments")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDepartments() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Department> departments = DepartmentsDAO.getDepartments(connection);
			return Response.ok().entity(departments).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deleteDepartment")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePress(@QueryParam("id") int id) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			DepartmentsDAO.deleteDepartment(connection, id);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyDepartmentName")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyPressName(@QueryParam("id") int id, @QueryParam("name") String name) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM DEPARTMENTS WHERE NAME = ? AND ID != ?");
			select.setString(1, name);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null : "Department " + name + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("upsertDepartment")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertPress(Department department) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			DepartmentsDAO.upsertDepartment(connection, department);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
