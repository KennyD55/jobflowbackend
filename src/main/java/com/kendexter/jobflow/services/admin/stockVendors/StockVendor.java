package com.kendexter.jobflow.services.admin.stockVendors;

public class StockVendor {

	private String vendorName;

	public StockVendor() {
	}

	public StockVendor(String vendorName) {
		setVendorName(vendorName);
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

}
