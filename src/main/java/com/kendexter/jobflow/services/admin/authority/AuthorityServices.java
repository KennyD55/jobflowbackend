package com.kendexter.jobflow.services.admin.authority;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.ErrorMsg;
import com.kendexter.jobflow.config.AppContext;

@Path("admin")
public class AuthorityServices {
	private static Logger logger = Logger.getLogger(AuthorityServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;

//	@Path("getJobStatuses")
//	@GET
//	@RolesAllowed(AppContext.ADMINISTRATION)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getJobStatuses() {
//		Connection connection = null;
//		try {
//			connection = AppContext.jobflowDataSource.getConnection();
//			return Response.ok().entity(JobStatusesDAO.getJobStatuses(connection)).build();
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
//		} finally {
//			try {
//				connection.close();
//			} catch (Exception e) {
//				logger.error(e.getMessage(), e);
//			}
//		}
//	}

	@Path("upsertRole")
	@POST
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upsertRole(RolesForm form) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			AuthorityDAO.upsertRole(connection, form);
			connection.commit();
			return Response.ok().entity("{}").build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("verifyRole")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRole(@QueryParam("role") String role, @QueryParam("id") int id) {
		Connection conn = null;
		try {
			conn = AppContext.jobflowDataSource.getConnection();
			
			PreparedStatement select = conn.prepareStatement("SELECT COUNT(*) FROM ROLES WHERE ROLE = ? AND ID != ?");
			select.setString(1, role);
			select.setInt(2, id);
			ResultSet rs = select.executeQuery();
			rs.next();
			ErrorMsg errorMsg = new ErrorMsg(rs.getInt(1) == 0 ? null
					: "Job Title " + role + " already in use.");
			return Response.ok().entity(errorMsg).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("deleteRole")
	@RolesAllowed(AppContext.ADMINISTRATION)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteJobStatus(@QueryParam("role") String role) {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			AuthorityDAO.deleteRole(connection, role);
			if (AuthorityDAO.areThereAnyAdminRolesLeft(connection)) {
				connection.commit();
				return Response.ok().entity("{}").build();
			} else {
				connection.rollback();
				return Response.ok().entity("{\"error\":\"Can't delete this job title because there will be no one with Administration authority\"}").build();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getTitlesAndRoles")
	@GET
	@RolesAllowed({ AppContext.ADMINISTRATION })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTitlesAndRoles() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			LinkedList<RolesForm> rolesForms = new LinkedList<RolesForm>();
			Collection<Role> allRoles = AuthorityDAO.getRoles(connection);
			for (Role role : allRoles) {
				UserRole userRole = new UserRole(role);
				userRole.loadFunctions(AuthorityDAO.getFunctions(connection, role.getRole()));
				RolesForm form = new RolesForm(userRole);
				rolesForms.add(form);
			}
			return Response.ok().entity(rolesForms).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Path("getRoles")
	@GET
	@RolesAllowed(AppContext.ADMINISTRATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTitles() {
		Connection connection = null;
		try {
			connection = AppContext.jobflowDataSource.getConnection();
			Collection<Role> allRoles =  AuthorityDAO.getRoles(connection);
			Collection<String> roleNames = new LinkedList<String>();
			for (Role role : allRoles) {
				roleNames.add(role.getRole());
			}
			return Response.ok().entity(roleNames).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
