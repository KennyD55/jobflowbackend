package com.kendexter.jobflow.services.admin.misc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.jobs.JobSummary;
import com.kendexter.jobflow.services.jobs.JobsDAO;
import com.kendexter.jobflow.services.purchaseOrders.PurchaseOrderForm;
import com.kendexter.jobflow.services.users.User;

@Path("admin")
public class MiscellaneousServices {
	private static Logger logger = Logger.getLogger(MiscellaneousServices.class);
	private @Context ServletContext context;
	private @Context ContainerRequestContext requestContext;
	private static Object mutex = new Object();

	@Path("resetAllJobs")
	@POST
	@RolesAllowed({ AppContext.RESET_ALL })
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetAllJobs(@QueryParam("days") int days) {
		synchronized (mutex) {
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				PreparedStatement update = connection
						.prepareStatement("UPDATE JOBS SET JEOPARDY_TIME = JEOPARDY_TIME + INTERVAL ? DAY WHERE ARCHIVED = 0 AND JEOPARDY_TIME IS NOT NULL");
				update.setInt(1, days);
				update.executeUpdate();
				update.close();
				connection.commit();
				return Response.ok().entity("{}").build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("resetJob")
	@POST
	@RolesAllowed({ AppContext.RESET, AppContext.RESET_ALL })
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetJob(@QueryParam("days") int days, @QueryParam("jobNumber") int jobNumber, @Context HttpServletRequest request) {
		synchronized (mutex) {
			User user = (User) request.getAttribute("user");
			
			Connection connection = null;
			try {
				connection = AppContext.jobflowDataSource.getConnection();
				//
				JobSummary job = JobsDAO.getJobSummary(connection, jobNumber);
				//
				PreparedStatement update = connection.prepareStatement("UPDATE JOBS SET JEOPARDY_TIME = JEOPARDY_TIME + INTERVAL ? DAY WHERE JOB_NUMBER = ?");
				update.setInt(1, days);
				update.setInt(2, jobNumber);
				update.executeUpdate();
				update.close();
				//
				job = JobsDAO.getJobSummary(connection, jobNumber);
				//
				JobsDAO.insertJobHistory(connection, jobNumber, "Job Reset by " + days + (days == 1 ? " day" : " days"), user.getName(), job.getStatus());

				connection.commit();
	
				return Response.ok().entity(job).build();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
			} finally {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	@Path("getPending")
	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPending() {
		Connection connection = null;
		try {
			PendingActions pending = new PendingActions();
			connection = AppContext.jobflowDataSource.getConnection();

			PreparedStatement select = connection.prepareStatement("SELECT COUNT(*) FROM PURCHASE_ORDERS WHERE STATUS = ?");
			select.setString(1, PurchaseOrderForm.PENDING);
			ResultSet rs = select.executeQuery();
			rs.next();
			pending.setPendingPurchaseOrders(rs.getInt(1) > 0);
			rs.close();
			select.close();

			select = connection.prepareStatement("SELECT COUNT(*) FROM CUSTOMERS WHERE ACCOUNT_NUMBER IS NULL");
			rs = select.executeQuery();
			rs.next();
			pending.setPendingAccounts(rs.getInt(1) > 0);
			rs.close();
			select.close();

			connection.commit();
			return Response.ok().entity(pending).build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response.serverError().entity("{\"error\":\"Unexpected system error\"}").build();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

}
