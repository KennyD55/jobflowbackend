package com.kendexter.jobflow.services.admin.authority;

public class Function {

	private String name;
	private boolean authorized;

	public Function() {
	}

	public Function(String name, boolean authorized) {
		this.name = name;
		this.authorized = authorized;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAuthorized() {
		return authorized;
	}

	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}

}
