package com.kendexter.jobflow.services.admin.parameters;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.config.AppContext;

public class ParametersDAO {
	@SuppressWarnings("unused") private static Logger logger = Logger.getLogger(ParametersDAO.class);

	private ParametersDAO() {
	}

	public static ParametersForm getFormParameters(Connection connection) throws Exception {
		ResultSet rs = null;
		PreparedStatement select = null;
		ParametersForm parameters = new ParametersForm();
		try {
			select = connection.prepareStatement("SELECT NAME, VALUE FROM PARAMETERS");
			rs = select.executeQuery();
			while (rs.next()) {
				String name = rs.getString("NAME");
				String value = rs.getString("VALUE");
				if (name.equals(AppContext.COMPANY_CODE))
					parameters.setCompanyCode(value);
				else if (name.equals(AppContext.NEXT_ACCOUNT_NUMBER))
					parameters.setNextAccountNumber(value);
				else if (name.equals(AppContext.NEXT_JOB_NUMBER))
					parameters.setNextJobNumber(value);
				else if (name.equals(AppContext.NEXT_PO_NUMBER))
					parameters.setNextPoNumber(value);
				else if (name.equals(AppContext.CEILING_LIMIT))
					parameters.setCeilingLimit(value);
				else if (name.equals(AppContext.PRESS_START_TIME_HOUR))
					parameters.setPressStartTimeH(value);
				else if (name.equals(AppContext.PRESS_START_TIME_MINUTE))
					parameters.setPressStartTimeM(value);
				else if (name.equals(AppContext.PRESS_START_TIME_AMPM))
					parameters.setPressStartTimeAMPM(value);
				else if (name.equals(AppContext.SCHEDULE_PROMOTE_STATUS))
					parameters.setSchedulePromoteStatus(value);
				else if (name.equals(AppContext.SHIP_TO_ADDRESS))
					parameters.setShipToAddress(value);
				else if (name.equals(AppContext.BILL_TO_ADDRESS))
					parameters.setBillToAddress(value);
				else if (name.equals(AppContext.TIME_ZONE))
					parameters.setTimeZone(value);
				else if (name.equals(AppContext.TIMEOUT))
					parameters.setTimeoutInMinutes(value);
			}
			return parameters;
		} finally {
			try {
				rs.close();
				select.close();
			} catch (Exception e) {
			}
		}
	}

	public static synchronized int updateParameters(Connection connection, ParametersForm form) throws Exception {
		PreparedStatement update = null;
		int count = 0;
		try {
			update = connection.prepareStatement("UPDATE PARAMETERS SET VALUE = ? WHERE NAME = ?");
			update.setString(1, form.getCompanyCode());
			update.setString(2, AppContext.COMPANY_CODE);
			count += update.executeUpdate();
			
			update.setString(1, form.getNextAccountNumber());
			update.setString(2, AppContext.NEXT_ACCOUNT_NUMBER);
			count += update.executeUpdate();
			
			update.setString(1, form.getNextJobNumber());
			update.setString(2, AppContext.NEXT_JOB_NUMBER);
			count += update.executeUpdate();
			
			update.setString(1, form.getNextPoNumber());
			update.setString(2, AppContext.NEXT_PO_NUMBER);
			count += update.executeUpdate();
			
			/*  Add the decimals to ceiling limit */
			String ceiling = form.getCeilingLimit();
			if (ceiling.indexOf(".") == -1)
				ceiling = ceiling + ".00";
			else if (ceiling.split("\\.")[1].length() < 2)
				ceiling = ceiling + "0";
			update.setString(1, ceiling);
			update.setString(2, AppContext.CEILING_LIMIT);
			count += update.executeUpdate();
			
			update.setString(1, form.getPressStartTimeM());
			update.setString(2, AppContext.PRESS_START_TIME_HOUR);
			count += update.executeUpdate();
			
			update.setString(1, form.getPressStartTimeM());
			update.setString(2, AppContext.PRESS_START_TIME_MINUTE);
			count += update.executeUpdate();
			
			update.setString(1, form.getPressStartTimeAMPM());
			update.setString(2, AppContext.PRESS_START_TIME_AMPM);
			count += update.executeUpdate();
			
			update.setString(1, form.getSchedulePromoteStatus());
			update.setString(2, AppContext.SCHEDULE_PROMOTE_STATUS);
			count += update.executeUpdate();
			
			update.setString(1, form.getShipToAddress());
			update.setString(2, AppContext.SHIP_TO_ADDRESS);
			count += update.executeUpdate();
			
			update.setString(1, form.getBillToAddress());
			update.setString(2, AppContext.BILL_TO_ADDRESS);
			count += update.executeUpdate();
			
			update.setString(1, form.getTimeZone());
			update.setString(2, AppContext.TIME_ZONE);
			count += update.executeUpdate();
			
			update.setString(1, form.getTimeoutInMinutes());
			update.setString(2, AppContext.TIMEOUT);
			count += update.executeUpdate();
			return count;
		} finally {
			try {

				update.close();
			} catch (Exception e) {
			}
		}
	}
}
