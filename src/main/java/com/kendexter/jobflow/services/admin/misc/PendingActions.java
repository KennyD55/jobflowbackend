package com.kendexter.jobflow.services.admin.misc;

public class PendingActions {

	private boolean pendingPurchaseOrders;
	private boolean pendingAccounts;
	
	public PendingActions() {
		setPendingPurchaseOrders(false);
		setPendingAccounts(false);
	}

	public boolean isPendingPurchaseOrders() {
		return pendingPurchaseOrders;
	}

	public void setPendingPurchaseOrders(boolean pendingPurchaseOrders) {
		this.pendingPurchaseOrders = pendingPurchaseOrders;
	}

	public boolean isPendingAccounts() {
		return pendingAccounts;
	}

	public void setPendingAccounts(boolean pendingAccounts) {
		this.pendingAccounts = pendingAccounts;
	}

}
