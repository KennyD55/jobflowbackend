package com.kendexter.jobflow.filters;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.kendexter.jobflow.beans.TokenAttribute;

/**
 * This filter verify the access permissions for a user based on username and passowrd provided in request
 */
@SuppressWarnings({"unchecked", "serial"})
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {
	@Context private ResourceInfo resourceInfo;
	@Context private ServletContext context;
	Logger logger = Logger.getLogger(AuthenticationFilter.class);
	private static final ResponseBuilder NOT_LOGGED_ON = Response.status(Response.Status.NETWORK_AUTHENTICATION_REQUIRED).entity("You are not logged on");
	private static final ResponseBuilder ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN).entity("You cannot access this resource");
	
	private static final LinkedList<String> bypassMethods = new LinkedList<String>() {{add("login");add("logout");add("getJobXML");add("viewNotes");add("viewJob");add("viewPO");}};
	@Override
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();
		if (bypassMethods.contains(method.getName())) {
			return;  // Authorization logic is outside this filter (Login and pop-ups with onetime tokens).
		}
		
		//Get request headers
		final MultivaluedMap<String, String> headers = requestContext.getHeaders();
		String bearerToken;
		List<String> authorizationHeaders = headers.get("authorization");
		if (authorizationHeaders == null || authorizationHeaders.size() == 0) {
			try {
				throw new Exception("No authorization token provided in request");
			} catch (Exception e) {
				logger.error("method.name=" + method.getClass() + "." + method.getName());
				logger.error("No authorization token provided in request", e);
				requestContext.abortWith(ACCESS_FORBIDDEN.build());
				return;
			}
		}
		try {
			bearerToken = headers.get("authorization").iterator().next().substring(7);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			requestContext.abortWith(ACCESS_FORBIDDEN.build());
			return;
		}
		Map<String, TokenAttribute> map = ((Map<String, TokenAttribute>) context.getAttribute("tokenMap"));
		Set<String> tokens = map.keySet();
		LinkedList<String> deleteTokens = new LinkedList<String>();
		/*
		 * Remove expired tokens
		 */
		for (String token : tokens) {
			TokenAttribute ta = map.get(token);
			if (ta.expired()) {
				deleteTokens.add(token);
			}
		}
		for (String token : deleteTokens) {
			map.remove(token);
		}
		
		TokenAttribute ta = map.get(bearerToken);
		
		if (ta == null) {
			requestContext.abortWith(NOT_LOGGED_ON.build());
			return;
		}
		ta.reset();  // Reset the timeout clock.
		Collection<String> functions = ta.getUser().getFunctions();
		requestContext.setProperty("user", ta.getUser());   // Maps to HttpRequestServlet.setAttribute.
		//Access allowed for all
		if (method.isAnnotationPresent(PermitAll.class)) {
			return;
		}
		//Verify user access
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
			Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
			for (String role : rolesSet) {
				if (functions.contains(role)) {
					return;
				}
			}
		}
		logger.error(ta.getUser().getUserID() + " attempted to access " + method.getDeclaringClass().getSimpleName() + "." + method.getName());
		
		requestContext.abortWith(ACCESS_FORBIDDEN.build());
	}
	public static TokenAttribute getOneTimeToken(ServletContext context, String token) {
		Map<String, TokenAttribute> map = ((Map<String, TokenAttribute>) context.getAttribute("tokenMap"));
		TokenAttribute ta = map.remove(token);
		return ta;
	}
}