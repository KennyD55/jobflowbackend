package com.kendexter.jobflow.config;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TimeZone;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kendexter.jobflow.beans.TokenAttribute;
import com.kendexter.jobflow.services.purchaseOrders.PurchaseOrderForm;
import com.kendexter.jobflow.services.users.User;

@WebListener
@SuppressWarnings("unchecked")
public class AppContext implements ServletContextListener {
	public static DataSource jobflowDataSource;
	private static ServletContext context;
	private static Logger logger = Logger.getLogger(AppContext.class);
	public static final ThreadLocal<ObjectMapper> MAPPER = new ThreadLocal<ObjectMapper>() {
		public ObjectMapper initialValue() {
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.setSerializationInclusion(Include.NON_NULL);
			mapper.setTimeZone(TimeZone.getDefault());
			return mapper;
		}
	};
	public static final ThreadLocal<ObjectMapper> PRINTMAPPER = new ThreadLocal<ObjectMapper>() {
		@Override
		public ObjectMapper initialValue() {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.setSerializationInclusion(Include.NON_NULL);
			mapper.setTimeZone(TimeZone.getDefault());
			return mapper;
		}
	};

	public static final ThreadLocal<SimpleDateFormat> YYYYMMDD = new ThreadLocal<SimpleDateFormat>() {
		@Override
		public SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};

	@Override
	public void contextInitialized(ServletContextEvent event) {
		context = event.getServletContext();
		context.setAttribute("tokenMap", new HashMap<String, TokenAttribute>());
		InitialContext cxt = null;
		try {
			cxt = new InitialContext();
			jobflowDataSource = (DataSource) cxt.lookup("java:/comp/env/jdbc/Jobflow");
		} catch (Exception e) {
			logger.error("", e);
		}
		loadParameters();
		loadJobsWithPurchaseOrders();
		logger.info("Jobflow Backend Context Initialized");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
				logger.info(String.format("deregistering jdbc driver: %s", driver));
			} catch (SQLException e) {
				logger.error(String.format("Error deregistering driver %s", driver), e);
			}

		}
	}
	public static void loadParameters() {
		HashMap<String, String> parameters = new HashMap<String, String>();
		context.setAttribute("parameters", parameters);
		Connection connection = null;
		try {
			connection = jobflowDataSource.getConnection();
			PreparedStatement select = connection.prepareStatement("SELECT NAME, VALUE FROM PARAMETERS");
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				parameters.put(rs.getString("NAME"), rs.getString("VALUE"));
			}
		} catch (Exception e) {
			logger.error("", e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}

	public static void loadJobsWithPurchaseOrders() {
		Connection conn = null;
		try {
			conn = jobflowDataSource.getConnection();
			PreparedStatement select = conn.prepareStatement("SELECT DISTINCT A.JOB_NUMBER FROM JOBS A"
					+ " WHERE A.ARCHIVED = 0"
					+ " AND EXISTS (SELECT * FROM PURCHASE_ORDERS B WHERE A.JOB_NUMBER = B.JOB_NUMBER AND B.STATUS != '"
					+ PurchaseOrderForm.CANCELLED
					+ "')");
			ResultSet rs = select.executeQuery();
			LinkedList<Integer> jobsWPurchaseOrders = new LinkedList<Integer>();
			while (rs.next()) {
				jobsWPurchaseOrders.add(rs.getInt(1));
			}
			context.setAttribute("jobsWPurchaseOrders", jobsWPurchaseOrders);
			rs.close();
			select.close();
		} catch (Exception e) {
			logger.error("", e);
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}

	/*
	 * Get the user from the bearer token by looking it up on the context tokenMap attribute
	 */
	public static User getUser(ServletContext context, ContainerRequestContext requestContext) {
		Map<String, TokenAttribute> map = ((Map<String, TokenAttribute>) context.getAttribute("tokenMap"));
		final MultivaluedMap<String, String> headers = requestContext.getHeaders();
		String bearerToken = headers.get("authorization").iterator().next().substring(7);
		TokenAttribute ta = map.get(bearerToken);
		return ta.getUser();
	}

	public static String getParameterValue(String parameter) {
		HashMap<String, String> parameters = (HashMap<String, String>) context.getAttribute("parameters");
		return parameters.get(parameter);
	}

	public static void setParameterValue(Connection connection, String name, String value) throws Exception {

		PreparedStatement update = connection.prepareStatement("UPDATE PARAMETERS SET VALUE = ? WHERE NAME = ?");
		update.setString(1, value);
		update.setString(2, name);
		update.executeUpdate();
		connection.commit();
		update.close();

		HashMap<String, String> parameters = (HashMap<String, String>) context.getAttribute("parameters");
		parameters.put(name, value);
	}

	public static synchronized TimeZone getTimeZone() {
		if (jobflowDataSource == null) {  // For batch processing of schedule
			return TimeZone.getDefault();
		}
		TimeZone timeZone = null;
		Connection connection = null;
		try {
			connection = jobflowDataSource.getConnection();
			String timeZoneValue = getParameterValue("time_zone");
			if (timeZoneValue == null) {
				timeZone = TimeZone.getDefault();
				setParameterValue(connection, "time_zone", timeZone.getID());
				connection.commit();
			} else
				timeZone = TimeZone.getTimeZone(timeZoneValue);
			return timeZone;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	/*
	 * Parameter names
	 */
	public static final String COMPANY_CODE = "company_code";
	public static final String NEXT_ACCOUNT_NUMBER = "next_account_number";
	public static final String NEXT_JOB_NUMBER = "next_job_number";
	public static final String NEXT_PO_NUMBER = "next_po_number";
	public static final String CEILING_LIMIT = "ceiling_limit";
	public static final String PRESS_START_TIME_HOUR = "press_start_time_h";
	public static final String PRESS_START_TIME_MINUTE = "press_start_time_m";
	public static final String PRESS_START_TIME_AMPM = "press_start_time_ampm";
	public static final String SCHEDULE_PROMOTE_STATUS = "schedule_promote_status";
	public static final String SHIP_TO_ADDRESS = "ship_to_address";
	public static final String BILL_TO_ADDRESS = "bill_to_address";
	public static final String TIMEOUT = "timeout_in_minutes";
	public static final String TIME_ZONE = "time_zone";
	public static final String HOST_EMAIL = "host_email";
	public static final String HOST_SERVER = "host_server";
	public static final String EMAIL_PASSWORD = "email_password";
	public static final String PORT = "port";
	public static final String EMAIL_USER = "email_user";
	public static final String SETUP_COMPLETE = "setup_complete";
	// Functions
	public static final String ADD_CUSTOMER = "Add Customer";
	public static final String ADMINISTRATION = "Administration";
	public static final String APPROVE_PURCHASE_ORDERS = "Approve Purchase Orders";
	public static final String ARCHIVE = "Archive";
	public static final String BACKUP_JOBS = "Backup Jobs";
	public static final String COD = "COD";
	public static final String DELETE_JOBS = "Delete Jobs";
	public static final String ENTER_JOB_ESTIMATES = "Enter Job Estimates";
	public static final String ENTER_JOBS = "Enter Jobs";
	public static final String ENTER_PURCHASE_ORDERS = "Enter Purchase Orders";
	public static final String HOT = "HOT";
	public static final String MAINTAIN_ALL_CUSTOMERS = "Maintain All Customers";
	public static final String MAINTAIN_JOBS = "Maintain Jobs";
	public static final String MAINTAIN_MY_CUSTOMERS = "Maintain My Customers";
	public static final String PRESSMAN = "Pressman";
	public static final String PROMOTE = "Promote";
	public static final String REPORTS = "Reports";
	public static final String RESET = "Reset";
	public static final String RESET_ALL = "Reset All";
	public static final String RESTRICTED_STATUS = "Restricted Status";
	public static final String SCHEDULE_LEVEL_I = "Schedule Level I";
	public static final String SCHEDULE_LEVEL_II = "Schedule Level II";
	public static final String SCHEDULE_LEVEL_III = "Schedule Level III";
	public static final String STOCK = "Stock";
	public static final String VIEW_JOBS = "View Jobs";
	public static final String VIEW_MAIL_JOBS = "View Mail Jobs";
	@SuppressWarnings("serial") public static final LinkedList<String> ALL_FUNCTIONS = new LinkedList<String>() {
		{
			add(ADMINISTRATION);
			add(ENTER_JOBS);
			add(MAINTAIN_JOBS);
			add(VIEW_JOBS);
			add(MAINTAIN_ALL_CUSTOMERS);
			add(MAINTAIN_MY_CUSTOMERS);
			add(PRESSMAN);
			add(ADD_CUSTOMER);
			add(ENTER_JOB_ESTIMATES);
			add(COD);
			add(REPORTS);
			add(ARCHIVE);
			add(PROMOTE);
			add(RESET);
			add(HOT);
			add(ENTER_PURCHASE_ORDERS);
			add(APPROVE_PURCHASE_ORDERS);
			add(STOCK);
			add(RESET_ALL);
			add(SCHEDULE_LEVEL_I);
			add(SCHEDULE_LEVEL_II);
			add(SCHEDULE_LEVEL_III);
			add(BACKUP_JOBS);
			add(DELETE_JOBS);
			add(RESTRICTED_STATUS);
			add(VIEW_MAIL_JOBS);
		}
	};

}
