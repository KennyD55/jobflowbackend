package com.kendexter.jobflow.config;

import java.text.SimpleDateFormat;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class MyApplication extends Application {
	public MyApplication() {
		System.out.println(getClass() + "\r\nJobflow2018 restarted @ "
				+ new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss a").format(System.currentTimeMillis()));
	}
}