package com.kendexter.jobflow.beans;

import java.util.Collection;

import com.kendexter.jobflow.config.AppContext;
import com.kendexter.jobflow.services.users.User;

public class TokenAttribute {
	private String token;
	private long lastAccessTime;
	private User user;
	private Collection<Integer> monitoredJobs;

	public TokenAttribute(String token, User user, Collection<Integer> monitoredJobs) {
		this.setUser(user);
		this.setToken(token);
		this.setMonitoredJobs(monitoredJobs);
		reset();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void reset() {
		this.lastAccessTime = System.currentTimeMillis();
	}

	public boolean expired() {
		long expirationTime = lastAccessTime + (Integer.parseInt(AppContext.getParameterValue(AppContext.TIMEOUT)) * 60000);
		return expirationTime < System.currentTimeMillis(); 
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Integer> getMonitoredJobs() {
		return monitoredJobs;
	}

	public void setMonitoredJobs(Collection<Integer> monitoredJobs) {
		this.monitoredJobs = monitoredJobs;
	}
}
