package com.kendexter.pdf;

import java.awt.Color;

/*
 * Delegate class so I don't have to re-write the PDF prints with for PDFBox
 */
public class Line {
	public float x1;
	public float y1;
	public float x2;
	public float y2;
	public Color color;

	public Line(float x1, float y1, float x2, float y2, Color color) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.color = color;
	}

	public Line(float x1, float y1, float x2, float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.color = Color.BLACK;
	}
}
