package com.kendexter.pdf;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.kendexter.jobflow.services.users.User;


@SuppressWarnings("serial")
public abstract class PDFBoxServlet {
	private static Logger logger = Logger.getLogger(PDFBoxServlet.class);
	protected float pageHeight;
	protected float pageWidth;
	protected HttpServletRequest request;
	protected PDPageContentStream pageContentStream;
	protected PDDocument document;
	
	public PDFBoxServlet(HttpServletRequest request) {
		this.request = request;
	}
	public PDDocument generatePDF () throws Exception {
		document = new PDDocument();
		request.setAttribute("document", document);
		try {
			User user = (User) request.getAttribute("user");
			process(user);
			return document;
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}

	protected abstract void process(User user) throws Exception;

	protected void addPage(float width, float height) throws IOException {
		addPage(new PDRectangle(width, height));
	}

	protected void addPage(PDRectangle rect) throws IOException {

		if (pageContentStream != null) {
			pageContentStream.close();
		}
		PDPage page = new PDPage(rect);
		pageHeight = rect.getHeight();
		pageWidth = rect.getWidth();
		document.addPage(page);
		pageContentStream = new PDPageContentStream(document, page);
	}

	protected void block(float xleft, float ytop, float xright, float ybottom, String text) throws IOException {
		block(xleft, ytop, xright, ybottom, text, Math.PI / 2);
	}

	protected void block(float xleft, float ytop, float xright, float ybottom, String text, double rotateAngle) throws IOException {
		float height = ybottom - ytop;
		float width = xright - xleft;
		float fontSize = 12f;
		pageContentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		pageContentStream.fillRect(xleft, pageHeight - ybottom, width, height);
		pageContentStream.beginText();
		pageContentStream.setFont(PDType1Font.TIMES_BOLD_ITALIC, fontSize);
		pageContentStream.setNonStrokingColor(Color.WHITE);
		/*
		 * Center
		 */
		while ((PDType1Font.TIMES_BOLD_ITALIC.getStringWidth(text) / 1000 * fontSize) < ybottom - ytop)
			text = " " + text + " ";
		//
		pageContentStream.setTextRotation(rotateAngle, xright - 7, pageHeight - ybottom);
		pageContentStream.drawString(text);
		pageContentStream.endText();
	}

	protected void addText(String text, PDFont font, float fontSize, float x, float y) throws IOException {
		addText(text, font, fontSize, x, y, Color.BLACK);
	}

	protected void addText(String text, PDFont font, float fontSize, float x, float y, Color color) throws IOException {
		pageContentStream.setNonStrokingColor(color);
		pageContentStream.beginText();
		pageContentStream.setFont(font, fontSize);
		pageContentStream.moveTextPositionByAmount(x, pageHeight - y);
		pageContentStream.drawString(text == null ? "" : text);
		pageContentStream.endText();
	}

	protected void addLine(float lineWidth, float xleft, float ytop, float xright, float ybottom) throws IOException {
		addLine(lineWidth, xleft, ytop, xright, ybottom, Color.BLACK);
	}

	protected void addLine(float lineWidth, float xleft, float ytop, float xright, float ybottom, Color color) throws IOException {
		pageContentStream.setStrokingColor(color);
		pageContentStream.setLineWidth(lineWidth);
		pageContentStream.addLine(xleft, pageHeight - ytop, xright, pageHeight - ybottom);
		pageContentStream.closeAndStroke();
	}

	protected void addHorizontal(float lineWidth, float xleft, float xright, float y, Color color) throws IOException {
		addLine(lineWidth, xleft, y, xright, y, color);
	}

	protected void addVertical(float lineWidth, float x, float ytop, float ybottom, Color color) throws IOException {
		addLine(lineWidth, x, ytop, x, ybottom, color);
	}

	protected void addBox(float xleft, float ytop, float xright, float ybottom, Color color) throws IOException {
		addBox(1f, xleft, ytop, xright, ybottom, color);
	}

	protected void addBox(float lineWidth, float xleft, float ytop, float xright, float ybottom, Color color) throws IOException {
		addHorizontal(lineWidth, xleft, xright, ytop, color);
		addHorizontal(lineWidth, xleft, xright, ybottom, color);
		addVertical(lineWidth, xleft, ytop, ybottom, color);
		addVertical(lineWidth, xright, ytop, ybottom, color);
	}

	protected void addTable(int[][] columns, final String[] texts, PDFont font, float fontSize, float y, float height, Color color, boolean borders)
			throws IOException {
		LinkedList<String[]> list = new LinkedList<String[]>() {
			{
				add(texts);
			}
		};
		addTable(columns, list, font, fontSize, y, height, color, borders);
	}

	protected void addTable(int[][] columns, Collection<String[]> texts, PDFont font, float fontSize, float y, float height, Color color, boolean borders)
			throws IOException {
		if (borders) {
			addHorizontal(1, columns[0][0], columns[columns.length - 1][0], y, color);
		}
		for (String[] lines : texts) {
			addRow(columns, lines, font, fontSize, y, height, color, borders);
			y += height;
		}
	}

	protected void addRow(int[][] columns, String[] texts, PDFont font, float fontSize, float y, float height, Color color, boolean borders) throws IOException {
		for (int i = 0; i < columns.length; i++) {
			if (borders) {
				addVertical(1, columns[i][0], y, y + height + 2, color);
			}
			if (i < texts.length) {
				String text = columns[i].length > 1 && texts[i].length() > columns[i][1] ? texts[i].substring(0, columns[i][1]) : texts[i];
				addText(text, font, fontSize, columns[i][0] + 4, y + height - 2);
			}
		}
		if (borders) {
			addHorizontal(1, columns[0][0], columns[columns.length - 1][0], y + height + 2, color);
		}
	}

	protected void gen8(String s, float x, float y) throws IOException {
		addText(s, PDType1Font.TIMES_ITALIC, 9, x, y);
	}

	protected void gen12(String s, float x, float y) throws IOException {
		addText(s, PDType1Font.TIMES_ITALIC, 12, x, y);
	}

	protected void gen12Bold(String s, float x, float y) throws IOException {
		addText(s, PDType1Font.TIMES_BOLD, 12, x, y);
	}

	protected void gen(String s, float x, float y) throws IOException {
		addText(s, PDType1Font.TIMES_ITALIC, 12, x, y);
	}

	protected void checkBox(float x, float y, boolean checked) throws IOException {
		y -= 10; //Legacy requirement
		addBox(1f, x, y, x + 9, y + 9, Color.LIGHT_GRAY);
		if (checked) {
			addLine(2, x, y, x + 4, y + 7);
			addLine(2, x + 4, y + 9, x + 10, y - 5);
		}
	}

	protected void splitUpText(String text, int rowLength, float x, float y, float yincr, int maxLines, PDFont font, float fontSize) throws IOException {
		ArrayList<String> lines = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(text == null ? "" : text);
		StringBuilder sb = new StringBuilder();
		while (st.hasMoreTokens()) { // split up tokens larger than the rowlength
			String next = st.nextToken();
			while (next.length() > rowLength) {
				sb.append(next.substring(0, rowLength) + " ");
				next = next.substring(rowLength);
			}
			sb.append(next + " ");
		}
		text = sb.toString();
		st = new StringTokenizer(text);
		String line = "";
		String token = "";
		while (st.hasMoreElements()) {
			token = st.nextToken();
			String test = line.length() > 0 ? line + " " + token : token;
			if (test.length() > rowLength) {
				lines.add(line);
				line = token;
			} else {
				line = test;
			}
		}
		if (line.length() > 0)
			lines.add(line);
		if (lines.size() > maxLines) {
			while (lines.size() > maxLines) {
				lines.remove(lines.size() - 1);
			}
			String work = lines.get(lines.size() - 1);
			work = work.substring(0, work.length() - 3) + "...";
			lines.remove(lines.size() - 1);
			lines.add(work);
		}
		for (int i = 0; i < lines.size(); i++) {
			addText(lines.get(i), font, fontSize, x, y);
			y += yincr;
		}
	}
}
