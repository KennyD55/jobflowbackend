package com;

public class Temp2 {

	private String fld1;
	private String fld2;

	public Temp2() {
	}

	public Temp2(String fld1, String fld2) {
		super();
		this.fld1 = fld1;
		this.fld2 = fld2;
	}

	public String toString() {
		return "fld1=" + fld1 + " fld2=" + fld2;
	}

	public String getFld1() {
		return fld1;
	}

	public void setFld1(String fld1) {
		this.fld1 = fld1;
	}

	public String getFld2() {
		return fld2;
	}

	public void setFld2(String fld2) {
		this.fld2 = fld2;
	}

}
