package com;

import java.util.LinkedList;

import com.kendexter.Utilities;

public class Temp {
	private LinkedList<Temp2> flds = new LinkedList<Temp2>();
	private String fldB;

	public static void main(String[] args) throws Exception {
		System.out.println("****** shallow Clone Test *******");
		shallowClone();
		System.out.println("\r\n****** deep Clone Test *******");
		deepClone();

		System.out.println("OK");
	}

	private static void shallowClone() {
		Temp tempa = new Temp();
		tempa.setFldB("Ken");
		tempa.getFlds().add(new Temp2("abc", "def"));
		System.out.println("tempa=" + tempa);
		Temp tempb = new Temp();
		tempb = (Temp) Utilities.shallowClone(tempa, tempb);
		System.out.println("tempb=" + tempb);
		System.out.println("***************");
		tempa.getFlds().get(0).setFld1("XYZ");
		tempa.getFlds().add(new Temp2("jkl", "mnop"));
		tempa.setFldB("Janice");
		System.out.println("tempa=" + tempa);
		System.out.println("tempb=" + tempb);
	}

	private static void deepClone() {
		Temp tempa = new Temp();
		tempa.setFldB("Ken");
		tempa.getFlds().add(new Temp2("abc", "def"));
		System.out.println("tempa=" + tempa);
		Temp tempb = new Temp();
		tempb = (Temp) Utilities.deepClone(tempa, tempb);
		System.out.println("tempb=" + tempb);
		System.out.println("***************");
		tempa.getFlds().get(0).setFld1("XYZ");
		tempa.getFlds().add(new Temp2("jkl", "mnop"));
		tempa.setFldB("Janice");
		System.out.println("tempa=" + tempa);
		System.out.println("tempb=" + tempb);
	}

	public Temp() {
		setFlds(new LinkedList<Temp2>());
		setFldB("");
	}

	public String toString() {
		return "fldB=" + fldB + " flds=" + flds;
	}

	public LinkedList<Temp2> getFlds() {
		return flds;
	}

	public void setFlds(LinkedList<Temp2> flds) {
		this.flds = flds;
	}

	public String getFldB() {
		return fldB;
	}

	public void setFldB(String fldB) {
		this.fldB = fldB;
	}
}
